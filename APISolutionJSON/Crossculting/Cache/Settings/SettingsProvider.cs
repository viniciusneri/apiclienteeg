﻿using System;

namespace Saraiva.SE.Crossculting.Cache.Settings
{
    public interface ISettingsProvider
    {
        string GetConnectionString(string name);
    }

    public class ConfigurationManagerSettingsProvider : ISettingsProvider
    {
        public string GetConnectionString(string name)
        {
            return Helper.HelperConfig.GetConfigByLabel("Connection.json", "Cache", name);
        }
    }

    public static class SettingsManager
    {
        private static ISettingsProvider _instance;
        public static ISettingsProvider Provider
        {
            get
            {
                if (_instance == null)
                    _instance = new ConfigurationManagerSettingsProvider();

                return _instance;
            }
        }
        public static void SetProvider(ISettingsProvider instance)
        {
            if (instance == null)
                throw new ArgumentException("Use \"ConfigurationManagetSettingsProvider\" instead of a null reference.");
            _instance = instance;
        }

        public static string GetString(string key)
        {
            return Provider.GetConnectionString(key);
        }
    }
}
