﻿using Newtonsoft.Json;
using Saraiva.SE.Crossculting.Cache.Settings;
using StackExchange.Redis;
using System;

namespace Saraiva.SE.Crossculting.Cache.Repository
{
    public class CacheRepository
    {
        static ConnectionMultiplexer _connectionMultiplexer = OpenRedis();
        static JsonSerializerSettings _jsonSettings = GetJsonSettings();

        private static JsonSerializerSettings GetJsonSettings()
        {
            var resolver = new Newtonsoft.Json.Serialization.DefaultContractResolver() { };

            var settings = new JsonSerializerSettings
            {
                ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                ContractResolver = resolver
            };

            return settings;
        }

        private static ConnectionMultiplexer OpenRedis()
        {
            var cachingConfig = new ConfigurationManagerSettingsProvider();
                        
            var settings = cachingConfig.GetConnectionString("Cache.Redis.Server");
            var databaseDefault = cachingConfig.GetConnectionString("Cache.Redis.Database");
            var password = cachingConfig.GetConnectionString("Cache.Redis.Password");

            var config = new ConfigurationOptions
            {
                AbortOnConnectFail = false,
                AllowAdmin = true,
                ConnectTimeout = 5000,
                ResponseTimeout = 5000,
                Ssl = true
            };


            if (password != null) config.Password = password;
            if (databaseDefault != null)
            {
                if (Convert.ToInt32(databaseDefault) > 0)
                    config.DefaultDatabase = Convert.ToInt32(databaseDefault);
            }

            config.EndPoints.Add(settings);
            return ConnectionMultiplexer.Connect(config);
        }
        public object GetObject(Type type, string key)
        {
            try
            {
                var db = _connectionMultiplexer.GetDatabase();
                string json = db.StringGet(key);
                if (string.IsNullOrEmpty(json))
                    return null;

                return JsonConvert.DeserializeObject(json, type, _jsonSettings);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        public object GetObject(string key)
        {
            try
            {
                var db = _connectionMultiplexer.GetDatabase();
                string json = db.StringGet(key);
                if (string.IsNullOrEmpty(json))
                    return null;

                return json;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public T GetObject<T>(string key)
        {
            try
            {
                var db = _connectionMultiplexer.GetDatabase();
                string json = db.StringGet(key);
                if (string.IsNullOrEmpty(json))
                    return default(T);

                return JsonConvert.DeserializeObject<T>(json, _jsonSettings);
            }
            catch (Exception ex)
            {
                return default(T);
            }
        }
        public void StoreObject(string key, object value, TimeSpan expiry)
        {
            try
            {
                if (value == null)
                    return;

                var db = _connectionMultiplexer.GetDatabase();
                var obj = Newtonsoft.Json.JsonConvert.SerializeObject(value);
                var result = db.StringSet(key, obj, expiry);
            }
            catch (Exception ex)
            {
            }
        }
        public void FlushAll()
        {
            var endpoints = OpenRedis().GetEndPoints(true);
            foreach (var endpoint in endpoints)
            {
                var server = _connectionMultiplexer.GetServer(endpoint);
                server.FlushAllDatabases();
            }
        }
    }
}
