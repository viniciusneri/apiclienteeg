﻿using Microsoft.Azure.ServiceBus;
using Microsoft.Azure.ServiceBus.Core;
using Microsoft.Extensions.Configuration;
using Saraiva.Framework.Service.Fila;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace Saraiva.SE.Crossculting.AzureServices
{
    public interface IServiceBus
    {
        Task<bool> PostReceiptBusAsync<T>(object obj, string config);
        Task ProcessReceiptBusAsync<T>(Func<T, Task<bool>> method, string config);
        Task RecuperarMensagemFilaLoopXml(string config, Func<XmlDocument, Task<bool>> metodoProcessamento);
        Task<bool> EnviarRegistroFilaXml(XmlDocument objeto, string config);
    }

    public class ServiceBus : IServiceBus
    {
        private Dictionary<string, QueueClient> queueClient;
        private Dictionary<string, MessageReceiver> receiverClient;
        private Dictionary<string, Message> mensagens;

        public ServiceBus()
        {
            queueClient = new Dictionary<string, QueueClient>();
            receiverClient = new Dictionary<string, MessageReceiver>();
        }

        public async Task<bool> PostReceiptBusAsync<T>(object obj, string config)
        {
            Mensagem<T> _mensagem = new Mensagem<T>();

            return await _mensagem.EnviarRegistroFila((T)obj, config);
        }

        public async Task ProcessReceiptBusAsync<T>(Func<T, Task<bool>> method, string config)
        {
            Mensagem<T> _mensagem = new Mensagem<T>();

            await _mensagem.RecuperarMensagemFila(method, config);
        }

        public async Task RecuperarMensagemFilaLoopXml(string config, Func<XmlDocument, Task<bool>> metodoProcessamento)
        {
            var configuracao = LerParametrosFila(config);

            if (!receiverClient.ContainsKey(string.Concat(configuracao.UrlFila, configuracao.NomeFila)))
                receiverClient.Add(string.Concat(configuracao.UrlFila, configuracao.NomeFila), new MessageReceiver(configuracao.UrlFila, configuracao.NomeFila, ReceiveMode.PeekLock, null, configuracao.QuantidadeMensagem));

            var receiver = receiverClient[string.Concat(configuracao.UrlFila, configuracao.NomeFila)];
            receiver.OperationTimeout = new TimeSpan(0, 10, 0);

            //var retorno = Task.Run(async () => await receiver.ReceiveAsync(configuracao.QuantidadeMensagem));
            //Task.WaitAll(retorno);
            //var mensagem = retorno.Result;

            var mensagem = await receiver.ReceiveAsync(configuracao.QuantidadeMensagem);
            if (mensagem == null)
                return;
            else
                foreach (var msg in mensagem)
                {
                    XmlDocument doc = new XmlDocument();
                    string xml = Encoding.UTF8.GetString(msg.Body);
                    doc.LoadXml(xml);

                    //var renew = Task.Run(async () => await receiver.RenewLockAsync(msg));
                    //Task.WaitAll(renew);

                    //var process = Task.Run(async () => await metodoProcessamento(doc));
                    //Task.WaitAll(process);
                    //if (process.Result)

                    var process = await metodoProcessamento(doc);
                    if (process)
                    {
                        //var complete = Task.Run(async () => await receiver.CompleteAsync(msg.SystemProperties.LockToken));
                        //Task.WaitAll(complete);
                        await receiver.CompleteAsync(msg.SystemProperties.LockToken);
                    }
                    else
                    {
                        //var AbandonAsync = Task.Run(async () => await receiver.AbandonAsync(msg.SystemProperties.LockToken));
                        //Task.WaitAll(AbandonAsync);
                        await receiver.AbandonAsync(msg.SystemProperties.LockToken);
                    }
                }
        }

        public async Task<bool> EnviarRegistroFilaXml(XmlDocument objeto, string config)
        {
            var configuracao = LerParametrosFila(config);

            if (!queueClient.ContainsKey(string.Concat(configuracao.UrlFila, configuracao.NomeFila)))
                queueClient.Add(string.Concat(configuracao.UrlFila, configuracao.NomeFila), new QueueClient(configuracao.UrlFila, configuracao.NomeFila));

            var _client = queueClient[string.Concat(configuracao.UrlFila, configuracao.NomeFila)];

            await _client.SendAsync(new Message()
            {
                Body = Encoding.Default.GetBytes(objeto.OuterXml),
                ContentType = "application/xml"
            });

            return true;
        }

        private Configuracao LerParametrosFila(string nomeConfig)
        {
            var appSettings = "appsettings.json";
#if DEBUG
            appSettings = "appsettings.Development.json";
#endif

            var configuracao = new Configuracao();
            var builder = new ConfigurationBuilder()
                        .SetBasePath(Directory.GetCurrentDirectory())
                        .AddJsonFile(appSettings);
            var configuracaoAppsettings = builder.Build();

            configuracao.ProxyHost = configuracaoAppsettings.GetValue<string>("Proxy:ProxyHost");
            configuracao.ProxyPorta = configuracaoAppsettings.GetValue<int>("Proxy:ProxyPorta");

            var tipoServico = configuracaoAppsettings.GetValue(typeof(string), "ServicoMensagem");

            configuracao.UrlFila = configuracaoAppsettings.GetValue<string>(string.Format("{0}:Fila", nomeConfig));
            configuracao.QuantidadeMensagem = configuracaoAppsettings.GetValue<int>(string.Format("{0}:QuantidadeDeMensagens", nomeConfig));
            if (tipoServico == null || !tipoServico.ToString().ToLower().Equals("azure"))
            {
                configuracao.AccessKeyId = configuracaoAppsettings.GetValue<string>(string.Format("{0}:AccessKey", nomeConfig));
                configuracao.SecretAccessKey = configuracaoAppsettings.GetValue<string>(string.Format("{0}:SecretAccessKey", nomeConfig));
                configuracao.Regiao = configuracaoAppsettings.GetValue<string>(string.Format("{0}:Regiao", nomeConfig));
            }
            else
                configuracao.NomeFila = configuracaoAppsettings.GetValue<string>(string.Format("{0}:NomeFila", nomeConfig));

            return configuracao;
        }
    }

    public class Configuracao
    {
        public string ProxyHost { get; set; }
        public int ProxyPorta { get; set; }
        public string AccessKeyId { get; set; }
        public string SecretAccessKey { get; set; }
        public string Regiao { get; set; }
        public string UrlFila { get; set; }
        public int QuantidadeMensagem { get; set; }
        public string NomeFila { get; set; }
    }
}
