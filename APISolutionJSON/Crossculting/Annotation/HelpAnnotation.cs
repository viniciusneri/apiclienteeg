﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Reflection;

namespace Saraiva.SE.Crossculting.Annotation
{
    [System.AttributeUsage(AttributeTargets.Property, AllowMultiple = true)]
    public class BindProperty : System.Attribute
    {
        private string _PropertyRef;
        public string PropertyRef
        {
            get { return _PropertyRef; }
            set { _PropertyRef = value; }
        }

        public BindProperty(string Propriedade)
        {
            this._PropertyRef = Propriedade;
        }
    }

    public class Methods
    {
        public static Y GetFields<T, Y>(T objectCompare, Type pType = null)
        {
            dynamic objectReturn;
            if (pType == null)
                objectReturn = Activator.CreateInstance<Y>();
            else
                objectReturn = Activator.CreateInstance(pType);

            try
            {
                var prop = objectCompare.GetType().GetProperties();
                foreach (var it in prop)
                {
                    var tAttribute = (BindProperty)it.GetCustomAttribute(typeof(BindProperty));
                    if ((tAttribute != null))
                    {
                        var fieldIdentity = tAttribute.GetType().GetProperty("PropertyRef")
                            .GetValue(tAttribute).ToString();

                        if (!string.IsNullOrEmpty(fieldIdentity))
                        {
                            //PropertyInfo pInfoInject = objectReturn.GetType().GetProperty(fieldIdentity);

                            //if (it.PropertyType.IsGenericType)
                            //{
                            //    var typeList = it.PropertyType.GenericTypeArguments[0];
                            //    var typeDestiny = pInfoInject.PropertyType.GenericTypeArguments[0];

                            //    var vlrInclude = it.GetValue(objectCompare);
                            //    var lstTypeDestiny = new List<dynamic>();

                            //    foreach (var itemLst in (IEnumerable)vlrInclude)
                            //    {
                            //        var valueBinded = GetFields<dynamic, dynamic>(itemLst, typeDestiny);
                            //        lstTypeDestiny.Add(valueBinded);

                            //        //Tem que iniciar a lista na classe construtor
                            //        var t = pInfoInject.GetValue(objectReturn) as IList;
                            //        t.Add(valueBinded);
                            //    }
                            //}
                            //else if (it.PropertyType.IsClass && it.PropertyType.Namespace != "System")
                            //{
                            //    var typeDestiny = pInfoInject.PropertyType.UnderlyingSystemType;
                            //    var objectDestReturn = GetFields<dynamic, dynamic>(it.GetValue(objectCompare), typeDestiny);
                            //    pInfoInject.SetValue(objectReturn, objectDestReturn);
                            //}
                            //else
                            //    pInfoInject.SetValue(objectReturn, it.GetValue(objectCompare));
                        }
                    }
                }
            }
            catch { }
            return objectReturn;
        }

        public static Y GetFieldGenericExpando<Y>(ExpandoObject objectCompare, Type TObjectSecondary = null)
        {
            try
            {
                var objectLinked = TObjectSecondary == null ? Activator.CreateInstance<Y>() : Activator.CreateInstance(TObjectSecondary);

                objectLinked.GetType().GetProperties(BindingFlags.Public | BindingFlags.IgnoreCase | BindingFlags.Instance).ToList()
                    .ForEach(it =>
                    {
                        try
                        {
                            var tAttribute = (BindProperty)it.GetCustomAttribute(typeof(BindProperty));
                            if (tAttribute != null)
                            {
                                var itSelectedFound = objectCompare.Where(p => p.Key == tAttribute.PropertyRef);

                                if (tAttribute.PropertyRef.Contains("Enum"))
                                {
                                    var splitEnum = tAttribute.PropertyRef.Split('.');

                                    var enumType = Activator.CreateInstance(Type.GetType(
                                        string.Concat("Saraiva.SE.Crossculting.Enums.", splitEnum[1])))
                                        .GetType().GetMembers();

                                    var enumProperty = (FieldInfo)enumType.ToList().Where(pi => pi.Name.Contains(splitEnum[2])).ToList()[0];
                                    var valueEnum = (int)enumProperty.GetValue(null);
                                    it.SetValue(objectLinked, Convert.ChangeType(valueEnum, it.PropertyType));
                                }
                                else if (tAttribute.PropertyRef.Contains("Const"))
                                {
                                    var splitConst = tAttribute.PropertyRef.Split('.');
                                    var valueConst = splitConst[2];
                                    it.SetValue(objectLinked, Convert.ChangeType(valueConst, it.PropertyType));
                                }
                                else if (tAttribute.PropertyRef.Contains("Object"))
                                {
                                    var splitObject = tAttribute.PropertyRef.Split('.');
                                    var valueObject = splitObject[1];
                                    var objectLocalized = (ExpandoObject)objectCompare.Where(p => p.Key == valueObject).ToList()?[0].Value;
                                    var valueObjectInput = objectLocalized.Where(itT => itT.Key == splitObject[2])?.ToList()[0].Value;
                                    it.SetValue(objectLinked, Convert.ChangeType(valueObjectInput, it.PropertyType));
                                }
                                else if (tAttribute.PropertyRef.Contains("Collection"))
                                {
                                    var splitObject = tAttribute.PropertyRef.Split('.');
                                    var valueCollection = splitObject[1];
                                    var collectionLocalized = (IList)objectCompare.Where(p => p.Key == valueCollection).ToList()?[0].Value;

                                    object instance = Activator.CreateInstance(it.PropertyType);
                                    IList listObjects = (IList)instance;

                                    foreach (ExpandoObject itLst in collectionLocalized)
                                    {
                                        var objectListItem = GetFieldGenericExpando<dynamic>(itLst, it.PropertyType.GenericTypeArguments[0]);
                                        listObjects.Add(objectListItem);
                                    }

                                    it.SetValue(objectLinked, listObjects, null);
                                }
                                else
                                {
                                    var selectItem = itSelectedFound.Count() > 0;

                                    if (selectItem)
                                    {
                                        var valueItem = itSelectedFound.ToList()[0].Value;
                                        it.SetValue(objectLinked, Convert.ChangeType(valueItem, it.PropertyType));
                                    }
                                }
                            }
                        }
                        catch (Exception ex) { }
                    });

                return (dynamic)objectLinked;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }

    public static class Extensions
    {
        public static ExpandoObject ToDynamic(this object value)
        {
            IDictionary<string, object> expando = new ExpandoObject();

            foreach (var property in value.GetType().GetProperties())
                expando.Add(property.Name, property.GetValue(value));

            return expando as ExpandoObject;
        }

    }

}
