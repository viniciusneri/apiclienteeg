﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using Saraiva.SE.Crossculting.Logs;
using Saraiva.SE.Crossculting.Service;
using System;
using System.Linq;

namespace Saraiva.SE.Crossculting.Annotation
{
    public class TrackingLog : ActionFilterAttribute
    {
        private string Collection { get; set; }
        private Log Logger { get; set; }
        private object Inputs { get; set; }
        private object Keys { get; set; }

        public TrackingLog(string collection)
        {
            Collection = collection;
            Logger = new Log();
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {

            Inputs = context.ActionArguments.FirstOrDefault().Value;
            Keys = context.ActionArguments.FirstOrDefault().Key;

            Logger.Controller = context.RouteData.Values["Controller"].ToString().Trim();
            Logger.Request = JsonConvert.SerializeObject(context.ActionArguments);
            Logger.Action = context.RouteData.Values["Action"].ToString().Trim();
            Logger.Date = DateTime.Now;

            switch (Logger.Controller.ToUpper())
            {
                case "CANCEL":
                    LogCancel();
                    break;
                case "TROCADEVOLUCAO":
                case "CARTARECLAMATORIA":
                case "LOGISTICAREVERSA":
                    LogDevolution();
                    break;
                case "REFUND":
                    LogRefund();
                    break;
                case "NOTAFISCAL":
                    LogOrder();
                    break;
            }

            base.OnActionExecuting(context);
        }

        public override void OnResultExecuting(ResultExecutingContext context)
        {

            var type = context.Result.GetType().FullName;

            if (type == "Microsoft.AspNetCore.Mvc.BadRequestObjectResult")
            {
                var badRequest = (BadRequestObjectResult)context.Result;
                Logger.IsError = true;
                Logger.Menssage = badRequest.Value.ToString();
                Logger.Response = JsonConvert.SerializeObject(context.Result);
            }
            else if (type == "Microsoft.AspNetCore.Mvc.OkObjectResult")
            {
                var Ok = (OkObjectResult)context.Result;

                Logger.Response = JsonConvert.SerializeObject(Ok.Value);
            }
            else
            {
                var InternalError = (ContentResult)context.Result;

                Logger.Response = JsonConvert.SerializeObject(InternalError);
                Logger.Menssage = InternalError?.Content.ToString();
            }

            ServiceCross.Insert<Log>(Logger, Collection);

            base.OnResultExecuting(context);
        }

        private void LogCancel()
        {
            Logger.OrderId = Convert.ToInt32(((dynamic)Inputs)?.OrderId);
        }

        private void LogDevolution()
        {
            try
            {
                Logger.OrderId = Convert.ToInt32(((dynamic)Inputs)?.OrderId);
            }
            catch
            {
                try
                {
                    Logger.key = Convert.ToString(((dynamic)Inputs).NotaCuponada.Data.NumeroCupom);
                }
                catch
                {
                    try
                    {
                        if (Keys.ToString().ToLower() == "devolutionid")
                            Logger.key = Convert.ToString(Inputs);
                    }
                    catch
                    {
                        Logger.OrderId = Convert.ToInt32(Inputs);
                    }
                }
            }
        }

        private void LogRefund()
        {
            try
            {
                Logger.OrderId = Convert.ToInt32(((dynamic)Inputs)?.OrderId);
            }
            catch
            {
                try
                {
                    Logger.OrderId = Convert.ToInt32(Inputs);
                }
                catch
                {
                    Logger.key = "Serviço ExecuteServiceBus";
                }

            }

        }

        private void LogOrder()
        {
            Logger.key = Convert.ToString(((dynamic)Inputs).Data.NumeroCupom);
        }
    }
}
