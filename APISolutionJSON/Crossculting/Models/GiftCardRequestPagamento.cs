﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Saraiva.SE.Crossculting.Models
{
    public class GiftCardRequestPagamento
    {
        //@pDataLancto - Data
        //@pVlrLancto - Valor
        //@pUsu_id - id do usuário
        //@pNroPdv  - pdv
        //@pFil_id - filial
        //@pGCD_id - Cód.Gift Card
        //@pNroDocto - nro nf ou do cupom fiscal
        //@pSerie - série nf
        //@pCdOp - cód.op.pdv
        //@pOffLine - S/N(off-line?)
        //@pPin - pin impresso no cartao


        [JsonProperty(PropertyName = "DataLancamento")]
        public DateTime EGC_DTLANCTO { get; set; }

        [JsonProperty(PropertyName = "ValorLancamento")]
        public decimal EGC_VLRLANCTO { get; set; }

        [JsonProperty(PropertyName = "UserId")]
        public int EGC_USU_ID { get; set; }

        [JsonProperty(PropertyName = "NroPdv")]
        public int EGC_NROPDV { get; set; }

        [JsonProperty(PropertyName = "FilialId")]
        public int EGC_FIL_ID { get; set; }

        [JsonProperty(PropertyName = "GiftCardId")]
        public int GCD_ID { get; set; }

        [JsonProperty(PropertyName = "NroDocumentoNfCf")]
        public int EGC_NRODOCTO { get; set; }

        [JsonProperty(PropertyName = "SserieNf")]
        public string EGC_SERIE { get; set; }

        [JsonProperty(PropertyName = "CodOpPdv")]
        public int EGC_CDOPERADORPDV { get; set; }

        [JsonProperty(PropertyName = "SerialNumber")]
        public string GCD_CodBarras { get; set; }

        [JsonProperty(PropertyName = "PinCartao")]
        public string GCD_PIN { get; set; }
    }
}


