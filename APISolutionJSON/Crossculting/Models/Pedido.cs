﻿using System.Collections.Generic;
using System.Xml.Serialization;

namespace Saraiva.SE.Crossculting.Models
{
    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Pedido
    {

        private int idPedidoField;

        private bool idPedidoFieldSpecified;

        private bool clickCollectField;

        private bool clickCollectFieldSpecified;

        private System.DateTime dataPedidoField;

        private System.DateTime dataAceiteField;

        private bool dataAceiteFieldSpecified;

        private int idStatusField;

        private bool idStatusFieldSpecified;

        private string statusField;

        private int idMotivoField;

        private bool idMotivoFieldSpecified;

        private string motivoField;

        private int idClienteField;

        private bool idClienteFieldSpecified;

        private string idClienteCriptoField;

        private int idFilialField;

        private bool trocaField;

        private bool trocaFieldSpecified;

        private int idPedidoOriginalField;

        private bool idPedidoOriginalFieldSpecified;

        private int idPacField;

        private bool idPacFieldSpecified;

        private int origemField;

        private bool origemFieldSpecified;

        private int idOperadorField;

        private bool idOperadorFieldSpecified;

        private int idAprovadorField;

        private bool idAprovadorFieldSpecified;

        private bool encomendaDigipixField;

        private bool encomendaDigipixFieldSpecified;

        private bool encomendaUmCliqueField;

        private bool encomendaUmCliqueFieldSpecified;

        private bool ativarUmCliqueField;

        private bool ativarUmCliqueFieldSpecified;

        private Cliente clienteField;

        private Telefone telefoneSMSField;

        private Carrinho carrinhoField;

        private Endereco enderecoCobrancaField;

        private Endereco enderecoEntregaField;

        private Entregas entregasField;

        private decimal totalPedidoField;

        private Pagamento pagamentoField;

        private List<Cupom> cuponsField;

        private PrazoFrete postagemField;

        private PrazoFrete entregaField;

        private EntregaAgendada entregaAgendadaField;

        private Faturamentos faturamentosField;

        private List<Devolucao> devolucoesField;

        private List<Estorno> estornosField;

        /// <remarks/>
        public int IdPedido
        {
            get
            {
                return this.idPedidoField;
            }
            set
            {
                this.idPedidoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPedidoSpecified
        {
            get
            {
                return this.idPedidoFieldSpecified;
            }
            set
            {
                this.idPedidoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool ClickCollect
        {
            get
            {
                return this.clickCollectField;
            }
            set
            {
                this.clickCollectField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ClickCollectSpecified
        {
            get
            {
                return this.clickCollectFieldSpecified;
            }
            set
            {
                this.clickCollectFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataPedido
        {
            get
            {
                return this.dataPedidoField;
            }
            set
            {
                this.dataPedidoField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataAceite
        {
            get
            {
                return this.dataAceiteField;
            }
            set
            {
                this.dataAceiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataAceiteSpecified
        {
            get
            {
                return this.dataAceiteFieldSpecified;
            }
            set
            {
                this.dataAceiteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdStatus
        {
            get
            {
                return this.idStatusField;
            }
            set
            {
                this.idStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdStatusSpecified
        {
            get
            {
                return this.idStatusFieldSpecified;
            }
            set
            {
                this.idStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public int IdMotivo
        {
            get
            {
                return this.idMotivoField;
            }
            set
            {
                this.idMotivoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdMotivoSpecified
        {
            get
            {
                return this.idMotivoFieldSpecified;
            }
            set
            {
                this.idMotivoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Motivo
        {
            get
            {
                return this.motivoField;
            }
            set
            {
                this.motivoField = value;
            }
        }

        /// <remarks/>
        public int IdCliente
        {
            get
            {
                return this.idClienteField;
            }
            set
            {
                this.idClienteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdClienteSpecified
        {
            get
            {
                return this.idClienteFieldSpecified;
            }
            set
            {
                this.idClienteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string IdClienteCripto
        {
            get
            {
                return this.idClienteCriptoField;
            }
            set
            {
                this.idClienteCriptoField = value;
            }
        }

        /// <remarks/>
        public int IdFilial
        {
            get
            {
                return this.idFilialField;
            }
            set
            {
                this.idFilialField = value;
            }
        }

        /// <remarks/>
        public bool Troca
        {
            get
            {
                return this.trocaField;
            }
            set
            {
                this.trocaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TrocaSpecified
        {
            get
            {
                return this.trocaFieldSpecified;
            }
            set
            {
                this.trocaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdPedidoOriginal
        {
            get
            {
                return this.idPedidoOriginalField;
            }
            set
            {
                this.idPedidoOriginalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPedidoOriginalSpecified
        {
            get
            {
                return this.idPedidoOriginalFieldSpecified;
            }
            set
            {
                this.idPedidoOriginalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdPac
        {
            get
            {
                return this.idPacField;
            }
            set
            {
                this.idPacField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPacSpecified
        {
            get
            {
                return this.idPacFieldSpecified;
            }
            set
            {
                this.idPacFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Origem
        {
            get
            {
                return this.origemField;
            }
            set
            {
                this.origemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OrigemSpecified
        {
            get
            {
                return this.origemFieldSpecified;
            }
            set
            {
                this.origemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdOperador
        {
            get
            {
                return this.idOperadorField;
            }
            set
            {
                this.idOperadorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdOperadorSpecified
        {
            get
            {
                return this.idOperadorFieldSpecified;
            }
            set
            {
                this.idOperadorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdAprovador
        {
            get
            {
                return this.idAprovadorField;
            }
            set
            {
                this.idAprovadorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdAprovadorSpecified
        {
            get
            {
                return this.idAprovadorFieldSpecified;
            }
            set
            {
                this.idAprovadorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool EncomendaDigipix
        {
            get
            {
                return this.encomendaDigipixField;
            }
            set
            {
                this.encomendaDigipixField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EncomendaDigipixSpecified
        {
            get
            {
                return this.encomendaDigipixFieldSpecified;
            }
            set
            {
                this.encomendaDigipixFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool EncomendaUmClique
        {
            get
            {
                return this.encomendaUmCliqueField;
            }
            set
            {
                this.encomendaUmCliqueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EncomendaUmCliqueSpecified
        {
            get
            {
                return this.encomendaUmCliqueFieldSpecified;
            }
            set
            {
                this.encomendaUmCliqueFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool AtivarUmClique
        {
            get
            {
                return this.ativarUmCliqueField;
            }
            set
            {
                this.ativarUmCliqueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AtivarUmCliqueSpecified
        {
            get
            {
                return this.ativarUmCliqueFieldSpecified;
            }
            set
            {
                this.ativarUmCliqueFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Cliente Cliente
        {
            get
            {
                return this.clienteField;
            }
            set
            {
                this.clienteField = value;
            }
        }

        /// <remarks/>
        public Telefone TelefoneSMS
        {
            get
            {
                return this.telefoneSMSField;
            }
            set
            {
                this.telefoneSMSField = value;
            }
        }

        /// <remarks/>
        public Carrinho Carrinho
        {
            get
            {
                return this.carrinhoField;
            }
            set
            {
                this.carrinhoField = value;
            }
        }

        /// <remarks/>
        public Endereco EnderecoCobranca
        {
            get
            {
                return this.enderecoCobrancaField;
            }
            set
            {
                this.enderecoCobrancaField = value;
            }
        }

        /// <remarks/>
        public Endereco EnderecoEntrega
        {
            get
            {
                return this.enderecoEntregaField;
            }
            set
            {
                this.enderecoEntregaField = value;
            }
        }

        /// <remarks/>
        public Entregas Entregas
        {
            get
            {
                return this.entregasField;
            }
            set
            {
                this.entregasField = value;
            }
        }

        /// <remarks/>
        public decimal TotalPedido
        {
            get
            {
                return this.totalPedidoField;
            }
            set
            {
                this.totalPedidoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public Pagamento Pagamento
        {
            get
            {
                return this.pagamentoField;
            }
            set
            {
                this.pagamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Cupom> Cupons
        {
            get
            {
                return this.cuponsField;
            }
            set
            {
                this.cuponsField = value;
            }
        }

        /// <remarks/>
        public PrazoFrete Postagem
        {
            get
            {
                return this.postagemField;
            }
            set
            {
                this.postagemField = value;
            }
        }

        /// <remarks/>
        public PrazoFrete Entrega
        {
            get
            {
                return this.entregaField;
            }
            set
            {
                this.entregaField = value;
            }
        }

        /// <remarks/>
        public EntregaAgendada EntregaAgendada
        {
            get
            {
                return this.entregaAgendadaField;
            }
            set
            {
                this.entregaAgendadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public Faturamentos Faturamentos
        {
            get
            {
                return this.faturamentosField;
            }
            set
            {
                this.faturamentosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Devolucao> Devolucoes
        {
            get
            {
                return this.devolucoesField;
            }
            set
            {
                this.devolucoesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Estorno> Estornos
        {
            get
            {
                return this.estornosField;
            }
            set
            {
                this.estornosField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Cliente
    {

        private int idClienteField;

        private bool idClienteFieldSpecified;

        private int idFilialCadastroField;

        private bool idFilialCadastroFieldSpecified;

        private string nomeField;

        private string loginField;

        private string emailField;

        private string senhaField;

        private bool pessoaJuridicaField;

        private bool pessoaJuridicaFieldSpecified;

        private Documento documentoField;

        private Perfil perfilField;

        private ListaEndereco enderecosField;

        private bool ativoField;

        private bool ativoFieldSpecified;

        private int origemField;

        private bool origemFieldSpecified;

        private bool colaboradorField;

        private bool colaboradorFieldSpecified;

        private string umCliqueField;

        private bool contribuinteField;

        private bool contribuinteFieldSpecified;

        private int idSaraivaPlusField;

        private bool idSaraivaPlusFieldSpecified;

        /// <remarks/>
        public int IdCliente
        {
            get
            {
                return this.idClienteField;
            }
            set
            {
                this.idClienteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdClienteSpecified
        {
            get
            {
                return this.idClienteFieldSpecified;
            }
            set
            {
                this.idClienteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdFilialCadastro
        {
            get
            {
                return this.idFilialCadastroField;
            }
            set
            {
                this.idFilialCadastroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdFilialCadastroSpecified
        {
            get
            {
                return this.idFilialCadastroFieldSpecified;
            }
            set
            {
                this.idFilialCadastroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public string Login
        {
            get
            {
                return this.loginField;
            }
            set
            {
                this.loginField = value;
            }
        }

        /// <remarks/>
        public string Email
        {
            get
            {
                return this.emailField;
            }
            set
            {
                this.emailField = value;
            }
        }

        /// <remarks/>
        public string Senha
        {
            get
            {
                return this.senhaField;
            }
            set
            {
                this.senhaField = value;
            }
        }

        /// <remarks/>
        public bool PessoaJuridica
        {
            get
            {
                return this.pessoaJuridicaField;
            }
            set
            {
                this.pessoaJuridicaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PessoaJuridicaSpecified
        {
            get
            {
                return this.pessoaJuridicaFieldSpecified;
            }
            set
            {
                this.pessoaJuridicaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Documento Documento
        {
            get
            {
                return this.documentoField;
            }
            set
            {
                this.documentoField = value;
            }
        }

        /// <remarks/>
        public Perfil Perfil
        {
            get
            {
                return this.perfilField;
            }
            set
            {
                this.perfilField = value;
            }
        }

        /// <remarks/>
        public ListaEndereco Enderecos
        {
            get
            {
                return this.enderecosField;
            }
            set
            {
                this.enderecosField = value;
            }
        }

        /// <remarks/>
        public bool Ativo
        {
            get
            {
                return this.ativoField;
            }
            set
            {
                this.ativoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AtivoSpecified
        {
            get
            {
                return this.ativoFieldSpecified;
            }
            set
            {
                this.ativoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Origem
        {
            get
            {
                return this.origemField;
            }
            set
            {
                this.origemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OrigemSpecified
        {
            get
            {
                return this.origemFieldSpecified;
            }
            set
            {
                this.origemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Colaborador
        {
            get
            {
                return this.colaboradorField;
            }
            set
            {
                this.colaboradorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ColaboradorSpecified
        {
            get
            {
                return this.colaboradorFieldSpecified;
            }
            set
            {
                this.colaboradorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string UmClique
        {
            get
            {
                return this.umCliqueField;
            }
            set
            {
                this.umCliqueField = value;
            }
        }

        /// <remarks/>
        public bool Contribuinte
        {
            get
            {
                return this.contribuinteField;
            }
            set
            {
                this.contribuinteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ContribuinteSpecified
        {
            get
            {
                return this.contribuinteFieldSpecified;
            }
            set
            {
                this.contribuinteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdSaraivaPlus
        {
            get
            {
                return this.idSaraivaPlusField;
            }
            set
            {
                this.idSaraivaPlusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSaraivaPlusSpecified
        {
            get
            {
                return this.idSaraivaPlusFieldSpecified;
            }
            set
            {
                this.idSaraivaPlusFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Documento
    {

        private string cPFField;

        private string rgField;

        private string cNPJField;

        private string inscricaoEstadualField;

        private string inscricaoMunicipalField;

        private string passaporteField;

        private string crachaField;

        private string centroCustoField;

        /// <remarks/>
        public string CPF
        {
            get
            {
                return this.cPFField;
            }
            set
            {
                this.cPFField = value;
            }
        }

        /// <remarks/>
        public string RG
        {
            get
            {
                return this.rgField;
            }
            set
            {
                this.rgField = value;
            }
        }

        /// <remarks/>
        public string CNPJ
        {
            get
            {
                return this.cNPJField;
            }
            set
            {
                this.cNPJField = value;
            }
        }

        /// <remarks/>
        public string InscricaoEstadual
        {
            get
            {
                return this.inscricaoEstadualField;
            }
            set
            {
                this.inscricaoEstadualField = value;
            }
        }

        /// <remarks/>
        public string InscricaoMunicipal
        {
            get
            {
                return this.inscricaoMunicipalField;
            }
            set
            {
                this.inscricaoMunicipalField = value;
            }
        }

        /// <remarks/>
        public string Passaporte
        {
            get
            {
                return this.passaporteField;
            }
            set
            {
                this.passaporteField = value;
            }
        }

        /// <remarks/>
        public string Cracha
        {
            get
            {
                return this.crachaField;
            }
            set
            {
                this.crachaField = value;
            }
        }

        /// <remarks/>
        public string CentroCusto
        {
            get
            {
                return this.centroCustoField;
            }
            set
            {
                this.centroCustoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Devolucao
    {

        private int idField;

        private bool idFieldSpecified;

        private int idPedidoTrocaField;

        private bool idPedidoTrocaFieldSpecified;

        private bool canceladoField;

        private bool canceladoFieldSpecified;

        private int idProdutoField;

        private bool idProdutoFieldSpecified;

        private int quantidadeField;

        private bool quantidadeFieldSpecified;

        private bool conferenciaField;

        private bool conferenciaFieldSpecified;

        private bool aceiteField;

        private bool aceiteFieldSpecified;

        private int idMotivoField;

        private bool idMotivoFieldSpecified;

        private string motivoClienteField;

        private bool recebimentoField;

        private bool recebimentoFieldSpecified;

        private System.DateTime dataRecebimentoField;

        private bool dataRecebimentoFieldSpecified;

        private System.DateTime dataConferenciaField;

        private bool dataConferenciaFieldSpecified;

        private System.DateTime dataAceiteField;

        private bool dataAceiteFieldSpecified;

        private System.DateTime dataEstornoField;

        private bool dataEstornoFieldSpecified;

        private bool trocaField;

        private bool trocaFieldSpecified;

        /// <remarks/>
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSpecified
        {
            get
            {
                return this.idFieldSpecified;
            }
            set
            {
                this.idFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdPedidoTroca
        {
            get
            {
                return this.idPedidoTrocaField;
            }
            set
            {
                this.idPedidoTrocaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPedidoTrocaSpecified
        {
            get
            {
                return this.idPedidoTrocaFieldSpecified;
            }
            set
            {
                this.idPedidoTrocaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Cancelado
        {
            get
            {
                return this.canceladoField;
            }
            set
            {
                this.canceladoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CanceladoSpecified
        {
            get
            {
                return this.canceladoFieldSpecified;
            }
            set
            {
                this.canceladoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdProduto
        {
            get
            {
                return this.idProdutoField;
            }
            set
            {
                this.idProdutoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdProdutoSpecified
        {
            get
            {
                return this.idProdutoFieldSpecified;
            }
            set
            {
                this.idProdutoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Quantidade
        {
            get
            {
                return this.quantidadeField;
            }
            set
            {
                this.quantidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeSpecified
        {
            get
            {
                return this.quantidadeFieldSpecified;
            }
            set
            {
                this.quantidadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Conferencia
        {
            get
            {
                return this.conferenciaField;
            }
            set
            {
                this.conferenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ConferenciaSpecified
        {
            get
            {
                return this.conferenciaFieldSpecified;
            }
            set
            {
                this.conferenciaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Aceite
        {
            get
            {
                return this.aceiteField;
            }
            set
            {
                this.aceiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AceiteSpecified
        {
            get
            {
                return this.aceiteFieldSpecified;
            }
            set
            {
                this.aceiteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdMotivo
        {
            get
            {
                return this.idMotivoField;
            }
            set
            {
                this.idMotivoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdMotivoSpecified
        {
            get
            {
                return this.idMotivoFieldSpecified;
            }
            set
            {
                this.idMotivoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string MotivoCliente
        {
            get
            {
                return this.motivoClienteField;
            }
            set
            {
                this.motivoClienteField = value;
            }
        }

        /// <remarks/>
        public bool Recebimento
        {
            get
            {
                return this.recebimentoField;
            }
            set
            {
                this.recebimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool RecebimentoSpecified
        {
            get
            {
                return this.recebimentoFieldSpecified;
            }
            set
            {
                this.recebimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataRecebimento
        {
            get
            {
                return this.dataRecebimentoField;
            }
            set
            {
                this.dataRecebimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataRecebimentoSpecified
        {
            get
            {
                return this.dataRecebimentoFieldSpecified;
            }
            set
            {
                this.dataRecebimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataConferencia
        {
            get
            {
                return this.dataConferenciaField;
            }
            set
            {
                this.dataConferenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataConferenciaSpecified
        {
            get
            {
                return this.dataConferenciaFieldSpecified;
            }
            set
            {
                this.dataConferenciaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataAceite
        {
            get
            {
                return this.dataAceiteField;
            }
            set
            {
                this.dataAceiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataAceiteSpecified
        {
            get
            {
                return this.dataAceiteFieldSpecified;
            }
            set
            {
                this.dataAceiteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataEstorno
        {
            get
            {
                return this.dataEstornoField;
            }
            set
            {
                this.dataEstornoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataEstornoSpecified
        {
            get
            {
                return this.dataEstornoFieldSpecified;
            }
            set
            {
                this.dataEstornoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Troca
        {
            get
            {
                return this.trocaField;
            }
            set
            {
                this.trocaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TrocaSpecified
        {
            get
            {
                return this.trocaFieldSpecified;
            }
            set
            {
                this.trocaFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Cupom
    {

        private int idCupomField;

        private bool idCupomFieldSpecified;

        private string codigoCupomField;

        private CupomEscopo escopoField;

        private bool escopoFieldSpecified;

        private int idItemField;

        private bool idItemFieldSpecified;

        /// <remarks/>
        public int IdCupom
        {
            get
            {
                return this.idCupomField;
            }
            set
            {
                this.idCupomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCupomSpecified
        {
            get
            {
                return this.idCupomFieldSpecified;
            }
            set
            {
                this.idCupomFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string CodigoCupom
        {
            get
            {
                return this.codigoCupomField;
            }
            set
            {
                this.codigoCupomField = value;
            }
        }

        /// <remarks/>
        public CupomEscopo Escopo
        {
            get
            {
                return this.escopoField;
            }
            set
            {
                this.escopoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EscopoSpecified
        {
            get
            {
                return this.escopoFieldSpecified;
            }
            set
            {
                this.escopoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdItem
        {
            get
            {
                return this.idItemField;
            }
            set
            {
                this.idItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdItemSpecified
        {
            get
            {
                return this.idItemFieldSpecified;
            }
            set
            {
                this.idItemFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum CupomEscopo
    {

        /// <remarks/>
        Pedido,

        /// <remarks/>
        Item,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class MarketPlaceReverso
    {

        private string codigoField;

        private int parcelasField;

        private decimal porcentagemDescontoField;

        private decimal valorMinimoParcelaField;

        private decimal jurosAoMesField;

        /// <remarks/>
        public string Codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        /// <remarks/>
        public int Parcelas
        {
            get
            {
                return this.parcelasField;
            }
            set
            {
                this.parcelasField = value;
            }
        }

        /// <remarks/>
        public decimal PorcentagemDesconto
        {
            get
            {
                return this.porcentagemDescontoField;
            }
            set
            {
                this.porcentagemDescontoField = value;
            }
        }

        /// <remarks/>
        public decimal ValorMinimoParcela
        {
            get
            {
                return this.valorMinimoParcelaField;
            }
            set
            {
                this.valorMinimoParcelaField = value;
            }
        }

        /// <remarks/>
        public decimal JurosAoMes
        {
            get
            {
                return this.jurosAoMesField;
            }
            set
            {
                this.jurosAoMesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class PagamentoLoja
    {

        private string codigoField;

        /// <remarks/>
        public string Codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Premio
    {

        private int pontosField;

        private decimal valorField;

        /// <remarks/>
        public int Pontos
        {
            get
            {
                return this.pontosField;
            }
            set
            {
                this.pontosField = value;
            }
        }

        /// <remarks/>
        public decimal Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(SaraivaPlus))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ProgramaRecompensa
    {

        private int idClienteField;

        private int pontosField;

        private bool pontosFieldSpecified;

        private Premio premioField;

        private bool ativoField;

        private bool ativoFieldSpecified;

        /// <remarks/>
        public int IdCliente
        {
            get
            {
                return this.idClienteField;
            }
            set
            {
                this.idClienteField = value;
            }
        }

        /// <remarks/>
        public int Pontos
        {
            get
            {
                return this.pontosField;
            }
            set
            {
                this.pontosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PontosSpecified
        {
            get
            {
                return this.pontosFieldSpecified;
            }
            set
            {
                this.pontosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Premio Premio
        {
            get
            {
                return this.premioField;
            }
            set
            {
                this.premioField = value;
            }
        }

        /// <remarks/>
        public bool Ativo
        {
            get
            {
                return this.ativoField;
            }
            set
            {
                this.ativoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AtivoSpecified
        {
            get
            {
                return this.ativoFieldSpecified;
            }
            set
            {
                this.ativoFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ValeCultura
    {

        private int idOperadoraField;

        private bool idOperadoraFieldSpecified;

        private ValeCulturaOperadora operadoraField;

        private bool operadoraFieldSpecified;

        private string nomeField;

        private string numeroField;

        private string codigoSegurancaField;

        private string numeroMascaraField;

        private string numeroCripto1Field;

        private string numeroCripto2Field;

        private int parcelasField;

        private decimal porcentagemDescontoField;

        private decimal valorMinimoParcelaField;

        private decimal jurosAoMesField;

        /// <remarks/>
        public int IdOperadora
        {
            get
            {
                return this.idOperadoraField;
            }
            set
            {
                this.idOperadoraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdOperadoraSpecified
        {
            get
            {
                return this.idOperadoraFieldSpecified;
            }
            set
            {
                this.idOperadoraFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ValeCulturaOperadora Operadora
        {
            get
            {
                return this.operadoraField;
            }
            set
            {
                this.operadoraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OperadoraSpecified
        {
            get
            {
                return this.operadoraFieldSpecified;
            }
            set
            {
                this.operadoraFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public string Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        public string CodigoSeguranca
        {
            get
            {
                return this.codigoSegurancaField;
            }
            set
            {
                this.codigoSegurancaField = value;
            }
        }

        /// <remarks/>
        public string NumeroMascara
        {
            get
            {
                return this.numeroMascaraField;
            }
            set
            {
                this.numeroMascaraField = value;
            }
        }

        /// <remarks/>
        public string NumeroCripto1
        {
            get
            {
                return this.numeroCripto1Field;
            }
            set
            {
                this.numeroCripto1Field = value;
            }
        }

        /// <remarks/>
        public string NumeroCripto2
        {
            get
            {
                return this.numeroCripto2Field;
            }
            set
            {
                this.numeroCripto2Field = value;
            }
        }

        /// <remarks/>
        public int Parcelas
        {
            get
            {
                return this.parcelasField;
            }
            set
            {
                this.parcelasField = value;
            }
        }

        /// <remarks/>
        public decimal PorcentagemDesconto
        {
            get
            {
                return this.porcentagemDescontoField;
            }
            set
            {
                this.porcentagemDescontoField = value;
            }
        }

        /// <remarks/>
        public decimal ValorMinimoParcela
        {
            get
            {
                return this.valorMinimoParcelaField;
            }
            set
            {
                this.valorMinimoParcelaField = value;
            }
        }

        /// <remarks/>
        public decimal JurosAoMes
        {
            get
            {
                return this.jurosAoMesField;
            }
            set
            {
                this.jurosAoMesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum ValeCulturaOperadora
    {

        /// <remarks/>
        Ticket,

        /// <remarks/>
        Sodexo,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Boleto
    {

        private int idBancoField;

        private bool idBancoFieldSpecified;

        private BoletoBanco bancoField;

        private bool bancoFieldSpecified;

        private string codigoField;

        private System.DateTime vencimentoField;

        private bool vencimentoFieldSpecified;

        private string nossoNumeroField;

        private int parcelasField;

        private decimal porcentagemDescontoField;

        private decimal valorMinimoParcelaField;

        private decimal jurosAoMesField;

        /// <remarks/>
        public int IdBanco
        {
            get
            {
                return this.idBancoField;
            }
            set
            {
                this.idBancoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdBancoSpecified
        {
            get
            {
                return this.idBancoFieldSpecified;
            }
            set
            {
                this.idBancoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public BoletoBanco Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BancoSpecified
        {
            get
            {
                return this.bancoFieldSpecified;
            }
            set
            {
                this.bancoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime Vencimento
        {
            get
            {
                return this.vencimentoField;
            }
            set
            {
                this.vencimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VencimentoSpecified
        {
            get
            {
                return this.vencimentoFieldSpecified;
            }
            set
            {
                this.vencimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NossoNumero
        {
            get
            {
                return this.nossoNumeroField;
            }
            set
            {
                this.nossoNumeroField = value;
            }
        }

        /// <remarks/>
        public int Parcelas
        {
            get
            {
                return this.parcelasField;
            }
            set
            {
                this.parcelasField = value;
            }
        }

        /// <remarks/>
        public decimal PorcentagemDesconto
        {
            get
            {
                return this.porcentagemDescontoField;
            }
            set
            {
                this.porcentagemDescontoField = value;
            }
        }

        /// <remarks/>
        public decimal ValorMinimoParcela
        {
            get
            {
                return this.valorMinimoParcelaField;
            }
            set
            {
                this.valorMinimoParcelaField = value;
            }
        }

        /// <remarks/>
        public decimal JurosAoMes
        {
            get
            {
                return this.jurosAoMesField;
            }
            set
            {
                this.jurosAoMesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum BoletoBanco
    {

        /// <remarks/>
        Itau,

        /// <remarks/>
        Santander,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class DebitoOnline
    {

        private int idBancoField;

        private bool idBancoFieldSpecified;

        private DebitoOnlineBanco bancoField;

        private bool bancoFieldSpecified;

        private string codigoField;

        private System.DateTime vencimentoField;

        private bool vencimentoFieldSpecified;

        private int parcelasField;

        private decimal porcentagemDescontoField;

        private decimal valorMinimoParcelaField;

        private decimal jurosAoMesField;

        /// <remarks/>
        public int IdBanco
        {
            get
            {
                return this.idBancoField;
            }
            set
            {
                this.idBancoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdBancoSpecified
        {
            get
            {
                return this.idBancoFieldSpecified;
            }
            set
            {
                this.idBancoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public DebitoOnlineBanco Banco
        {
            get
            {
                return this.bancoField;
            }
            set
            {
                this.bancoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BancoSpecified
        {
            get
            {
                return this.bancoFieldSpecified;
            }
            set
            {
                this.bancoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Codigo
        {
            get
            {
                return this.codigoField;
            }
            set
            {
                this.codigoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime Vencimento
        {
            get
            {
                return this.vencimentoField;
            }
            set
            {
                this.vencimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VencimentoSpecified
        {
            get
            {
                return this.vencimentoFieldSpecified;
            }
            set
            {
                this.vencimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Parcelas
        {
            get
            {
                return this.parcelasField;
            }
            set
            {
                this.parcelasField = value;
            }
        }

        /// <remarks/>
        public decimal PorcentagemDesconto
        {
            get
            {
                return this.porcentagemDescontoField;
            }
            set
            {
                this.porcentagemDescontoField = value;
            }
        }

        /// <remarks/>
        public decimal ValorMinimoParcela
        {
            get
            {
                return this.valorMinimoParcelaField;
            }
            set
            {
                this.valorMinimoParcelaField = value;
            }
        }

        /// <remarks/>
        public decimal JurosAoMes
        {
            get
            {
                return this.jurosAoMesField;
            }
            set
            {
                this.jurosAoMesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum DebitoOnlineBanco
    {

        /// <remarks/>
        Itau,

        /// <remarks/>
        Bradesco,

        /// <remarks/>
        BB,

        /// <remarks/>
        Cielo,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class CartaoCredito
    {

        private int idCartaoField;

        private bool idCartaoFieldSpecified;

        private string tokenField;

        private int idBandeiraField;

        private bool idBandeiraFieldSpecified;

        private CartaoCreditoBandeira bandeiraField;

        private bool bandeiraFieldSpecified;

        private string nomeField;

        private string numeroField;

        private string numeroMascaraField;

        private string numeroCripto1Field;

        private string numeroCripto2Field;

        private string cVVField;

        private int validadeMesField;

        private int validadeAnoField;

        private int parcelasField;

        private decimal porcentagemDescontoField;

        private decimal valorMinimoParcelaField;

        private decimal jurosAoMesField;

        /// <remarks/>
        public int IdCartao
        {
            get
            {
                return this.idCartaoField;
            }
            set
            {
                this.idCartaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCartaoSpecified
        {
            get
            {
                return this.idCartaoFieldSpecified;
            }
            set
            {
                this.idCartaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Token
        {
            get
            {
                return this.tokenField;
            }
            set
            {
                this.tokenField = value;
            }
        }

        /// <remarks/>
        public int IdBandeira
        {
            get
            {
                return this.idBandeiraField;
            }
            set
            {
                this.idBandeiraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdBandeiraSpecified
        {
            get
            {
                return this.idBandeiraFieldSpecified;
            }
            set
            {
                this.idBandeiraFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CartaoCreditoBandeira Bandeira
        {
            get
            {
                return this.bandeiraField;
            }
            set
            {
                this.bandeiraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BandeiraSpecified
        {
            get
            {
                return this.bandeiraFieldSpecified;
            }
            set
            {
                this.bandeiraFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public string Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        public string NumeroMascara
        {
            get
            {
                return this.numeroMascaraField;
            }
            set
            {
                this.numeroMascaraField = value;
            }
        }

        /// <remarks/>
        public string NumeroCripto1
        {
            get
            {
                return this.numeroCripto1Field;
            }
            set
            {
                this.numeroCripto1Field = value;
            }
        }

        /// <remarks/>
        public string NumeroCripto2
        {
            get
            {
                return this.numeroCripto2Field;
            }
            set
            {
                this.numeroCripto2Field = value;
            }
        }

        /// <remarks/>
        public string CVV
        {
            get
            {
                return this.cVVField;
            }
            set
            {
                this.cVVField = value;
            }
        }

        /// <remarks/>
        public int ValidadeMes
        {
            get
            {
                return this.validadeMesField;
            }
            set
            {
                this.validadeMesField = value;
            }
        }

        /// <remarks/>
        public int ValidadeAno
        {
            get
            {
                return this.validadeAnoField;
            }
            set
            {
                this.validadeAnoField = value;
            }
        }

        /// <remarks/>
        public int Parcelas
        {
            get
            {
                return this.parcelasField;
            }
            set
            {
                this.parcelasField = value;
            }
        }

        /// <remarks/>
        public decimal PorcentagemDesconto
        {
            get
            {
                return this.porcentagemDescontoField;
            }
            set
            {
                this.porcentagemDescontoField = value;
            }
        }

        /// <remarks/>
        public decimal ValorMinimoParcela
        {
            get
            {
                return this.valorMinimoParcelaField;
            }
            set
            {
                this.valorMinimoParcelaField = value;
            }
        }

        /// <remarks/>
        public decimal JurosAoMes
        {
            get
            {
                return this.jurosAoMesField;
            }
            set
            {
                this.jurosAoMesField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum CartaoCreditoBandeira
    {

        /// <remarks/>
        Visa,

        /// <remarks/>
        MasterCard,

        /// <remarks/>
        Amex,

        /// <remarks/>
        Diners,

        /// <remarks/>
        Hipercard,

        /// <remarks/>
        Aura,

        /// <remarks/>
        Saraiva,

        /// <remarks/>
        Elo,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class OpcaoPagamento
    {

        private int idOpcaoPagamentoField;

        private bool idOpcaoPagamentoFieldSpecified;

        private int idCondicaoPagamentoField;

        private bool idCondicaoPagamentoFieldSpecified;

        private int idTipoOpcaoPagamentoField;

        private bool idTipoOpcaoPagamentoFieldSpecified;

        private System.DateTime dataAlteracaoCondicaoField;

        private bool dataAlteracaoCondicaoFieldSpecified;

        private decimal valorField;

        private decimal valorFaturadoField;

        private bool valorFaturadoFieldSpecified;

        private bool bonusField;

        private bool bonusFieldSpecified;

        private bool prePagoField;

        private bool prePagoFieldSpecified;

        private CartaoCredito cartaoCreditoField;

        private DebitoOnline debitoOnlineField;

        private Boleto boletoField;

        private ValeCultura valeCulturaField;

        private CartaoPresente cartaoPresenteField;

        private SaraivaPlus saraivaPlusField;

        private PagamentoLoja pagamentoLojaField;

        private MarketPlaceReverso marketPlaceReversoField;

        private int numeroDocumentoField;

        private bool numeroDocumentoFieldSpecified;

        private string codigoBancoField;

        private decimal valorDocumentoField;

        private bool valorDocumentoFieldSpecified;

        private System.DateTime dataLimiteRecebimentoField;

        private bool dataLimiteRecebimentoFieldSpecified;

        private decimal valorRecebidoField;

        private bool valorRecebidoFieldSpecified;

        private System.DateTime dataRecebimentoField;

        private bool dataRecebimentoFieldSpecified;

        private string observacaoField;

        private int parcelasField;

        private bool parcelasFieldSpecified;

        private decimal porcentagemDescontoField;

        private bool porcentagemDescontoFieldSpecified;

        private decimal jurosAoMesField;

        private bool jurosAoMesFieldSpecified;

        /// <remarks/>
        public int IdOpcaoPagamento
        {
            get
            {
                return this.idOpcaoPagamentoField;
            }
            set
            {
                this.idOpcaoPagamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdOpcaoPagamentoSpecified
        {
            get
            {
                return this.idOpcaoPagamentoFieldSpecified;
            }
            set
            {
                this.idOpcaoPagamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdCondicaoPagamento
        {
            get
            {
                return this.idCondicaoPagamentoField;
            }
            set
            {
                this.idCondicaoPagamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCondicaoPagamentoSpecified
        {
            get
            {
                return this.idCondicaoPagamentoFieldSpecified;
            }
            set
            {
                this.idCondicaoPagamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdTipoOpcaoPagamento
        {
            get
            {
                return this.idTipoOpcaoPagamentoField;
            }
            set
            {
                this.idTipoOpcaoPagamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdTipoOpcaoPagamentoSpecified
        {
            get
            {
                return this.idTipoOpcaoPagamentoFieldSpecified;
            }
            set
            {
                this.idTipoOpcaoPagamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataAlteracaoCondicao
        {
            get
            {
                return this.dataAlteracaoCondicaoField;
            }
            set
            {
                this.dataAlteracaoCondicaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataAlteracaoCondicaoSpecified
        {
            get
            {
                return this.dataAlteracaoCondicaoFieldSpecified;
            }
            set
            {
                this.dataAlteracaoCondicaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }

        /// <remarks/>
        public decimal ValorFaturado
        {
            get
            {
                return this.valorFaturadoField;
            }
            set
            {
                this.valorFaturadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorFaturadoSpecified
        {
            get
            {
                return this.valorFaturadoFieldSpecified;
            }
            set
            {
                this.valorFaturadoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Bonus
        {
            get
            {
                return this.bonusField;
            }
            set
            {
                this.bonusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BonusSpecified
        {
            get
            {
                return this.bonusFieldSpecified;
            }
            set
            {
                this.bonusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool PrePago
        {
            get
            {
                return this.prePagoField;
            }
            set
            {
                this.prePagoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrePagoSpecified
        {
            get
            {
                return this.prePagoFieldSpecified;
            }
            set
            {
                this.prePagoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CartaoCredito CartaoCredito
        {
            get
            {
                return this.cartaoCreditoField;
            }
            set
            {
                this.cartaoCreditoField = value;
            }
        }

        /// <remarks/>
        public DebitoOnline DebitoOnline
        {
            get
            {
                return this.debitoOnlineField;
            }
            set
            {
                this.debitoOnlineField = value;
            }
        }

        /// <remarks/>
        public Boleto Boleto
        {
            get
            {
                return this.boletoField;
            }
            set
            {
                this.boletoField = value;
            }
        }

        /// <remarks/>
        public ValeCultura ValeCultura
        {
            get
            {
                return this.valeCulturaField;
            }
            set
            {
                this.valeCulturaField = value;
            }
        }

        /// <remarks/>
        public CartaoPresente CartaoPresente
        {
            get
            {
                return this.cartaoPresenteField;
            }
            set
            {
                this.cartaoPresenteField = value;
            }
        }

        /// <remarks/>
        public SaraivaPlus SaraivaPlus
        {
            get
            {
                return this.saraivaPlusField;
            }
            set
            {
                this.saraivaPlusField = value;
            }
        }

        /// <remarks/>
        public PagamentoLoja PagamentoLoja
        {
            get
            {
                return this.pagamentoLojaField;
            }
            set
            {
                this.pagamentoLojaField = value;
            }
        }

        /// <remarks/>
        public MarketPlaceReverso MarketPlaceReverso
        {
            get
            {
                return this.marketPlaceReversoField;
            }
            set
            {
                this.marketPlaceReversoField = value;
            }
        }

        /// <remarks/>
        public int NumeroDocumento
        {
            get
            {
                return this.numeroDocumentoField;
            }
            set
            {
                this.numeroDocumentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroDocumentoSpecified
        {
            get
            {
                return this.numeroDocumentoFieldSpecified;
            }
            set
            {
                this.numeroDocumentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string CodigoBanco
        {
            get
            {
                return this.codigoBancoField;
            }
            set
            {
                this.codigoBancoField = value;
            }
        }

        /// <remarks/>
        public decimal ValorDocumento
        {
            get
            {
                return this.valorDocumentoField;
            }
            set
            {
                this.valorDocumentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorDocumentoSpecified
        {
            get
            {
                return this.valorDocumentoFieldSpecified;
            }
            set
            {
                this.valorDocumentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataLimiteRecebimento
        {
            get
            {
                return this.dataLimiteRecebimentoField;
            }
            set
            {
                this.dataLimiteRecebimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataLimiteRecebimentoSpecified
        {
            get
            {
                return this.dataLimiteRecebimentoFieldSpecified;
            }
            set
            {
                this.dataLimiteRecebimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ValorRecebido
        {
            get
            {
                return this.valorRecebidoField;
            }
            set
            {
                this.valorRecebidoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorRecebidoSpecified
        {
            get
            {
                return this.valorRecebidoFieldSpecified;
            }
            set
            {
                this.valorRecebidoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataRecebimento
        {
            get
            {
                return this.dataRecebimentoField;
            }
            set
            {
                this.dataRecebimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataRecebimentoSpecified
        {
            get
            {
                return this.dataRecebimentoFieldSpecified;
            }
            set
            {
                this.dataRecebimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Observacao
        {
            get
            {
                return this.observacaoField;
            }
            set
            {
                this.observacaoField = value;
            }
        }

        /// <remarks/>
        public int Parcelas
        {
            get
            {
                return this.parcelasField;
            }
            set
            {
                this.parcelasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ParcelasSpecified
        {
            get
            {
                return this.parcelasFieldSpecified;
            }
            set
            {
                this.parcelasFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal PorcentagemDesconto
        {
            get
            {
                return this.porcentagemDescontoField;
            }
            set
            {
                this.porcentagemDescontoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PorcentagemDescontoSpecified
        {
            get
            {
                return this.porcentagemDescontoFieldSpecified;
            }
            set
            {
                this.porcentagemDescontoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal JurosAoMes
        {
            get
            {
                return this.jurosAoMesField;
            }
            set
            {
                this.jurosAoMesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool JurosAoMesSpecified
        {
            get
            {
                return this.jurosAoMesFieldSpecified;
            }
            set
            {
                this.jurosAoMesFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class CartaoPresente
    {

        private int idCartaoPresenteField;

        private bool idCartaoPresenteFieldSpecified;

        private decimal numeroField;

        private int pINField;

        private decimal saldoField;

        private bool saldoFieldSpecified;

        private System.DateTime validadeField;

        private bool validadeFieldSpecified;

        private CartaoPresenteAtivo ativoField;

        private bool ativoFieldSpecified;

        /// <remarks/>
        public int IdCartaoPresente
        {
            get
            {
                return this.idCartaoPresenteField;
            }
            set
            {
                this.idCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCartaoPresenteSpecified
        {
            get
            {
                return this.idCartaoPresenteFieldSpecified;
            }
            set
            {
                this.idCartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        public int PIN
        {
            get
            {
                return this.pINField;
            }
            set
            {
                this.pINField = value;
            }
        }

        /// <remarks/>
        public decimal Saldo
        {
            get
            {
                return this.saldoField;
            }
            set
            {
                this.saldoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SaldoSpecified
        {
            get
            {
                return this.saldoFieldSpecified;
            }
            set
            {
                this.saldoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime Validade
        {
            get
            {
                return this.validadeField;
            }
            set
            {
                this.validadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValidadeSpecified
        {
            get
            {
                return this.validadeFieldSpecified;
            }
            set
            {
                this.validadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CartaoPresenteAtivo Ativo
        {
            get
            {
                return this.ativoField;
            }
            set
            {
                this.ativoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AtivoSpecified
        {
            get
            {
                return this.ativoFieldSpecified;
            }
            set
            {
                this.ativoFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum CartaoPresenteAtivo
    {

        /// <remarks/>
        N,

        /// <remarks/>
        value,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class SaraivaPlus : ProgramaRecompensa
    {

        private string numeroCartaoField;

        /// <remarks/>
        public string NumeroCartao
        {
            get
            {
                return this.numeroCartaoField;
            }
            set
            {
                this.numeroCartaoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Estorno
    {

        private int idField;

        private bool idFieldSpecified;

        private EstornoTipo tipoField;

        private bool tipoFieldSpecified;

        private int idStatusField;

        private bool idStatusFieldSpecified;

        private string statusField;

        private int idMetodoField;

        private bool idMetodoFieldSpecified;

        private string metodoField;

        private System.DateTime dataEstornoField;

        private bool dataEstornoFieldSpecified;

        private int quantidadeEstornadaField;

        private bool quantidadeEstornadaFieldSpecified;

        private int pontosSaraivaPlusField;

        private bool pontosSaraivaPlusFieldSpecified;

        private decimal valorSaraivaPlusField;

        private bool valorSaraivaPlusFieldSpecified;

        private decimal valorCartaoPresenteField;

        private bool valorCartaoPresenteFieldSpecified;

        private decimal valorEstornoField;

        private bool valorEstornoFieldSpecified;

        private decimal valorTotalEstornoField;

        private bool valorTotalEstornoFieldSpecified;

        private string codigoBancoField;

        private string agenciaField;

        private string contaCorrenteField;

        private System.DateTime dataCadastroField;

        private bool dataCadastroFieldSpecified;

        private string correntistaField;

        private string cPFField;

        private string tipoContaField;

        private int idPedidoCartaoPresenteField;

        private bool idPedidoCartaoPresenteFieldSpecified;

        private int idCartaoPresenteField;

        private bool idCartaoPresenteFieldSpecified;

        private List<Item> itemField;

        /// <remarks/>
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSpecified
        {
            get
            {
                return this.idFieldSpecified;
            }
            set
            {
                this.idFieldSpecified = value;
            }
        }

        /// <remarks/>
        public EstornoTipo Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TipoSpecified
        {
            get
            {
                return this.tipoFieldSpecified;
            }
            set
            {
                this.tipoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdStatus
        {
            get
            {
                return this.idStatusField;
            }
            set
            {
                this.idStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdStatusSpecified
        {
            get
            {
                return this.idStatusFieldSpecified;
            }
            set
            {
                this.idStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public int IdMetodo
        {
            get
            {
                return this.idMetodoField;
            }
            set
            {
                this.idMetodoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdMetodoSpecified
        {
            get
            {
                return this.idMetodoFieldSpecified;
            }
            set
            {
                this.idMetodoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Metodo
        {
            get
            {
                return this.metodoField;
            }
            set
            {
                this.metodoField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataEstorno
        {
            get
            {
                return this.dataEstornoField;
            }
            set
            {
                this.dataEstornoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataEstornoSpecified
        {
            get
            {
                return this.dataEstornoFieldSpecified;
            }
            set
            {
                this.dataEstornoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeEstornada
        {
            get
            {
                return this.quantidadeEstornadaField;
            }
            set
            {
                this.quantidadeEstornadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeEstornadaSpecified
        {
            get
            {
                return this.quantidadeEstornadaFieldSpecified;
            }
            set
            {
                this.quantidadeEstornadaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int PontosSaraivaPlus
        {
            get
            {
                return this.pontosSaraivaPlusField;
            }
            set
            {
                this.pontosSaraivaPlusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PontosSaraivaPlusSpecified
        {
            get
            {
                return this.pontosSaraivaPlusFieldSpecified;
            }
            set
            {
                this.pontosSaraivaPlusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ValorSaraivaPlus
        {
            get
            {
                return this.valorSaraivaPlusField;
            }
            set
            {
                this.valorSaraivaPlusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorSaraivaPlusSpecified
        {
            get
            {
                return this.valorSaraivaPlusFieldSpecified;
            }
            set
            {
                this.valorSaraivaPlusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ValorCartaoPresente
        {
            get
            {
                return this.valorCartaoPresenteField;
            }
            set
            {
                this.valorCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorCartaoPresenteSpecified
        {
            get
            {
                return this.valorCartaoPresenteFieldSpecified;
            }
            set
            {
                this.valorCartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ValorEstorno
        {
            get
            {
                return this.valorEstornoField;
            }
            set
            {
                this.valorEstornoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorEstornoSpecified
        {
            get
            {
                return this.valorEstornoFieldSpecified;
            }
            set
            {
                this.valorEstornoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal ValorTotalEstorno
        {
            get
            {
                return this.valorTotalEstornoField;
            }
            set
            {
                this.valorTotalEstornoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorTotalEstornoSpecified
        {
            get
            {
                return this.valorTotalEstornoFieldSpecified;
            }
            set
            {
                this.valorTotalEstornoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string CodigoBanco
        {
            get
            {
                return this.codigoBancoField;
            }
            set
            {
                this.codigoBancoField = value;
            }
        }

        /// <remarks/>
        public string Agencia
        {
            get
            {
                return this.agenciaField;
            }
            set
            {
                this.agenciaField = value;
            }
        }

        /// <remarks/>
        public string ContaCorrente
        {
            get
            {
                return this.contaCorrenteField;
            }
            set
            {
                this.contaCorrenteField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataCadastro
        {
            get
            {
                return this.dataCadastroField;
            }
            set
            {
                this.dataCadastroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataCadastroSpecified
        {
            get
            {
                return this.dataCadastroFieldSpecified;
            }
            set
            {
                this.dataCadastroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Correntista
        {
            get
            {
                return this.correntistaField;
            }
            set
            {
                this.correntistaField = value;
            }
        }

        /// <remarks/>
        public string CPF
        {
            get
            {
                return this.cPFField;
            }
            set
            {
                this.cPFField = value;
            }
        }

        /// <remarks/>
        public string TipoConta
        {
            get
            {
                return this.tipoContaField;
            }
            set
            {
                this.tipoContaField = value;
            }
        }

        /// <remarks/>
        public int IdPedidoCartaoPresente
        {
            get
            {
                return this.idPedidoCartaoPresenteField;
            }
            set
            {
                this.idPedidoCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPedidoCartaoPresenteSpecified
        {
            get
            {
                return this.idPedidoCartaoPresenteFieldSpecified;
            }
            set
            {
                this.idPedidoCartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdCartaoPresente
        {
            get
            {
                return this.idCartaoPresenteField;
            }
            set
            {
                this.idCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCartaoPresenteSpecified
        {
            get
            {
                return this.idCartaoPresenteFieldSpecified;
            }
            set
            {
                this.idCartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Item")]
        public List<Item> Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum EstornoTipo
    {

        /// <remarks/>
        ContaCorrente,

        /// <remarks/>
        CartaoCredito,

        /// <remarks/>
        CartaoPresente,
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ValidacaoItem))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Item
    {

        private int idParceiroField;

        private bool idParceiroFieldSpecified;

        private int idItemField;

        private bool idItemFieldSpecified;

        private int idEntregaField;

        private bool idEntregaFieldSpecified;

        private Produto produtoField;

        private Vinculo vinculoField;

        private int quantidadeField;

        private bool quantidadeFieldSpecified;

        private int quantidadeFreteField;

        private bool quantidadeFreteFieldSpecified;

        private int quantidadeCobradaField;

        private bool quantidadeCobradaFieldSpecified;

        private int quantidadeSubtraidaField;

        private bool quantidadeSubtraidaFieldSpecified;

        private int quantidadeEstornadaField;

        private bool quantidadeEstornadaFieldSpecified;

        private int quantidadeAtendidaField;

        private bool quantidadeAtendidaFieldSpecified;

        private int quantidadePendenteField;

        private bool quantidadePendenteFieldSpecified;

        private int quantidadeFaturadaField;

        private bool quantidadeFaturadaFieldSpecified;

        private int quantidadePostadaField;

        private bool quantidadePostadaFieldSpecified;

        private int quantidadeDevolvidaField;

        private bool quantidadeDevolvidaFieldSpecified;

        private int quantidadePendenteDevolucaoField;

        private bool quantidadePendenteDevolucaoFieldSpecified;

        private decimal precoUnitarioField;

        private bool precoUnitarioFieldSpecified;

        private decimal precoTotalUnitarioField;

        private bool precoTotalUnitarioFieldSpecified;

        private decimal precoTotalField;

        private bool precoTotalFieldSpecified;

        private decimal descontoUnitarioField;

        private bool descontoUnitarioFieldSpecified;

        private decimal descontoPagamentoField;

        private bool descontoPagamentoFieldSpecified;

        private decimal descontoCupomField;

        private bool descontoCupomFieldSpecified;

        private decimal descontoSaraivaPlusField;

        private bool descontoSaraivaPlusFieldSpecified;

        private decimal descontoTotalField;

        private bool descontoTotalFieldSpecified;

        private decimal acrescimoJurosField;

        private bool acrescimoJurosFieldSpecified;

        private decimal acrescimoOutrosField;

        private bool acrescimoOutrosFieldSpecified;

        private decimal acrescimoTotalField;

        private bool acrescimoTotalFieldSpecified;

        private bool embrulharPresenteField;

        private bool embrulharPresenteFieldSpecified;

        private string textoEmbrulhoField;

        private decimal precoEmbrulhoField;

        private bool precoEmbrulhoFieldSpecified;

        private bool cartaoPresenteField;

        private bool cartaoPresenteFieldSpecified;

        private string nomeCartaoPresenteField;

        private string emailCartaoPresenteField;

        private decimal totalItemField;

        private bool totalItemFieldSpecified;

        private bool brindeField;

        private bool brindeFieldSpecified;

        private bool prevendaField;

        private bool prevendaFieldSpecified;

        private PrazoFrete postagemField;

        private PrazoFrete entregaField;

        private PrazoFrete entregaPrevendaField;

        private EntregaAgendada entregaAgendadaField;

        private OpcaoFrete freteField;

        private decimal numeroRecargaCartaoPresenteField;

        private bool numeroRecargaCartaoPresenteFieldSpecified;

        private Entregas entregasField;

        private List<Seguro> segurosField;

        private List<GarantiaEstendida> garantiasEstendidasField;

        private int idStatusField;

        private bool idStatusFieldSpecified;

        private int idMotivoField;

        private bool idMotivoFieldSpecified;

        private string motivoField;

        private Prorrogacao prorrogacaoField;

        private ItemTipoServico tipoServicoField;

        private bool tipoServicoFieldSpecified;

        private Estorno estornoField;

        /// <remarks/>
        public int IdParceiro
        {
            get
            {
                return this.idParceiroField;
            }
            set
            {
                this.idParceiroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdParceiroSpecified
        {
            get
            {
                return this.idParceiroFieldSpecified;
            }
            set
            {
                this.idParceiroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdItem
        {
            get
            {
                return this.idItemField;
            }
            set
            {
                this.idItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdItemSpecified
        {
            get
            {
                return this.idItemFieldSpecified;
            }
            set
            {
                this.idItemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdEntrega
        {
            get
            {
                return this.idEntregaField;
            }
            set
            {
                this.idEntregaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdEntregaSpecified
        {
            get
            {
                return this.idEntregaFieldSpecified;
            }
            set
            {
                this.idEntregaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Produto Produto
        {
            get
            {
                return this.produtoField;
            }
            set
            {
                this.produtoField = value;
            }
        }

        /// <remarks/>
        public Vinculo Vinculo
        {
            get
            {
                return this.vinculoField;
            }
            set
            {
                this.vinculoField = value;
            }
        }

        /// <remarks/>
        public int Quantidade
        {
            get
            {
                return this.quantidadeField;
            }
            set
            {
                this.quantidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeSpecified
        {
            get
            {
                return this.quantidadeFieldSpecified;
            }
            set
            {
                this.quantidadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeFrete
        {
            get
            {
                return this.quantidadeFreteField;
            }
            set
            {
                this.quantidadeFreteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeFreteSpecified
        {
            get
            {
                return this.quantidadeFreteFieldSpecified;
            }
            set
            {
                this.quantidadeFreteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeCobrada
        {
            get
            {
                return this.quantidadeCobradaField;
            }
            set
            {
                this.quantidadeCobradaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeCobradaSpecified
        {
            get
            {
                return this.quantidadeCobradaFieldSpecified;
            }
            set
            {
                this.quantidadeCobradaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeSubtraida
        {
            get
            {
                return this.quantidadeSubtraidaField;
            }
            set
            {
                this.quantidadeSubtraidaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeSubtraidaSpecified
        {
            get
            {
                return this.quantidadeSubtraidaFieldSpecified;
            }
            set
            {
                this.quantidadeSubtraidaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeEstornada
        {
            get
            {
                return this.quantidadeEstornadaField;
            }
            set
            {
                this.quantidadeEstornadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeEstornadaSpecified
        {
            get
            {
                return this.quantidadeEstornadaFieldSpecified;
            }
            set
            {
                this.quantidadeEstornadaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeAtendida
        {
            get
            {
                return this.quantidadeAtendidaField;
            }
            set
            {
                this.quantidadeAtendidaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeAtendidaSpecified
        {
            get
            {
                return this.quantidadeAtendidaFieldSpecified;
            }
            set
            {
                this.quantidadeAtendidaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadePendente
        {
            get
            {
                return this.quantidadePendenteField;
            }
            set
            {
                this.quantidadePendenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadePendenteSpecified
        {
            get
            {
                return this.quantidadePendenteFieldSpecified;
            }
            set
            {
                this.quantidadePendenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeFaturada
        {
            get
            {
                return this.quantidadeFaturadaField;
            }
            set
            {
                this.quantidadeFaturadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeFaturadaSpecified
        {
            get
            {
                return this.quantidadeFaturadaFieldSpecified;
            }
            set
            {
                this.quantidadeFaturadaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadePostada
        {
            get
            {
                return this.quantidadePostadaField;
            }
            set
            {
                this.quantidadePostadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadePostadaSpecified
        {
            get
            {
                return this.quantidadePostadaFieldSpecified;
            }
            set
            {
                this.quantidadePostadaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeDevolvida
        {
            get
            {
                return this.quantidadeDevolvidaField;
            }
            set
            {
                this.quantidadeDevolvidaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeDevolvidaSpecified
        {
            get
            {
                return this.quantidadeDevolvidaFieldSpecified;
            }
            set
            {
                this.quantidadeDevolvidaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadePendenteDevolucao
        {
            get
            {
                return this.quantidadePendenteDevolucaoField;
            }
            set
            {
                this.quantidadePendenteDevolucaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadePendenteDevolucaoSpecified
        {
            get
            {
                return this.quantidadePendenteDevolucaoFieldSpecified;
            }
            set
            {
                this.quantidadePendenteDevolucaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal PrecoUnitario
        {
            get
            {
                return this.precoUnitarioField;
            }
            set
            {
                this.precoUnitarioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoUnitarioSpecified
        {
            get
            {
                return this.precoUnitarioFieldSpecified;
            }
            set
            {
                this.precoUnitarioFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal PrecoTotalUnitario
        {
            get
            {
                return this.precoTotalUnitarioField;
            }
            set
            {
                this.precoTotalUnitarioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoTotalUnitarioSpecified
        {
            get
            {
                return this.precoTotalUnitarioFieldSpecified;
            }
            set
            {
                this.precoTotalUnitarioFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal PrecoTotal
        {
            get
            {
                return this.precoTotalField;
            }
            set
            {
                this.precoTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoTotalSpecified
        {
            get
            {
                return this.precoTotalFieldSpecified;
            }
            set
            {
                this.precoTotalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal DescontoUnitario
        {
            get
            {
                return this.descontoUnitarioField;
            }
            set
            {
                this.descontoUnitarioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescontoUnitarioSpecified
        {
            get
            {
                return this.descontoUnitarioFieldSpecified;
            }
            set
            {
                this.descontoUnitarioFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal DescontoPagamento
        {
            get
            {
                return this.descontoPagamentoField;
            }
            set
            {
                this.descontoPagamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescontoPagamentoSpecified
        {
            get
            {
                return this.descontoPagamentoFieldSpecified;
            }
            set
            {
                this.descontoPagamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal DescontoCupom
        {
            get
            {
                return this.descontoCupomField;
            }
            set
            {
                this.descontoCupomField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescontoCupomSpecified
        {
            get
            {
                return this.descontoCupomFieldSpecified;
            }
            set
            {
                this.descontoCupomFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal DescontoSaraivaPlus
        {
            get
            {
                return this.descontoSaraivaPlusField;
            }
            set
            {
                this.descontoSaraivaPlusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescontoSaraivaPlusSpecified
        {
            get
            {
                return this.descontoSaraivaPlusFieldSpecified;
            }
            set
            {
                this.descontoSaraivaPlusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal DescontoTotal
        {
            get
            {
                return this.descontoTotalField;
            }
            set
            {
                this.descontoTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescontoTotalSpecified
        {
            get
            {
                return this.descontoTotalFieldSpecified;
            }
            set
            {
                this.descontoTotalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal AcrescimoJuros
        {
            get
            {
                return this.acrescimoJurosField;
            }
            set
            {
                this.acrescimoJurosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AcrescimoJurosSpecified
        {
            get
            {
                return this.acrescimoJurosFieldSpecified;
            }
            set
            {
                this.acrescimoJurosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal AcrescimoOutros
        {
            get
            {
                return this.acrescimoOutrosField;
            }
            set
            {
                this.acrescimoOutrosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AcrescimoOutrosSpecified
        {
            get
            {
                return this.acrescimoOutrosFieldSpecified;
            }
            set
            {
                this.acrescimoOutrosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal AcrescimoTotal
        {
            get
            {
                return this.acrescimoTotalField;
            }
            set
            {
                this.acrescimoTotalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AcrescimoTotalSpecified
        {
            get
            {
                return this.acrescimoTotalFieldSpecified;
            }
            set
            {
                this.acrescimoTotalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool EmbrulharPresente
        {
            get
            {
                return this.embrulharPresenteField;
            }
            set
            {
                this.embrulharPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EmbrulharPresenteSpecified
        {
            get
            {
                return this.embrulharPresenteFieldSpecified;
            }
            set
            {
                this.embrulharPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TextoEmbrulho
        {
            get
            {
                return this.textoEmbrulhoField;
            }
            set
            {
                this.textoEmbrulhoField = value;
            }
        }

        /// <remarks/>
        public decimal PrecoEmbrulho
        {
            get
            {
                return this.precoEmbrulhoField;
            }
            set
            {
                this.precoEmbrulhoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoEmbrulhoSpecified
        {
            get
            {
                return this.precoEmbrulhoFieldSpecified;
            }
            set
            {
                this.precoEmbrulhoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool CartaoPresente
        {
            get
            {
                return this.cartaoPresenteField;
            }
            set
            {
                this.cartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CartaoPresenteSpecified
        {
            get
            {
                return this.cartaoPresenteFieldSpecified;
            }
            set
            {
                this.cartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NomeCartaoPresente
        {
            get
            {
                return this.nomeCartaoPresenteField;
            }
            set
            {
                this.nomeCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        public string EmailCartaoPresente
        {
            get
            {
                return this.emailCartaoPresenteField;
            }
            set
            {
                this.emailCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        public decimal TotalItem
        {
            get
            {
                return this.totalItemField;
            }
            set
            {
                this.totalItemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalItemSpecified
        {
            get
            {
                return this.totalItemFieldSpecified;
            }
            set
            {
                this.totalItemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Brinde
        {
            get
            {
                return this.brindeField;
            }
            set
            {
                this.brindeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool BrindeSpecified
        {
            get
            {
                return this.brindeFieldSpecified;
            }
            set
            {
                this.brindeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Prevenda
        {
            get
            {
                return this.prevendaField;
            }
            set
            {
                this.prevendaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrevendaSpecified
        {
            get
            {
                return this.prevendaFieldSpecified;
            }
            set
            {
                this.prevendaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public PrazoFrete Postagem
        {
            get
            {
                return this.postagemField;
            }
            set
            {
                this.postagemField = value;
            }
        }

        /// <remarks/>
        public PrazoFrete Entrega
        {
            get
            {
                return this.entregaField;
            }
            set
            {
                this.entregaField = value;
            }
        }

        /// <remarks/>
        public PrazoFrete EntregaPrevenda
        {
            get
            {
                return this.entregaPrevendaField;
            }
            set
            {
                this.entregaPrevendaField = value;
            }
        }

        /// <remarks/>
        public EntregaAgendada EntregaAgendada
        {
            get
            {
                return this.entregaAgendadaField;
            }
            set
            {
                this.entregaAgendadaField = value;
            }
        }

        /// <remarks/>
        public OpcaoFrete Frete
        {
            get
            {
                return this.freteField;
            }
            set
            {
                this.freteField = value;
            }
        }

        /// <remarks/>
        public decimal NumeroRecargaCartaoPresente
        {
            get
            {
                return this.numeroRecargaCartaoPresenteField;
            }
            set
            {
                this.numeroRecargaCartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroRecargaCartaoPresenteSpecified
        {
            get
            {
                return this.numeroRecargaCartaoPresenteFieldSpecified;
            }
            set
            {
                this.numeroRecargaCartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Entregas Entregas
        {
            get
            {
                return this.entregasField;
            }
            set
            {
                this.entregasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Seguro> Seguros
        {
            get
            {
                return this.segurosField;
            }
            set
            {
                this.segurosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<GarantiaEstendida> GarantiasEstendidas
        {
            get
            {
                return this.garantiasEstendidasField;
            }
            set
            {
                this.garantiasEstendidasField = value;
            }
        }

        /// <remarks/>
        public int IdStatus
        {
            get
            {
                return this.idStatusField;
            }
            set
            {
                this.idStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdStatusSpecified
        {
            get
            {
                return this.idStatusFieldSpecified;
            }
            set
            {
                this.idStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdMotivo
        {
            get
            {
                return this.idMotivoField;
            }
            set
            {
                this.idMotivoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdMotivoSpecified
        {
            get
            {
                return this.idMotivoFieldSpecified;
            }
            set
            {
                this.idMotivoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Motivo
        {
            get
            {
                return this.motivoField;
            }
            set
            {
                this.motivoField = value;
            }
        }

        /// <remarks/>
        public Prorrogacao Prorrogacao
        {
            get
            {
                return this.prorrogacaoField;
            }
            set
            {
                this.prorrogacaoField = value;
            }
        }

        /// <remarks/>
        public ItemTipoServico TipoServico
        {
            get
            {
                return this.tipoServicoField;
            }
            set
            {
                this.tipoServicoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TipoServicoSpecified
        {
            get
            {
                return this.tipoServicoFieldSpecified;
            }
            set
            {
                this.tipoServicoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Estorno Estorno
        {
            get
            {
                return this.estornoField;
            }
            set
            {
                this.estornoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Produto
    {

        private int idProdutoField;

        private string idProdutoExternoField;

        private string nomeField;

        private string chamadaField;

        private string descricaoField;

        private string descricaoPreFormatadaField;

        private ProdutoTipo tipoField;

        private bool tipoFieldSpecified;

        private Marca marcaField;

        private string fabricanteField;

        private bool importadoField;

        private bool importadoFieldSpecified;

        private bool digitalField;

        private bool digitalFieldSpecified;

        private bool didaticoField;

        private bool didaticoFieldSpecified;

        private DadosDidatico dadosDidaticoField;

        private bool possuiAmostraDigitalField;

        private bool possuiAmostraDigitalFieldSpecified;

        private string amostraTextoField;

        private List<CD> faixasCDField;

        private List<Caracteristica> caracteristicasField;

        private List<Contribuidor> contribuidoresField;

        private List<Imagem> imagensField;

        private List<Video> videosField;

        private int produtoPaiField;

        private bool produtoPaiFieldSpecified;

        private ListaSKU sKUsField;

        private List<Componente> componentesField;

        private List<Relacionamento> relacionadosField;

        private List<Categoria> categoriasField;

        private Disponibilidade disponibilidadeField;

        private FatoresOrdenacao fatoresOrdenacaoField;

        private List<Seguro> seguroField;

        /// <remarks/>
        public int IdProduto
        {
            get
            {
                return this.idProdutoField;
            }
            set
            {
                this.idProdutoField = value;
            }
        }

        /// <remarks/>
        public string IdProdutoExterno
        {
            get
            {
                return this.idProdutoExternoField;
            }
            set
            {
                this.idProdutoExternoField = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public string Chamada
        {
            get
            {
                return this.chamadaField;
            }
            set
            {
                this.chamadaField = value;
            }
        }

        /// <remarks/>
        public string Descricao
        {
            get
            {
                return this.descricaoField;
            }
            set
            {
                this.descricaoField = value;
            }
        }

        /// <remarks/>
        public string DescricaoPreFormatada
        {
            get
            {
                return this.descricaoPreFormatadaField;
            }
            set
            {
                this.descricaoPreFormatadaField = value;
            }
        }

        /// <remarks/>
        public ProdutoTipo Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TipoSpecified
        {
            get
            {
                return this.tipoFieldSpecified;
            }
            set
            {
                this.tipoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Marca Marca
        {
            get
            {
                return this.marcaField;
            }
            set
            {
                this.marcaField = value;
            }
        }

        /// <remarks/>
        public string Fabricante
        {
            get
            {
                return this.fabricanteField;
            }
            set
            {
                this.fabricanteField = value;
            }
        }

        /// <remarks/>
        public bool Importado
        {
            get
            {
                return this.importadoField;
            }
            set
            {
                this.importadoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ImportadoSpecified
        {
            get
            {
                return this.importadoFieldSpecified;
            }
            set
            {
                this.importadoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Digital
        {
            get
            {
                return this.digitalField;
            }
            set
            {
                this.digitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DigitalSpecified
        {
            get
            {
                return this.digitalFieldSpecified;
            }
            set
            {
                this.digitalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Didatico
        {
            get
            {
                return this.didaticoField;
            }
            set
            {
                this.didaticoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DidaticoSpecified
        {
            get
            {
                return this.didaticoFieldSpecified;
            }
            set
            {
                this.didaticoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public DadosDidatico DadosDidatico
        {
            get
            {
                return this.dadosDidaticoField;
            }
            set
            {
                this.dadosDidaticoField = value;
            }
        }

        /// <remarks/>
        public bool PossuiAmostraDigital
        {
            get
            {
                return this.possuiAmostraDigitalField;
            }
            set
            {
                this.possuiAmostraDigitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PossuiAmostraDigitalSpecified
        {
            get
            {
                return this.possuiAmostraDigitalFieldSpecified;
            }
            set
            {
                this.possuiAmostraDigitalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string AmostraTexto
        {
            get
            {
                return this.amostraTextoField;
            }
            set
            {
                this.amostraTextoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<CD> FaixasCD
        {
            get
            {
                return this.faixasCDField;
            }
            set
            {
                this.faixasCDField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Caracteristica> Caracteristicas
        {
            get
            {
                return this.caracteristicasField;
            }
            set
            {
                this.caracteristicasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Contribuidor> Contribuidores
        {
            get
            {
                return this.contribuidoresField;
            }
            set
            {
                this.contribuidoresField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Imagem> Imagens
        {
            get
            {
                return this.imagensField;
            }
            set
            {
                this.imagensField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Video> Videos
        {
            get
            {
                return this.videosField;
            }
            set
            {
                this.videosField = value;
            }
        }

        /// <remarks/>
        public int ProdutoPai
        {
            get
            {
                return this.produtoPaiField;
            }
            set
            {
                this.produtoPaiField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ProdutoPaiSpecified
        {
            get
            {
                return this.produtoPaiFieldSpecified;
            }
            set
            {
                this.produtoPaiFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ListaSKU SKUs
        {
            get
            {
                return this.sKUsField;
            }
            set
            {
                this.sKUsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Componente> Componentes
        {
            get
            {
                return this.componentesField;
            }
            set
            {
                this.componentesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("ProdutoRelacionado", IsNullable = false)]
        public List<Relacionamento> Relacionados
        {
            get
            {
                return this.relacionadosField;
            }
            set
            {
                this.relacionadosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Categoria> Categorias
        {
            get
            {
                return this.categoriasField;
            }
            set
            {
                this.categoriasField = value;
            }
        }

        /// <remarks/>
        public Disponibilidade Disponibilidade
        {
            get
            {
                return this.disponibilidadeField;
            }
            set
            {
                this.disponibilidadeField = value;
            }
        }

        /// <remarks/>
        public FatoresOrdenacao FatoresOrdenacao
        {
            get
            {
                return this.fatoresOrdenacaoField;
            }
            set
            {
                this.fatoresOrdenacaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Seguro")]
        public List<Seguro> Seguro
        {
            get
            {
                return this.seguroField;
            }
            set
            {
                this.seguroField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum ProdutoTipo
    {

        /// <remarks/>
        Produto,

        /// <remarks/>
        SKU,

        /// <remarks/>
        Kit,

        /// <remarks/>
        Bundle,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Marca
    {

        private int idMarcaField;

        private bool idMarcaFieldSpecified;

        private string nomeField;

        /// <remarks/>
        public int IdMarca
        {
            get
            {
                return this.idMarcaField;
            }
            set
            {
                this.idMarcaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdMarcaSpecified
        {
            get
            {
                return this.idMarcaFieldSpecified;
            }
            set
            {
                this.idMarcaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class DadosDidatico
    {

        private int serieField;

        private bool serieFieldSpecified;

        private int grauField;

        private bool grauFieldSpecified;

        /// <remarks/>
        public int Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SerieSpecified
        {
            get
            {
                return this.serieFieldSpecified;
            }
            set
            {
                this.serieFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Grau
        {
            get
            {
                return this.grauField;
            }
            set
            {
                this.grauField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GrauSpecified
        {
            get
            {
                return this.grauFieldSpecified;
            }
            set
            {
                this.grauFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class CD
    {

        private string nomeField;

        private List<Contribuidor> artistasField;

        private int numeroField;

        private bool numeroFieldSpecified;

        private System.DateTime duracaoField;

        private bool duracaoFieldSpecified;

        private List<Faixa> faixasField;

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Artista", IsNullable = false)]
        public List<Contribuidor> Artistas
        {
            get
            {
                return this.artistasField;
            }
            set
            {
                this.artistasField = value;
            }
        }

        /// <remarks/>
        public int Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroSpecified
        {
            get
            {
                return this.numeroFieldSpecified;
            }
            set
            {
                this.numeroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime Duracao
        {
            get
            {
                return this.duracaoField;
            }
            set
            {
                this.duracaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DuracaoSpecified
        {
            get
            {
                return this.duracaoFieldSpecified;
            }
            set
            {
                this.duracaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Faixa> Faixas
        {
            get
            {
                return this.faixasField;
            }
            set
            {
                this.faixasField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Contribuidor
    {

        private int idContribuidorField;

        private bool idContribuidorFieldSpecified;

        private int idPapelField;

        private bool idPapelFieldSpecified;

        private string papelField;

        private string nomeField;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        /// <remarks/>
        public int IdContribuidor
        {
            get
            {
                return this.idContribuidorField;
            }
            set
            {
                this.idContribuidorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdContribuidorSpecified
        {
            get
            {
                return this.idContribuidorFieldSpecified;
            }
            set
            {
                this.idContribuidorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdPapel
        {
            get
            {
                return this.idPapelField;
            }
            set
            {
                this.idPapelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPapelSpecified
        {
            get
            {
                return this.idPapelFieldSpecified;
            }
            set
            {
                this.idPapelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Papel
        {
            get
            {
                return this.papelField;
            }
            set
            {
                this.papelField = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Faixa
    {

        private int idFaixaField;

        private bool idFaixaFieldSpecified;

        private int numeroField;

        private bool numeroFieldSpecified;

        private string nomeField;

        private System.DateTime duracaoField;

        private bool duracaoFieldSpecified;

        private List<Contribuidor> artistasField;

        private bool possuiAmostraField;

        private bool possuiAmostraFieldSpecified;

        /// <remarks/>
        public int IdFaixa
        {
            get
            {
                return this.idFaixaField;
            }
            set
            {
                this.idFaixaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdFaixaSpecified
        {
            get
            {
                return this.idFaixaFieldSpecified;
            }
            set
            {
                this.idFaixaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroSpecified
        {
            get
            {
                return this.numeroFieldSpecified;
            }
            set
            {
                this.numeroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public System.DateTime Duracao
        {
            get
            {
                return this.duracaoField;
            }
            set
            {
                this.duracaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DuracaoSpecified
        {
            get
            {
                return this.duracaoFieldSpecified;
            }
            set
            {
                this.duracaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Artista", IsNullable = false)]
        public List<Contribuidor> Artistas
        {
            get
            {
                return this.artistasField;
            }
            set
            {
                this.artistasField = value;
            }
        }

        /// <remarks/>
        public bool PossuiAmostra
        {
            get
            {
                return this.possuiAmostraField;
            }
            set
            {
                this.possuiAmostraField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PossuiAmostraSpecified
        {
            get
            {
                return this.possuiAmostraFieldSpecified;
            }
            set
            {
                this.possuiAmostraFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Caracteristica
    {

        private int idCaracteristicaField;

        private bool idCaracteristicaFieldSpecified;

        private string tipoField;

        private CaracteristicaUso usoField;

        private bool usoFieldSpecified;

        private CaracteristicaNome nomeField;

        private Valor valorField;

        /// <remarks/>
        public int IdCaracteristica
        {
            get
            {
                return this.idCaracteristicaField;
            }
            set
            {
                this.idCaracteristicaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCaracteristicaSpecified
        {
            get
            {
                return this.idCaracteristicaFieldSpecified;
            }
            set
            {
                this.idCaracteristicaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        /// <remarks/>
        public CaracteristicaUso Uso
        {
            get
            {
                return this.usoField;
            }
            set
            {
                this.usoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UsoSpecified
        {
            get
            {
                return this.usoFieldSpecified;
            }
            set
            {
                this.usoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public CaracteristicaNome Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public Valor Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum CaracteristicaUso
    {

        /// <remarks/>
        Descritivo,

        /// <remarks/>
        Definidor,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum CaracteristicaNome
    {

        /// <remarks/>
        ISBN,

        /// <remarks/>
        Formato,

        /// <remarks/>
        Idioma,

        /// <remarks/>
        Edicao,

        /// <remarks/>
        Ano,

        /// <remarks/>
        Altura,

        /// <remarks/>
        Largura,

        /// <remarks/>
        Comprimento,

        /// <remarks/>
        Peso,

        /// <remarks/>
        Diametro,

        /// <remarks/>
        Acabamento,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Valor
    {

        private int idValorField;

        private bool idValorFieldSpecified;

        private ValorTipo tipoField;

        private bool tipoFieldSpecified;

        private List<OpcaoValor> opcoesField;

        private string valor1Field;

        /// <remarks/>
        public int IdValor
        {
            get
            {
                return this.idValorField;
            }
            set
            {
                this.idValorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdValorSpecified
        {
            get
            {
                return this.idValorFieldSpecified;
            }
            set
            {
                this.idValorFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ValorTipo Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TipoSpecified
        {
            get
            {
                return this.tipoFieldSpecified;
            }
            set
            {
                this.tipoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute("Opcao", IsNullable = false)]
        public List<OpcaoValor> Opcoes
        {
            get
            {
                return this.opcoesField;
            }
            set
            {
                this.opcoesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Valor")]
        public string Valor1
        {
            get
            {
                return this.valor1Field;
            }
            set
            {
                this.valor1Field = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum ValorTipo
    {

        /// <remarks/>
        @int,

        /// <remarks/>
        boolean,

        /// <remarks/>
        @float,

        /// <remarks/>
        @string,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class OpcaoValor
    {

        private int idOpcaoField;

        private bool idOpcaoFieldSpecified;

        private OpcaoValorTipo tipoField;

        private bool tipoFieldSpecified;

        private string valorField;

        /// <remarks/>
        public int IdOpcao
        {
            get
            {
                return this.idOpcaoField;
            }
            set
            {
                this.idOpcaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdOpcaoSpecified
        {
            get
            {
                return this.idOpcaoFieldSpecified;
            }
            set
            {
                this.idOpcaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public OpcaoValorTipo Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TipoSpecified
        {
            get
            {
                return this.tipoFieldSpecified;
            }
            set
            {
                this.tipoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum OpcaoValorTipo
    {

        /// <remarks/>
        @int,

        /// <remarks/>
        boolean,

        /// <remarks/>
        @float,

        /// <remarks/>
        @string,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Imagem
    {

        private int idImagemField;

        private bool idImagemFieldSpecified;

        private string urlField;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        /// <remarks/>
        public int IdImagem
        {
            get
            {
                return this.idImagemField;
            }
            set
            {
                this.idImagemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdImagemSpecified
        {
            get
            {
                return this.idImagemFieldSpecified;
            }
            set
            {
                this.idImagemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Video
    {

        private int idVideoField;

        private bool idVideoFieldSpecified;

        private string urlField;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        /// <remarks/>
        public int IdVideo
        {
            get
            {
                return this.idVideoField;
            }
            set
            {
                this.idVideoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdVideoSpecified
        {
            get
            {
                return this.idVideoFieldSpecified;
            }
            set
            {
                this.idVideoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Url
        {
            get
            {
                return this.urlField;
            }
            set
            {
                this.urlField = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ListaSKU
    {

        private List<Caracteristica> caracteristicasField;

        private List<SKU> sKUField;

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Caracteristica> Caracteristicas
        {
            get
            {
                return this.caracteristicasField;
            }
            set
            {
                this.caracteristicasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("SKU")]
        public List<SKU> SKU
        {
            get
            {
                return this.sKUField;
            }
            set
            {
                this.sKUField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class SKU
    {

        private int idSKUField;

        private string nomeField;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        private List<Caracteristica> caracteristicasField;

        /// <remarks/>
        public int IdSKU
        {
            get
            {
                return this.idSKUField;
            }
            set
            {
                this.idSKUField = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Caracteristica> Caracteristicas
        {
            get
            {
                return this.caracteristicasField;
            }
            set
            {
                this.caracteristicasField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Componente
    {

        private int idProdutoField;

        private string nomeField;

        private int quantidadeField;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        /// <remarks/>
        public int IdProduto
        {
            get
            {
                return this.idProdutoField;
            }
            set
            {
                this.idProdutoField = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public int Quantidade
        {
            get
            {
                return this.quantidadeField;
            }
            set
            {
                this.quantidadeField = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Relacionamento
    {

        private int idProdutoField;

        private string nomeField;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        private TipoRelacionamento tipoField;

        /// <remarks/>
        public int IdProduto
        {
            get
            {
                return this.idProdutoField;
            }
            set
            {
                this.idProdutoField = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public TipoRelacionamento Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class TipoRelacionamento
    {

        private int idTipoRelacionamentoField;

        private bool idTipoRelacionamentoFieldSpecified;

        private string nomeField;

        /// <remarks/>
        public int IdTipoRelacionamento
        {
            get
            {
                return this.idTipoRelacionamentoField;
            }
            set
            {
                this.idTipoRelacionamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdTipoRelacionamentoSpecified
        {
            get
            {
                return this.idTipoRelacionamentoFieldSpecified;
            }
            set
            {
                this.idTipoRelacionamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Categoria
    {

        private int idCategoriaField;

        private bool idCategoriaFieldSpecified;

        private string idEstruturadoField;

        private string nomeField;

        private string descricaoField;

        private int nivelField;

        private bool nivelFieldSpecified;

        private int sequenciaField;

        private bool sequenciaFieldSpecified;

        private ServicosAssociados servicosAssociadosField;

        private List<Categoria> subcategoriasField;

        private List<Categoria> supercategoriasField;

        /// <remarks/>
        public int IdCategoria
        {
            get
            {
                return this.idCategoriaField;
            }
            set
            {
                this.idCategoriaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCategoriaSpecified
        {
            get
            {
                return this.idCategoriaFieldSpecified;
            }
            set
            {
                this.idCategoriaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string IdEstruturado
        {
            get
            {
                return this.idEstruturadoField;
            }
            set
            {
                this.idEstruturadoField = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public string Descricao
        {
            get
            {
                return this.descricaoField;
            }
            set
            {
                this.descricaoField = value;
            }
        }

        /// <remarks/>
        public int Nivel
        {
            get
            {
                return this.nivelField;
            }
            set
            {
                this.nivelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NivelSpecified
        {
            get
            {
                return this.nivelFieldSpecified;
            }
            set
            {
                this.nivelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Sequencia
        {
            get
            {
                return this.sequenciaField;
            }
            set
            {
                this.sequenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SequenciaSpecified
        {
            get
            {
                return this.sequenciaFieldSpecified;
            }
            set
            {
                this.sequenciaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ServicosAssociados ServicosAssociados
        {
            get
            {
                return this.servicosAssociadosField;
            }
            set
            {
                this.servicosAssociadosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Categoria> Subcategorias
        {
            get
            {
                return this.subcategoriasField;
            }
            set
            {
                this.subcategoriasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Categoria> Supercategorias
        {
            get
            {
                return this.supercategoriasField;
            }
            set
            {
                this.supercategoriasField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ServicosAssociados
    {

        private bool garantiaEstendidaField;

        private bool garantiaEstendidaFieldSpecified;

        private bool seguroRouboFurtoField;

        private bool seguroRouboFurtoFieldSpecified;

        /// <remarks/>
        public bool GarantiaEstendida
        {
            get
            {
                return this.garantiaEstendidaField;
            }
            set
            {
                this.garantiaEstendidaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GarantiaEstendidaSpecified
        {
            get
            {
                return this.garantiaEstendidaFieldSpecified;
            }
            set
            {
                this.garantiaEstendidaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool SeguroRouboFurto
        {
            get
            {
                return this.seguroRouboFurtoField;
            }
            set
            {
                this.seguroRouboFurtoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SeguroRouboFurtoSpecified
        {
            get
            {
                return this.seguroRouboFurtoFieldSpecified;
            }
            set
            {
                this.seguroRouboFurtoFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Disponibilidade
    {

        private bool prevendaField;

        private bool prevendaFieldSpecified;

        private System.DateTime dataLancamentoField;

        private bool dataLancamentoFieldSpecified;

        private bool avisemeField;

        private bool avisemeFieldSpecified;

        private bool vendaDescobertaField;

        private bool vendaDescobertaFieldSpecified;

        private bool aluguelField;

        private bool aluguelFieldSpecified;

        private bool umCliqueField;

        private bool umCliqueFieldSpecified;

        private bool embrulhavelField;

        private bool embrulhavelFieldSpecified;

        private bool entregaLojaField;

        private bool entregaLojaFieldSpecified;

        private bool visivelField;

        private bool visivelFieldSpecified;

        private bool compravelField;

        private bool compravelFieldSpecified;

        /// <remarks/>
        public bool Prevenda
        {
            get
            {
                return this.prevendaField;
            }
            set
            {
                this.prevendaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrevendaSpecified
        {
            get
            {
                return this.prevendaFieldSpecified;
            }
            set
            {
                this.prevendaFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DataLancamento
        {
            get
            {
                return this.dataLancamentoField;
            }
            set
            {
                this.dataLancamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataLancamentoSpecified
        {
            get
            {
                return this.dataLancamentoFieldSpecified;
            }
            set
            {
                this.dataLancamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Aviseme
        {
            get
            {
                return this.avisemeField;
            }
            set
            {
                this.avisemeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AvisemeSpecified
        {
            get
            {
                return this.avisemeFieldSpecified;
            }
            set
            {
                this.avisemeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool VendaDescoberta
        {
            get
            {
                return this.vendaDescobertaField;
            }
            set
            {
                this.vendaDescobertaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VendaDescobertaSpecified
        {
            get
            {
                return this.vendaDescobertaFieldSpecified;
            }
            set
            {
                this.vendaDescobertaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Aluguel
        {
            get
            {
                return this.aluguelField;
            }
            set
            {
                this.aluguelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AluguelSpecified
        {
            get
            {
                return this.aluguelFieldSpecified;
            }
            set
            {
                this.aluguelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool UmClique
        {
            get
            {
                return this.umCliqueField;
            }
            set
            {
                this.umCliqueField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UmCliqueSpecified
        {
            get
            {
                return this.umCliqueFieldSpecified;
            }
            set
            {
                this.umCliqueFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Embrulhavel
        {
            get
            {
                return this.embrulhavelField;
            }
            set
            {
                this.embrulhavelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EmbrulhavelSpecified
        {
            get
            {
                return this.embrulhavelFieldSpecified;
            }
            set
            {
                this.embrulhavelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool EntregaLoja
        {
            get
            {
                return this.entregaLojaField;
            }
            set
            {
                this.entregaLojaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EntregaLojaSpecified
        {
            get
            {
                return this.entregaLojaFieldSpecified;
            }
            set
            {
                this.entregaLojaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Visivel
        {
            get
            {
                return this.visivelField;
            }
            set
            {
                this.visivelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VisivelSpecified
        {
            get
            {
                return this.visivelFieldSpecified;
            }
            set
            {
                this.visivelFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Compravel
        {
            get
            {
                return this.compravelField;
            }
            set
            {
                this.compravelField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CompravelSpecified
        {
            get
            {
                return this.compravelFieldSpecified;
            }
            set
            {
                this.compravelFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class FatoresOrdenacao
    {

        private decimal disponibilidadeField;

        private bool disponibilidadeFieldSpecified;

        private decimal vendasField;

        private bool vendasFieldSpecified;

        private decimal dataLancamentoField;

        private bool dataLancamentoFieldSpecified;

        /// <remarks/>
        public decimal Disponibilidade
        {
            get
            {
                return this.disponibilidadeField;
            }
            set
            {
                this.disponibilidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DisponibilidadeSpecified
        {
            get
            {
                return this.disponibilidadeFieldSpecified;
            }
            set
            {
                this.disponibilidadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Vendas
        {
            get
            {
                return this.vendasField;
            }
            set
            {
                this.vendasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VendasSpecified
        {
            get
            {
                return this.vendasFieldSpecified;
            }
            set
            {
                this.vendasFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal DataLancamento
        {
            get
            {
                return this.dataLancamentoField;
            }
            set
            {
                this.dataLancamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataLancamentoSpecified
        {
            get
            {
                return this.dataLancamentoFieldSpecified;
            }
            set
            {
                this.dataLancamentoFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Seguro
    {

        private int idField;

        private bool idFieldSpecified;

        private int idStatusField;

        private bool idStatusFieldSpecified;

        private string statusField;

        private double precoFinalField;

        private bool precoFinalFieldSpecified;

        private string cdEstruturadoField;

        private string descricaoSeguroField;

        private float percentualSeguroField;

        private bool percentualSeguroFieldSpecified;

        private double valorSeguroField;

        private bool valorSeguroFieldSpecified;

        private int idProdutoSeguradoField;

        private bool idProdutoSeguradoFieldSpecified;

        private string certificadoField;

        private System.DateTime dataApoliceField;

        private bool dataApoliceFieldSpecified;

        private int vigenciaField;

        private bool vigenciaFieldSpecified;

        private int idTipoSeguroField;

        private bool idTipoSeguroFieldSpecified;

        private string tipoSeguroField;

        private int codigoItemSeguradoField;

        private bool codigoItemSeguradoFieldSpecified;

        private bool canceladoField;

        private bool canceladoFieldSpecified;

        /// <remarks/>
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSpecified
        {
            get
            {
                return this.idFieldSpecified;
            }
            set
            {
                this.idFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdStatus
        {
            get
            {
                return this.idStatusField;
            }
            set
            {
                this.idStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdStatusSpecified
        {
            get
            {
                return this.idStatusFieldSpecified;
            }
            set
            {
                this.idStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public double PrecoFinal
        {
            get
            {
                return this.precoFinalField;
            }
            set
            {
                this.precoFinalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoFinalSpecified
        {
            get
            {
                return this.precoFinalFieldSpecified;
            }
            set
            {
                this.precoFinalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string CdEstruturado
        {
            get
            {
                return this.cdEstruturadoField;
            }
            set
            {
                this.cdEstruturadoField = value;
            }
        }

        /// <remarks/>
        public string DescricaoSeguro
        {
            get
            {
                return this.descricaoSeguroField;
            }
            set
            {
                this.descricaoSeguroField = value;
            }
        }

        /// <remarks/>
        public float PercentualSeguro
        {
            get
            {
                return this.percentualSeguroField;
            }
            set
            {
                this.percentualSeguroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PercentualSeguroSpecified
        {
            get
            {
                return this.percentualSeguroFieldSpecified;
            }
            set
            {
                this.percentualSeguroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public double ValorSeguro
        {
            get
            {
                return this.valorSeguroField;
            }
            set
            {
                this.valorSeguroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ValorSeguroSpecified
        {
            get
            {
                return this.valorSeguroFieldSpecified;
            }
            set
            {
                this.valorSeguroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdProdutoSegurado
        {
            get
            {
                return this.idProdutoSeguradoField;
            }
            set
            {
                this.idProdutoSeguradoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdProdutoSeguradoSpecified
        {
            get
            {
                return this.idProdutoSeguradoFieldSpecified;
            }
            set
            {
                this.idProdutoSeguradoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Certificado
        {
            get
            {
                return this.certificadoField;
            }
            set
            {
                this.certificadoField = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataApolice
        {
            get
            {
                return this.dataApoliceField;
            }
            set
            {
                this.dataApoliceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataApoliceSpecified
        {
            get
            {
                return this.dataApoliceFieldSpecified;
            }
            set
            {
                this.dataApoliceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Vigencia
        {
            get
            {
                return this.vigenciaField;
            }
            set
            {
                this.vigenciaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool VigenciaSpecified
        {
            get
            {
                return this.vigenciaFieldSpecified;
            }
            set
            {
                this.vigenciaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdTipoSeguro
        {
            get
            {
                return this.idTipoSeguroField;
            }
            set
            {
                this.idTipoSeguroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdTipoSeguroSpecified
        {
            get
            {
                return this.idTipoSeguroFieldSpecified;
            }
            set
            {
                this.idTipoSeguroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TipoSeguro
        {
            get
            {
                return this.tipoSeguroField;
            }
            set
            {
                this.tipoSeguroField = value;
            }
        }

        /// <remarks/>
        public int CodigoItemSegurado
        {
            get
            {
                return this.codigoItemSeguradoField;
            }
            set
            {
                this.codigoItemSeguradoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CodigoItemSeguradoSpecified
        {
            get
            {
                return this.codigoItemSeguradoFieldSpecified;
            }
            set
            {
                this.codigoItemSeguradoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Cancelado
        {
            get
            {
                return this.canceladoField;
            }
            set
            {
                this.canceladoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CanceladoSpecified
        {
            get
            {
                return this.canceladoFieldSpecified;
            }
            set
            {
                this.canceladoFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Vinculo
    {

        private int idItemOrigemField;

        private bool idItemOrigemFieldSpecified;

        private int idProdutoOrigemField;

        private bool idProdutoOrigemFieldSpecified;

        private int idVinculoField;

        private bool idVinculoFieldSpecified;

        private int idTipoVinculoField;

        private bool idTipoVinculoFieldSpecified;

        private int quantidadeVinculadaField;

        private bool quantidadeVinculadaFieldSpecified;

        private int idEncomendaOrigemField;

        private bool idEncomendaOrigemFieldSpecified;

        /// <remarks/>
        public int IdItemOrigem
        {
            get
            {
                return this.idItemOrigemField;
            }
            set
            {
                this.idItemOrigemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdItemOrigemSpecified
        {
            get
            {
                return this.idItemOrigemFieldSpecified;
            }
            set
            {
                this.idItemOrigemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdProdutoOrigem
        {
            get
            {
                return this.idProdutoOrigemField;
            }
            set
            {
                this.idProdutoOrigemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdProdutoOrigemSpecified
        {
            get
            {
                return this.idProdutoOrigemFieldSpecified;
            }
            set
            {
                this.idProdutoOrigemFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdVinculo
        {
            get
            {
                return this.idVinculoField;
            }
            set
            {
                this.idVinculoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdVinculoSpecified
        {
            get
            {
                return this.idVinculoFieldSpecified;
            }
            set
            {
                this.idVinculoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdTipoVinculo
        {
            get
            {
                return this.idTipoVinculoField;
            }
            set
            {
                this.idTipoVinculoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdTipoVinculoSpecified
        {
            get
            {
                return this.idTipoVinculoFieldSpecified;
            }
            set
            {
                this.idTipoVinculoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int QuantidadeVinculada
        {
            get
            {
                return this.quantidadeVinculadaField;
            }
            set
            {
                this.quantidadeVinculadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeVinculadaSpecified
        {
            get
            {
                return this.quantidadeVinculadaFieldSpecified;
            }
            set
            {
                this.quantidadeVinculadaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdEncomendaOrigem
        {
            get
            {
                return this.idEncomendaOrigemField;
            }
            set
            {
                this.idEncomendaOrigemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdEncomendaOrigemSpecified
        {
            get
            {
                return this.idEncomendaOrigemFieldSpecified;
            }
            set
            {
                this.idEncomendaOrigemFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class PrazoFrete
    {

        private int prazoField;

        private bool prazoFieldSpecified;

        private PrazoFreteUnidade unidadeField;

        private bool unidadeFieldSpecified;

        private System.DateTime dataField;

        private bool dataFieldSpecified;

        /// <remarks/>
        public int Prazo
        {
            get
            {
                return this.prazoField;
            }
            set
            {
                this.prazoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrazoSpecified
        {
            get
            {
                return this.prazoFieldSpecified;
            }
            set
            {
                this.prazoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public PrazoFreteUnidade Unidade
        {
            get
            {
                return this.unidadeField;
            }
            set
            {
                this.unidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UnidadeSpecified
        {
            get
            {
                return this.unidadeFieldSpecified;
            }
            set
            {
                this.unidadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime Data
        {
            get
            {
                return this.dataField;
            }
            set
            {
                this.dataField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataSpecified
        {
            get
            {
                return this.dataFieldSpecified;
            }
            set
            {
                this.dataFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum PrazoFreteUnidade
    {

        /// <remarks/>
        dia,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("dia útil")]
        diaútil,

        /// <remarks/>
        dias,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("dias úteis")]
        diasúteis,

        /// <remarks/>
        semana,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("semana útil")]
        semanaútil,

        /// <remarks/>
        semanas,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("semanas úteis")]
        semanasúteis,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class EntregaAgendada
    {

        private bool manhaField;

        private bool manhaFieldSpecified;

        private bool tardeField;

        private bool tardeFieldSpecified;

        private bool noiteField;

        private bool noiteFieldSpecified;

        /// <remarks/>
        public bool Manha
        {
            get
            {
                return this.manhaField;
            }
            set
            {
                this.manhaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool ManhaSpecified
        {
            get
            {
                return this.manhaFieldSpecified;
            }
            set
            {
                this.manhaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Tarde
        {
            get
            {
                return this.tardeField;
            }
            set
            {
                this.tardeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TardeSpecified
        {
            get
            {
                return this.tardeFieldSpecified;
            }
            set
            {
                this.tardeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Noite
        {
            get
            {
                return this.noiteField;
            }
            set
            {
                this.noiteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NoiteSpecified
        {
            get
            {
                return this.noiteFieldSpecified;
            }
            set
            {
                this.noiteFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class OpcaoFrete
    {

        private int idOpcaoFreteField;

        private string nomeOpcaoFreteField;

        private int idCentroDistribuicaoField;

        private bool idCentroDistribuicaoFieldSpecified;

        private int idFulfillmentCenterField;

        private bool idFulfillmentCenterFieldSpecified;

        private Transportadora transportadoraField;

        private PrazoFrete postagemField;

        private PrazoFrete entregaField;

        private EntregaAgendada entregaAgendadaField;

        private decimal adValoremField;

        private bool adValoremFieldSpecified;

        private decimal fatorFreteField;

        private bool fatorFreteFieldSpecified;

        private decimal precoField;

        private bool precoFieldSpecified;

        private decimal precoAdicionalField;

        private bool precoAdicionalFieldSpecified;

        private decimal descontoField;

        private bool descontoFieldSpecified;

        private decimal totalFreteField;

        private bool totalFreteFieldSpecified;

        /// <remarks/>
        public int IdOpcaoFrete
        {
            get
            {
                return this.idOpcaoFreteField;
            }
            set
            {
                this.idOpcaoFreteField = value;
            }
        }

        /// <remarks/>
        public string NomeOpcaoFrete
        {
            get
            {
                return this.nomeOpcaoFreteField;
            }
            set
            {
                this.nomeOpcaoFreteField = value;
            }
        }

        /// <remarks/>
        public int IdCentroDistribuicao
        {
            get
            {
                return this.idCentroDistribuicaoField;
            }
            set
            {
                this.idCentroDistribuicaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCentroDistribuicaoSpecified
        {
            get
            {
                return this.idCentroDistribuicaoFieldSpecified;
            }
            set
            {
                this.idCentroDistribuicaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdFulfillmentCenter
        {
            get
            {
                return this.idFulfillmentCenterField;
            }
            set
            {
                this.idFulfillmentCenterField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdFulfillmentCenterSpecified
        {
            get
            {
                return this.idFulfillmentCenterFieldSpecified;
            }
            set
            {
                this.idFulfillmentCenterFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Transportadora Transportadora
        {
            get
            {
                return this.transportadoraField;
            }
            set
            {
                this.transportadoraField = value;
            }
        }

        /// <remarks/>
        public PrazoFrete Postagem
        {
            get
            {
                return this.postagemField;
            }
            set
            {
                this.postagemField = value;
            }
        }

        /// <remarks/>
        public PrazoFrete Entrega
        {
            get
            {
                return this.entregaField;
            }
            set
            {
                this.entregaField = value;
            }
        }

        /// <remarks/>
        public EntregaAgendada EntregaAgendada
        {
            get
            {
                return this.entregaAgendadaField;
            }
            set
            {
                this.entregaAgendadaField = value;
            }
        }

        /// <remarks/>
        public decimal AdValorem
        {
            get
            {
                return this.adValoremField;
            }
            set
            {
                this.adValoremField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AdValoremSpecified
        {
            get
            {
                return this.adValoremFieldSpecified;
            }
            set
            {
                this.adValoremFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal FatorFrete
        {
            get
            {
                return this.fatorFreteField;
            }
            set
            {
                this.fatorFreteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool FatorFreteSpecified
        {
            get
            {
                return this.fatorFreteFieldSpecified;
            }
            set
            {
                this.fatorFreteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Preco
        {
            get
            {
                return this.precoField;
            }
            set
            {
                this.precoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoSpecified
        {
            get
            {
                return this.precoFieldSpecified;
            }
            set
            {
                this.precoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal PrecoAdicional
        {
            get
            {
                return this.precoAdicionalField;
            }
            set
            {
                this.precoAdicionalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrecoAdicionalSpecified
        {
            get
            {
                return this.precoAdicionalFieldSpecified;
            }
            set
            {
                this.precoAdicionalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal Desconto
        {
            get
            {
                return this.descontoField;
            }
            set
            {
                this.descontoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DescontoSpecified
        {
            get
            {
                return this.descontoFieldSpecified;
            }
            set
            {
                this.descontoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalFrete
        {
            get
            {
                return this.totalFreteField;
            }
            set
            {
                this.totalFreteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalFreteSpecified
        {
            get
            {
                return this.totalFreteFieldSpecified;
            }
            set
            {
                this.totalFreteFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Transportadora
    {

        private int idContratoField;

        /// <remarks/>
        public int IdContrato
        {
            get
            {
                return this.idContratoField;
            }
            set
            {
                this.idContratoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Entregas
    {

        private List<Entrega> entregaField;

        private decimal totalFretesField;

        private bool totalFretesFieldSpecified;

        private decimal totalDescontoFretesField;

        private bool totalDescontoFretesFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Entrega")]
        public List<Entrega> Entrega
        {
            get
            {
                return this.entregaField;
            }
            set
            {
                this.entregaField = value;
            }
        }

        /// <remarks/>
        public decimal TotalFretes
        {
            get
            {
                return this.totalFretesField;
            }
            set
            {
                this.totalFretesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalFretesSpecified
        {
            get
            {
                return this.totalFretesFieldSpecified;
            }
            set
            {
                this.totalFretesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontoFretes
        {
            get
            {
                return this.totalDescontoFretesField;
            }
            set
            {
                this.totalDescontoFretesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontoFretesSpecified
        {
            get
            {
                return this.totalDescontoFretesFieldSpecified;
            }
            set
            {
                this.totalDescontoFretesFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Entrega
    {

        private int idEntregaField;

        private bool idEntregaFieldSpecified;

        private int idStatusField;

        private bool idStatusFieldSpecified;

        private string statusField;

        private int idMotivoField;

        private bool idMotivoFieldSpecified;

        private string motivoField;

        private bool entregaDigitalField;

        private bool entregaDigitalFieldSpecified;

        private Endereco enderecoEntregaField;

        private bool entregaLojaField;

        private bool entregaLojaFieldSpecified;

        private int idFilialField;

        private bool idFilialFieldSpecified;

        private bool entregaParceiroField;

        private bool entregaParceiroFieldSpecified;

        private int idParceiroField;

        private bool idParceiroFieldSpecified;

        private System.DateTime dataPostagemField;

        private bool dataPostagemFieldSpecified;

        private System.DateTime previsaoEntregaField;

        private bool previsaoEntregaFieldSpecified;

        private string numeroRastreamentoField;

        private Transportadora transportadoraField;

        private int quantidadePostadaField;

        private bool quantidadePostadaFieldSpecified;

        private Faturamentos faturamentosField;

        private OpcaoFrete freteField;

        private bool canceladoField;

        private bool canceladoFieldSpecified;

        private List<Item> itemField;

        /// <remarks/>
        public int IdEntrega
        {
            get
            {
                return this.idEntregaField;
            }
            set
            {
                this.idEntregaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdEntregaSpecified
        {
            get
            {
                return this.idEntregaFieldSpecified;
            }
            set
            {
                this.idEntregaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdStatus
        {
            get
            {
                return this.idStatusField;
            }
            set
            {
                this.idStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdStatusSpecified
        {
            get
            {
                return this.idStatusFieldSpecified;
            }
            set
            {
                this.idStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public int IdMotivo
        {
            get
            {
                return this.idMotivoField;
            }
            set
            {
                this.idMotivoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdMotivoSpecified
        {
            get
            {
                return this.idMotivoFieldSpecified;
            }
            set
            {
                this.idMotivoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Motivo
        {
            get
            {
                return this.motivoField;
            }
            set
            {
                this.motivoField = value;
            }
        }

        /// <remarks/>
        public bool EntregaDigital
        {
            get
            {
                return this.entregaDigitalField;
            }
            set
            {
                this.entregaDigitalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EntregaDigitalSpecified
        {
            get
            {
                return this.entregaDigitalFieldSpecified;
            }
            set
            {
                this.entregaDigitalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Endereco EnderecoEntrega
        {
            get
            {
                return this.enderecoEntregaField;
            }
            set
            {
                this.enderecoEntregaField = value;
            }
        }

        /// <remarks/>
        public bool EntregaLoja
        {
            get
            {
                return this.entregaLojaField;
            }
            set
            {
                this.entregaLojaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EntregaLojaSpecified
        {
            get
            {
                return this.entregaLojaFieldSpecified;
            }
            set
            {
                this.entregaLojaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdFilial
        {
            get
            {
                return this.idFilialField;
            }
            set
            {
                this.idFilialField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdFilialSpecified
        {
            get
            {
                return this.idFilialFieldSpecified;
            }
            set
            {
                this.idFilialFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool EntregaParceiro
        {
            get
            {
                return this.entregaParceiroField;
            }
            set
            {
                this.entregaParceiroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EntregaParceiroSpecified
        {
            get
            {
                return this.entregaParceiroFieldSpecified;
            }
            set
            {
                this.entregaParceiroFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdParceiro
        {
            get
            {
                return this.idParceiroField;
            }
            set
            {
                this.idParceiroField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdParceiroSpecified
        {
            get
            {
                return this.idParceiroFieldSpecified;
            }
            set
            {
                this.idParceiroFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DataPostagem
        {
            get
            {
                return this.dataPostagemField;
            }
            set
            {
                this.dataPostagemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataPostagemSpecified
        {
            get
            {
                return this.dataPostagemFieldSpecified;
            }
            set
            {
                this.dataPostagemFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime PrevisaoEntrega
        {
            get
            {
                return this.previsaoEntregaField;
            }
            set
            {
                this.previsaoEntregaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrevisaoEntregaSpecified
        {
            get
            {
                return this.previsaoEntregaFieldSpecified;
            }
            set
            {
                this.previsaoEntregaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string NumeroRastreamento
        {
            get
            {
                return this.numeroRastreamentoField;
            }
            set
            {
                this.numeroRastreamentoField = value;
            }
        }

        /// <remarks/>
        public Transportadora Transportadora
        {
            get
            {
                return this.transportadoraField;
            }
            set
            {
                this.transportadoraField = value;
            }
        }

        /// <remarks/>
        public int QuantidadePostada
        {
            get
            {
                return this.quantidadePostadaField;
            }
            set
            {
                this.quantidadePostadaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadePostadaSpecified
        {
            get
            {
                return this.quantidadePostadaFieldSpecified;
            }
            set
            {
                this.quantidadePostadaFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public Faturamentos Faturamentos
        {
            get
            {
                return this.faturamentosField;
            }
            set
            {
                this.faturamentosField = value;
            }
        }

        /// <remarks/>
        public OpcaoFrete Frete
        {
            get
            {
                return this.freteField;
            }
            set
            {
                this.freteField = value;
            }
        }

        /// <remarks/>
        public bool Cancelado
        {
            get
            {
                return this.canceladoField;
            }
            set
            {
                this.canceladoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CanceladoSpecified
        {
            get
            {
                return this.canceladoFieldSpecified;
            }
            set
            {
                this.canceladoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Item")]
        public List<Item> Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Endereco
    {

        private int idEnderecoField;

        private bool idEnderecoFieldSpecified;

        private string destinatarioField;

        private string logradouroField;

        private string numeroField;

        private string complementoField;

        private string referenciaField;

        private string bairroField;

        private string cidadeField;

        private int codMunicipioField;

        private bool codMunicipioFieldSpecified;

        private string ufField;

        private string cEPField;

        private string paisField;

        private ListaTelefone telefonesField;

        private int tipoField;

        private bool tipoFieldSpecified;

        private bool principalField;

        private bool principalFieldSpecified;

        /// <remarks/>
        public int IdEndereco
        {
            get
            {
                return this.idEnderecoField;
            }
            set
            {
                this.idEnderecoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdEnderecoSpecified
        {
            get
            {
                return this.idEnderecoFieldSpecified;
            }
            set
            {
                this.idEnderecoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Destinatario
        {
            get
            {
                return this.destinatarioField;
            }
            set
            {
                this.destinatarioField = value;
            }
        }

        /// <remarks/>
        public string Logradouro
        {
            get
            {
                return this.logradouroField;
            }
            set
            {
                this.logradouroField = value;
            }
        }

        /// <remarks/>
        public string Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        public string Complemento
        {
            get
            {
                return this.complementoField;
            }
            set
            {
                this.complementoField = value;
            }
        }

        /// <remarks/>
        public string Referencia
        {
            get
            {
                return this.referenciaField;
            }
            set
            {
                this.referenciaField = value;
            }
        }

        /// <remarks/>
        public string Bairro
        {
            get
            {
                return this.bairroField;
            }
            set
            {
                this.bairroField = value;
            }
        }

        /// <remarks/>
        public string Cidade
        {
            get
            {
                return this.cidadeField;
            }
            set
            {
                this.cidadeField = value;
            }
        }

        /// <remarks/>
        public int CodMunicipio
        {
            get
            {
                return this.codMunicipioField;
            }
            set
            {
                this.codMunicipioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CodMunicipioSpecified
        {
            get
            {
                return this.codMunicipioFieldSpecified;
            }
            set
            {
                this.codMunicipioFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string UF
        {
            get
            {
                return this.ufField;
            }
            set
            {
                this.ufField = value;
            }
        }

        /// <remarks/>
        public string CEP
        {
            get
            {
                return this.cEPField;
            }
            set
            {
                this.cEPField = value;
            }
        }

        /// <remarks/>
        public string Pais
        {
            get
            {
                return this.paisField;
            }
            set
            {
                this.paisField = value;
            }
        }

        /// <remarks/>
        public ListaTelefone Telefones
        {
            get
            {
                return this.telefonesField;
            }
            set
            {
                this.telefonesField = value;
            }
        }

        /// <remarks/>
        public int Tipo
        {
            get
            {
                return this.tipoField;
            }
            set
            {
                this.tipoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TipoSpecified
        {
            get
            {
                return this.tipoFieldSpecified;
            }
            set
            {
                this.tipoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Principal
        {
            get
            {
                return this.principalField;
            }
            set
            {
                this.principalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrincipalSpecified
        {
            get
            {
                return this.principalFieldSpecified;
            }
            set
            {
                this.principalFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ListaTelefone
    {

        private List<Telefone> fixoField;

        private List<Telefone> celularField;

        private List<Telefone> outrosField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Fixo")]
        public List<Telefone> Fixo
        {
            get
            {
                return this.fixoField;
            }
            set
            {
                this.fixoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Celular")]
        public List<Telefone> Celular
        {
            get
            {
                return this.celularField;
            }
            set
            {
                this.celularField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Outros")]
        public List<Telefone> Outros
        {
            get
            {
                return this.outrosField;
            }
            set
            {
                this.outrosField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Telefone
    {

        private int idTelefoneField;

        private bool idTelefoneFieldSpecified;

        private string dDIField;

        private string dDDField;

        private string numeroField;

        private string ramalField;

        /// <remarks/>
        public int IdTelefone
        {
            get
            {
                return this.idTelefoneField;
            }
            set
            {
                this.idTelefoneField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdTelefoneSpecified
        {
            get
            {
                return this.idTelefoneFieldSpecified;
            }
            set
            {
                this.idTelefoneFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string DDI
        {
            get
            {
                return this.dDIField;
            }
            set
            {
                this.dDIField = value;
            }
        }

        /// <remarks/>
        public string DDD
        {
            get
            {
                return this.dDDField;
            }
            set
            {
                this.dDDField = value;
            }
        }

        /// <remarks/>
        public string Numero
        {
            get
            {
                return this.numeroField;
            }
            set
            {
                this.numeroField = value;
            }
        }

        /// <remarks/>
        public string Ramal
        {
            get
            {
                return this.ramalField;
            }
            set
            {
                this.ramalField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Faturamento
    {

        private int idField;

        private bool idFieldSpecified;

        private int idProdutoField;

        private bool idProdutoFieldSpecified;

        private System.DateTime dataFaturamentoField;

        private bool dataFaturamentoFieldSpecified;

        private int quantidadeField;

        private bool quantidadeFieldSpecified;

        private int numeroNotaFiscalField;

        private bool numeroNotaFiscalFieldSpecified;

        private bool canceladoField;

        private bool canceladoFieldSpecified;

        private int idFilialField;

        private bool idFilialFieldSpecified;

        private string serieField;

        private string codigoChaveAcessoField;

        private string codigoChaveAcessoCntgField;

        private Entregas entregasField;

        /// <remarks/>
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSpecified
        {
            get
            {
                return this.idFieldSpecified;
            }
            set
            {
                this.idFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdProduto
        {
            get
            {
                return this.idProdutoField;
            }
            set
            {
                this.idProdutoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdProdutoSpecified
        {
            get
            {
                return this.idProdutoFieldSpecified;
            }
            set
            {
                this.idProdutoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataFaturamento
        {
            get
            {
                return this.dataFaturamentoField;
            }
            set
            {
                this.dataFaturamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataFaturamentoSpecified
        {
            get
            {
                return this.dataFaturamentoFieldSpecified;
            }
            set
            {
                this.dataFaturamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Quantidade
        {
            get
            {
                return this.quantidadeField;
            }
            set
            {
                this.quantidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeSpecified
        {
            get
            {
                return this.quantidadeFieldSpecified;
            }
            set
            {
                this.quantidadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int NumeroNotaFiscal
        {
            get
            {
                return this.numeroNotaFiscalField;
            }
            set
            {
                this.numeroNotaFiscalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroNotaFiscalSpecified
        {
            get
            {
                return this.numeroNotaFiscalFieldSpecified;
            }
            set
            {
                this.numeroNotaFiscalFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Cancelado
        {
            get
            {
                return this.canceladoField;
            }
            set
            {
                this.canceladoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CanceladoSpecified
        {
            get
            {
                return this.canceladoFieldSpecified;
            }
            set
            {
                this.canceladoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdFilial
        {
            get
            {
                return this.idFilialField;
            }
            set
            {
                this.idFilialField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdFilialSpecified
        {
            get
            {
                return this.idFilialFieldSpecified;
            }
            set
            {
                this.idFilialFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Serie
        {
            get
            {
                return this.serieField;
            }
            set
            {
                this.serieField = value;
            }
        }

        /// <remarks/>
        public string CodigoChaveAcesso
        {
            get
            {
                return this.codigoChaveAcessoField;
            }
            set
            {
                this.codigoChaveAcessoField = value;
            }
        }

        /// <remarks/>
        public string CodigoChaveAcessoCntg
        {
            get
            {
                return this.codigoChaveAcessoCntgField;
            }
            set
            {
                this.codigoChaveAcessoCntgField = value;
            }
        }

        /// <remarks/>
        public Entregas Entregas
        {
            get
            {
                return this.entregasField;
            }
            set
            {
                this.entregasField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class GarantiaEstendida
    {

        private int idField;

        private bool idFieldSpecified;

        private int idStatusField;

        private bool idStatusFieldSpecified;

        private string statusField;

        private int idProdutoField;

        private bool idProdutoFieldSpecified;

        private int idProdutoSeguradoField;

        private bool idProdutoSeguradoFieldSpecified;

        private int mesesField;

        private bool mesesFieldSpecified;

        private int garantiaFabricanteField;

        private bool garantiaFabricanteFieldSpecified;

        private Preco premioField;

        private Preco custoField;

        private int numeroApoliceField;

        private bool numeroApoliceFieldSpecified;

        private System.DateTime dataApoliceField;

        private bool dataApoliceFieldSpecified;

        private bool canceladoField;

        private bool canceladoFieldSpecified;

        /// <remarks/>
        public int Id
        {
            get
            {
                return this.idField;
            }
            set
            {
                this.idField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdSpecified
        {
            get
            {
                return this.idFieldSpecified;
            }
            set
            {
                this.idFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdStatus
        {
            get
            {
                return this.idStatusField;
            }
            set
            {
                this.idStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdStatusSpecified
        {
            get
            {
                return this.idStatusFieldSpecified;
            }
            set
            {
                this.idStatusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        public int IdProduto
        {
            get
            {
                return this.idProdutoField;
            }
            set
            {
                this.idProdutoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdProdutoSpecified
        {
            get
            {
                return this.idProdutoFieldSpecified;
            }
            set
            {
                this.idProdutoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdProdutoSegurado
        {
            get
            {
                return this.idProdutoSeguradoField;
            }
            set
            {
                this.idProdutoSeguradoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdProdutoSeguradoSpecified
        {
            get
            {
                return this.idProdutoSeguradoFieldSpecified;
            }
            set
            {
                this.idProdutoSeguradoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Meses
        {
            get
            {
                return this.mesesField;
            }
            set
            {
                this.mesesField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool MesesSpecified
        {
            get
            {
                return this.mesesFieldSpecified;
            }
            set
            {
                this.mesesFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int GarantiaFabricante
        {
            get
            {
                return this.garantiaFabricanteField;
            }
            set
            {
                this.garantiaFabricanteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool GarantiaFabricanteSpecified
        {
            get
            {
                return this.garantiaFabricanteFieldSpecified;
            }
            set
            {
                this.garantiaFabricanteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Preco Premio
        {
            get
            {
                return this.premioField;
            }
            set
            {
                this.premioField = value;
            }
        }

        /// <remarks/>
        public Preco Custo
        {
            get
            {
                return this.custoField;
            }
            set
            {
                this.custoField = value;
            }
        }

        /// <remarks/>
        public int NumeroApolice
        {
            get
            {
                return this.numeroApoliceField;
            }
            set
            {
                this.numeroApoliceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroApoliceSpecified
        {
            get
            {
                return this.numeroApoliceFieldSpecified;
            }
            set
            {
                this.numeroApoliceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataApolice
        {
            get
            {
                return this.dataApoliceField;
            }
            set
            {
                this.dataApoliceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataApoliceSpecified
        {
            get
            {
                return this.dataApoliceFieldSpecified;
            }
            set
            {
                this.dataApoliceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool Cancelado
        {
            get
            {
                return this.canceladoField;
            }
            set
            {
                this.canceladoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CanceladoSpecified
        {
            get
            {
                return this.canceladoFieldSpecified;
            }
            set
            {
                this.canceladoFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Preco
    {

        private PrecoMoeda moedaField;

        private decimal valorField;

        private int idListaField;

        private bool idListaFieldSpecified;

        private System.DateTime dataInicioField;

        private bool dataInicioFieldSpecified;

        private System.DateTime dataFimField;

        private bool dataFimFieldSpecified;

        /// <remarks/>
        public PrecoMoeda Moeda
        {
            get
            {
                return this.moedaField;
            }
            set
            {
                this.moedaField = value;
            }
        }

        /// <remarks/>
        public decimal Valor
        {
            get
            {
                return this.valorField;
            }
            set
            {
                this.valorField = value;
            }
        }

        /// <remarks/>
        public int IdLista
        {
            get
            {
                return this.idListaField;
            }
            set
            {
                this.idListaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdListaSpecified
        {
            get
            {
                return this.idListaFieldSpecified;
            }
            set
            {
                this.idListaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataInicio
        {
            get
            {
                return this.dataInicioField;
            }
            set
            {
                this.dataInicioField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataInicioSpecified
        {
            get
            {
                return this.dataInicioFieldSpecified;
            }
            set
            {
                this.dataInicioFieldSpecified = value;
            }
        }

        /// <remarks/>
        public System.DateTime DataFim
        {
            get
            {
                return this.dataFimField;
            }
            set
            {
                this.dataFimField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataFimSpecified
        {
            get
            {
                return this.dataFimFieldSpecified;
            }
            set
            {
                this.dataFimFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum PrecoMoeda
    {

        /// <remarks/>
        BRL,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Prorrogacao
    {

        private int prazoField;

        private bool prazoFieldSpecified;

        private ProrrogacaoUnidade unidadeField;

        private bool unidadeFieldSpecified;

        /// <remarks/>
        public int Prazo
        {
            get
            {
                return this.prazoField;
            }
            set
            {
                this.prazoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool PrazoSpecified
        {
            get
            {
                return this.prazoFieldSpecified;
            }
            set
            {
                this.prazoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public ProrrogacaoUnidade Unidade
        {
            get
            {
                return this.unidadeField;
            }
            set
            {
                this.unidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool UnidadeSpecified
        {
            get
            {
                return this.unidadeFieldSpecified;
            }
            set
            {
                this.unidadeFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum ProrrogacaoUnidade
    {

        /// <remarks/>
        dia,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("dia útil")]
        diaútil,

        /// <remarks/>
        dias,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("dias úteis")]
        diasúteis,

        /// <remarks/>
        semana,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("semana útil")]
        semanaútil,

        /// <remarks/>
        semanas,

        /// <remarks/>
        [System.Xml.Serialization.XmlEnumAttribute("semanas úteis")]
        semanasúteis,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum ItemTipoServico
    {

        /// <remarks/>
        Garantia,

        /// <remarks/>
        Seguro,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ValidacaoItem : Item
    {

        private int statusField;

        private bool statusFieldSpecified;

        private string statusMessageField;

        /// <remarks/>
        public int Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StatusSpecified
        {
            get
            {
                return this.statusFieldSpecified;
            }
            set
            {
                this.statusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string StatusMessage
        {
            get
            {
                return this.statusMessageField;
            }
            set
            {
                this.statusMessageField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class Perfil
    {

        private System.DateTime dataNascimentoField;

        private bool dataNascimentoFieldSpecified;

        private PerfilSexo sexoField;

        private bool sexoFieldSpecified;

        private int estadoCivilField;

        private bool estadoCivilFieldSpecified;

        private int numeroFilhosField;

        private bool numeroFilhosFieldSpecified;

        private int ocupacaoField;

        private bool ocupacaoFieldSpecified;

        private bool aceitaMalaDiretaField;

        private string autenticadorField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime DataNascimento
        {
            get
            {
                return this.dataNascimentoField;
            }
            set
            {
                this.dataNascimentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataNascimentoSpecified
        {
            get
            {
                return this.dataNascimentoFieldSpecified;
            }
            set
            {
                this.dataNascimentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public PerfilSexo Sexo
        {
            get
            {
                return this.sexoField;
            }
            set
            {
                this.sexoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool SexoSpecified
        {
            get
            {
                return this.sexoFieldSpecified;
            }
            set
            {
                this.sexoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int EstadoCivil
        {
            get
            {
                return this.estadoCivilField;
            }
            set
            {
                this.estadoCivilField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EstadoCivilSpecified
        {
            get
            {
                return this.estadoCivilFieldSpecified;
            }
            set
            {
                this.estadoCivilFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int NumeroFilhos
        {
            get
            {
                return this.numeroFilhosField;
            }
            set
            {
                this.numeroFilhosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool NumeroFilhosSpecified
        {
            get
            {
                return this.numeroFilhosFieldSpecified;
            }
            set
            {
                this.numeroFilhosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int Ocupacao
        {
            get
            {
                return this.ocupacaoField;
            }
            set
            {
                this.ocupacaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool OcupacaoSpecified
        {
            get
            {
                return this.ocupacaoFieldSpecified;
            }
            set
            {
                this.ocupacaoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public bool AceitaMalaDireta
        {
            get
            {
                return this.aceitaMalaDiretaField;
            }
            set
            {
                this.aceitaMalaDiretaField = value;
            }
        }

        /// <remarks/>
        public string Autenticador
        {
            get
            {
                return this.autenticadorField;
            }
            set
            {
                this.autenticadorField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://www.saraiva.com.br/")]
    public enum PerfilSexo
    {

        /// <remarks/>
        M,

        /// <remarks/>
        F,
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute("Enderecos", Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class ListaEndereco
    {

        private int totalPaginasField;

        private bool totalPaginasFieldSpecified;

        private List<Endereco> enderecoField;

        /// <remarks/>
        public int TotalPaginas
        {
            get
            {
                return this.totalPaginasField;
            }
            set
            {
                this.totalPaginasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalPaginasSpecified
        {
            get
            {
                return this.totalPaginasFieldSpecified;
            }
            set
            {
                this.totalPaginasFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Endereco")]
        public List<Endereco> Endereco
        {
            get
            {
                return this.enderecoField;
            }
            set
            {
                this.enderecoField = value;
            }
        }
    }

    /// <remarks/>
    [System.Xml.Serialization.XmlIncludeAttribute(typeof(ValidacaoCarrinho))]
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Carrinho
    {

        private int idClienteField;

        private bool idClienteFieldSpecified;

        private int idCarrinhoField;

        private bool idCarrinhoFieldSpecified;

        private List<Item> itemField;

        private decimal totalProdutosField;

        private bool totalProdutosFieldSpecified;

        private decimal totalDescontosUnitariosField;

        private bool totalDescontosUnitariosFieldSpecified;

        private decimal totalDescontosPagamentosField;

        private bool totalDescontosPagamentosFieldSpecified;

        private decimal totalDescontosCuponsField;

        private bool totalDescontosCuponsFieldSpecified;

        private decimal totalDescontosSaraivaPlusField;

        private bool totalDescontosSaraivaPlusFieldSpecified;

        private decimal totalPontosSaraivaPlusField;

        private bool totalPontosSaraivaPlusFieldSpecified;

        private decimal totalDescontosField;

        private bool totalDescontosFieldSpecified;

        private decimal totalAcrescimosField;

        private bool totalAcrescimosFieldSpecified;

        private decimal totalEmbrulhosField;

        private bool totalEmbrulhosFieldSpecified;

        private decimal totalCarrinhoField;

        private bool totalCarrinhoFieldSpecified;

        private decimal totalDescontoAdicionalField;

        private bool totalDescontoAdicionalFieldSpecified;

        /// <remarks/>
        public int IdCliente
        {
            get
            {
                return this.idClienteField;
            }
            set
            {
                this.idClienteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdClienteSpecified
        {
            get
            {
                return this.idClienteFieldSpecified;
            }
            set
            {
                this.idClienteFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdCarrinho
        {
            get
            {
                return this.idCarrinhoField;
            }
            set
            {
                this.idCarrinhoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCarrinhoSpecified
        {
            get
            {
                return this.idCarrinhoFieldSpecified;
            }
            set
            {
                this.idCarrinhoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Item")]
        public List<Item> Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        public decimal TotalProdutos
        {
            get
            {
                return this.totalProdutosField;
            }
            set
            {
                this.totalProdutosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalProdutosSpecified
        {
            get
            {
                return this.totalProdutosFieldSpecified;
            }
            set
            {
                this.totalProdutosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontosUnitarios
        {
            get
            {
                return this.totalDescontosUnitariosField;
            }
            set
            {
                this.totalDescontosUnitariosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontosUnitariosSpecified
        {
            get
            {
                return this.totalDescontosUnitariosFieldSpecified;
            }
            set
            {
                this.totalDescontosUnitariosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontosPagamentos
        {
            get
            {
                return this.totalDescontosPagamentosField;
            }
            set
            {
                this.totalDescontosPagamentosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontosPagamentosSpecified
        {
            get
            {
                return this.totalDescontosPagamentosFieldSpecified;
            }
            set
            {
                this.totalDescontosPagamentosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontosCupons
        {
            get
            {
                return this.totalDescontosCuponsField;
            }
            set
            {
                this.totalDescontosCuponsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontosCuponsSpecified
        {
            get
            {
                return this.totalDescontosCuponsFieldSpecified;
            }
            set
            {
                this.totalDescontosCuponsFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontosSaraivaPlus
        {
            get
            {
                return this.totalDescontosSaraivaPlusField;
            }
            set
            {
                this.totalDescontosSaraivaPlusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontosSaraivaPlusSpecified
        {
            get
            {
                return this.totalDescontosSaraivaPlusFieldSpecified;
            }
            set
            {
                this.totalDescontosSaraivaPlusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalPontosSaraivaPlus
        {
            get
            {
                return this.totalPontosSaraivaPlusField;
            }
            set
            {
                this.totalPontosSaraivaPlusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalPontosSaraivaPlusSpecified
        {
            get
            {
                return this.totalPontosSaraivaPlusFieldSpecified;
            }
            set
            {
                this.totalPontosSaraivaPlusFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontos
        {
            get
            {
                return this.totalDescontosField;
            }
            set
            {
                this.totalDescontosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontosSpecified
        {
            get
            {
                return this.totalDescontosFieldSpecified;
            }
            set
            {
                this.totalDescontosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalAcrescimos
        {
            get
            {
                return this.totalAcrescimosField;
            }
            set
            {
                this.totalAcrescimosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalAcrescimosSpecified
        {
            get
            {
                return this.totalAcrescimosFieldSpecified;
            }
            set
            {
                this.totalAcrescimosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalEmbrulhos
        {
            get
            {
                return this.totalEmbrulhosField;
            }
            set
            {
                this.totalEmbrulhosField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalEmbrulhosSpecified
        {
            get
            {
                return this.totalEmbrulhosFieldSpecified;
            }
            set
            {
                this.totalEmbrulhosFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalCarrinho
        {
            get
            {
                return this.totalCarrinhoField;
            }
            set
            {
                this.totalCarrinhoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalCarrinhoSpecified
        {
            get
            {
                return this.totalCarrinhoFieldSpecified;
            }
            set
            {
                this.totalCarrinhoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public decimal TotalDescontoAdicional
        {
            get
            {
                return this.totalDescontoAdicionalField;
            }
            set
            {
                this.totalDescontoAdicionalField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalDescontoAdicionalSpecified
        {
            get
            {
                return this.totalDescontoAdicionalFieldSpecified;
            }
            set
            {
                this.totalDescontoAdicionalFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    public partial class ValidacaoCarrinho : Carrinho
    {

        private List<ValidacaoItem> validacaoItemField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("ValidacaoItem")]
        public List<ValidacaoItem> ValidacaoItem
        {
            get
            {
                return this.validacaoItemField;
            }
            set
            {
                this.validacaoItemField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute("Pedidos", Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class ListaPedido
    {

        private int totalPaginasField;

        private bool totalPaginasFieldSpecified;

        private List<Pedido> pedidoField;

        /// <remarks/>
        public int TotalPaginas
        {
            get
            {
                return this.totalPaginasField;
            }
            set
            {
                this.totalPaginasField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalPaginasSpecified
        {
            get
            {
                return this.totalPaginasFieldSpecified;
            }
            set
            {
                this.totalPaginasFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Pedido")]
        public List<Pedido> Pedido
        {
            get
            {
                return this.pedidoField;
            }
            set
            {
                this.pedidoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Catalogo
    {

        private int idLojaField;

        private bool idLojaFieldSpecified;

        private int idCatalogoField;

        private bool idCatalogoFieldSpecified;

        private string nomeField;

        private string descricaoField;

        private List<Categoria> categoriasField;

        /// <remarks/>
        public int IdLoja
        {
            get
            {
                return this.idLojaField;
            }
            set
            {
                this.idLojaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdLojaSpecified
        {
            get
            {
                return this.idLojaFieldSpecified;
            }
            set
            {
                this.idLojaFieldSpecified = value;
            }
        }

        /// <remarks/>
        public int IdCatalogo
        {
            get
            {
                return this.idCatalogoField;
            }
            set
            {
                this.idCatalogoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdCatalogoSpecified
        {
            get
            {
                return this.idCatalogoFieldSpecified;
            }
            set
            {
                this.idCatalogoFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Nome
        {
            get
            {
                return this.nomeField;
            }
            set
            {
                this.nomeField = value;
            }
        }

        /// <remarks/>
        public string Descricao
        {
            get
            {
                return this.descricaoField;
            }
            set
            {
                this.descricaoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Categoria> Categorias
        {
            get
            {
                return this.categoriasField;
            }
            set
            {
                this.categoriasField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute("Seguros", Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class ListaSeguro
    {

        private List<Seguro> seguroField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Seguro")]
        public List<Seguro> Seguro
        {
            get
            {
                return this.seguroField;
            }
            set
            {
                this.seguroField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute("GarantiasEstendidas", Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class ListaGarantiaEstendida
    {

        private List<GarantiaEstendida> garantiaEstendidaField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("GarantiaEstendida")]
        public List<GarantiaEstendida> GarantiaEstendida
        {
            get
            {
                return this.garantiaEstendidaField;
            }
            set
            {
                this.garantiaEstendidaField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Precos
    {

        private int idPacField;

        private bool idPacFieldSpecified;

        private Produto produtoField;

        private List<Preco> precoVitrineField;

        private List<Preco> precoOfertaField;

        private List<Preco> precoPreferencialField;

        /// <remarks/>
        public int IdPac
        {
            get
            {
                return this.idPacField;
            }
            set
            {
                this.idPacField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool IdPacSpecified
        {
            get
            {
                return this.idPacFieldSpecified;
            }
            set
            {
                this.idPacFieldSpecified = value;
            }
        }

        /// <remarks/>
        public Produto Produto
        {
            get
            {
                return this.produtoField;
            }
            set
            {
                this.produtoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Preco> PrecoVitrine
        {
            get
            {
                return this.precoVitrineField;
            }
            set
            {
                this.precoVitrineField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Preco> PrecoOferta
        {
            get
            {
                return this.precoOfertaField;
            }
            set
            {
                this.precoOfertaField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlArrayItemAttribute(IsNullable = false)]
        public List<Preco> PrecoPreferencial
        {
            get
            {
                return this.precoPreferencialField;
            }
            set
            {
                this.precoPreferencialField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Pagamento
    {

        private List<OpcaoPagamento> opcaoPagamentoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("OpcaoPagamento")]
        public List<OpcaoPagamento> OpcaoPagamento
        {
            get
            {
                return this.opcaoPagamentoField;
            }
            set
            {
                this.opcaoPagamentoField = value;
            }
        }
    }

    /// <remarks/>
    [System.CodeDom.Compiler.GeneratedCodeAttribute("xsd", "4.6.1055.0")]
    [System.SerializableAttribute()]
    [System.Diagnostics.DebuggerStepThroughAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://www.saraiva.com.br/")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://www.saraiva.com.br/", IsNullable = false)]
    public partial class Faturamentos
    {

        private List<Faturamento> faturamentoField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Faturamento")]
        public List<Faturamento> Faturamento
        {
            get
            {
                return this.faturamentoField;
            }
            set
            {
                this.faturamentoField = value;
            }
        }
    }
}