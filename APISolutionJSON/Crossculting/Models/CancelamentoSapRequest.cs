﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;


namespace Saraiva.SE.Crossculting.Domain.Model
{
    [Serializable]
    [XmlRoot(ElementName = "MT_CANC_OV_REQ")]
    public class CancelamentoSapRequest
    {
        public CancelamentoSapRequest()
        {
            Item = new List<CancelSapItem>();
        }

        [XmlElement(ElementName = "Header")]
        public Header header { get; set; }
        [XmlElement(ElementName = "Item")]
        public List<CancelSapItem> Item { get; set; }
    }

    [XmlRoot(ElementName = "Header")]
    public class Header
    {
        [XmlElement(ElementName = "IdPedidoMagento")]
        public int IdPedidoMagento { get; set; }
        [XmlElement(ElementName = "DataPedido")]
        public DateTime DataPedido { get; set; }
        [XmlElement(ElementName = "CartaoPresente")]
        public bool CartaoPresente { get; set; }
    }

    [XmlRoot(ElementName = "Item")]
    public class CancelSapItem
    {
        [XmlElement(ElementName = "Produto")]
        public string Produto { get; set; }
        [XmlElement(ElementName = "Item")]
        public int item { get; set; }
        [XmlElement(ElementName = "Quantidade")]
        public int Quantidade { get; set; }
    }

}
