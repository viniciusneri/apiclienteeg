﻿using Saraiva.SE.Crossculting.Helper;
using Saraiva.SE.Crossculting.Infraestructure.Context;
using System;
using System.Data;
using System.Data.SqlClient;

namespace Saraiva.SE.Crossculting.Infraestructure.Repository
{
    public class NewIdRepository: ContextSqlServer
    {
        public NewIdRepository() : base(HelperConfig.GetConfigByLabel("Connection.json", "Connections", "l016nt01")) { }

        public int getNewID(string siglaTabela, SqlConnection strans = null, SqlTransaction sTransactionScope = null)
        {
            if (strans != null)
                _connection = strans;

            int newID = 0;
            using (SqlCommand command = new SqlCommand("STP_GETID", _connection))
            {
                command.Parameters.Clear();

                command.Parameters.Add(new SqlParameter("@pSiglaTab", siglaTabela));
                command.Transaction = sTransactionScope;

                SqlParameter newId = new SqlParameter();
                newId.ParameterName = "@oId";
                newId.DbType = DbType.Int32;
                newId.Direction = ParameterDirection.Output;
                command.Parameters.Add(newId);

                command.CommandType = System.Data.CommandType.StoredProcedure;
                command.ExecuteNonQuery();

                if (command.Parameters["@oId"].Value != null)
                    newID = Convert.ToInt32(command.Parameters["@oId"].Value);
            }
            return newID;
        }

    }
}
