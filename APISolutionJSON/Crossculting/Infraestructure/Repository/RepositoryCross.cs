﻿using MongoDB.Bson;
using MongoDB.Driver;
using Saraiva.SE.Crossculting.Helper;

namespace Saraiva.SE.Crossculting.Infraestructure.Repository
{
    public class RepositoryCross : MongoClient
    {
        private readonly string _baseName = "APIRETAGUARDA";
        private string _collectionSetted;
        public readonly IMongoDatabase _database;

        public RepositoryCross(string collectionName) :
               base(HelperConfig.GetConfigByLabel("appsettings.json", "Connections" , "MongoConnection"))
        {
            _collectionSetted = collectionName;
            _database = GetDatabase(_baseName);
        }

        public void Insert<T>(T objectInput)
        {
            var collection = _database.GetCollection<BsonDocument>(_collectionSetted);
            var serializedObject = objectInput.ToBsonDocument();
            collection.InsertOne(serializedObject);
        }
    }
}
