﻿using System;
using System.Data.SqlClient;

namespace Saraiva.SE.Crossculting.Infraestructure.Context
{
    public class ContextSqlServer
    {
        private readonly string _connectionStringRetaguarda;
        public SqlConnection _connection;

        public ContextSqlServer(string sConnectionString)
        {
            _connectionStringRetaguarda = sConnectionString;            
            _connection = new SqlConnection(_connectionStringRetaguarda);
        }

        ~ContextSqlServer()
        {
            GC.SuppressFinalize(this);
        }
    }
}
