﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Saraiva.SE.Crossculting.Helper
{
    public class HelperCorreios
    {
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public string UrlWebServiceCorreios { get; set; }
        public string UrlXmlCorreios { get; set; }
        public int CodAdministrativo { get; set; }
        public string Contrato { get; set; }
        public int CodServico { get; set; }
        public int Cartao { get; set; }
        //      "ConfigCorreios": {
        //"Usuario": "empresacws",
        //"Senha": "123456",
        //"UrlWebServiceCorreios": "https://apphom.correios.com.br/logisticaReversaWS/logisticaReversaService/logisticaReversaWS?wsdl",
        //"UrlXmlCorreios": "http://service.logisticareversa.correios.com.br/",
        //"CodAdministrativo": "17000190",
        //"Contrato": "9992157880",
        //"CodServico": "04677",
        //"Cartao": "0067599079"

    }
}
