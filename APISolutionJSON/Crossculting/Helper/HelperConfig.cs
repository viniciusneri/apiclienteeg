﻿using Microsoft.Extensions.Configuration;
using System.IO;

namespace Saraiva.SE.Crossculting.Helper
{
    public class HelperConfig
    {
        public static string GetConfigByLabel(string pFile, string pNode, string pLabel)
        {
            var appSettings = "appsettings.json";
#if DEBUG
            appSettings = "appsettings.Development.json";
#endif

            var builder = new ConfigurationBuilder();
            builder.SetBasePath(Directory.GetCurrentDirectory());
            pFile = appSettings;
            builder.AddJsonFile(string.Concat(pFile));
            var config = builder.Build();
            return config.GetSection(pNode)[pLabel];
        }
    }
}