﻿using System;
using System.Linq;

namespace Saraiva.SE.Crossculting.Helper
{
    public static class HelperMapper
    {
        public static T ToObjectInstance<T>(this object objectInsert)
        {
            var objectReturn = Activator.CreateInstance<T>();
            var filedsInsert = objectReturn.GetType().GetProperties();

            filedsInsert.ToList().ForEach(it => {
                try
                {
                    var fieldInsert = objectInsert.GetType().GetProperty(it.Name);
                    var valueInsert = fieldInsert.GetValue(objectInsert);
                    it.SetValue(objectReturn, valueInsert);
                }
                catch { }
            });

            return objectReturn;
        }
    }
}
