﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Saraiva.SE.Crossculting.Helper
{
    public class HelperQuery
    {
        public static bool IsValueMin(object value)
        {
            var returnBool = true;
            var typeObject = value.GetType().GetElementType();
            if (typeObject == typeof(DateTime))
                if (Convert.ToDateTime(value) == DateTime.MinValue) returnBool = false;
            if (typeObject == typeof(int))
                if ((int)value == int.MinValue) returnBool = false;
            if (typeObject == typeof(string))
                if ((string)value == string.Empty) returnBool = false;
            if (typeObject == typeof(decimal))
                if ((decimal)value == decimal.MinValue) returnBool = false;

            return returnBool;
        }

        public static string GenerateQuerySelect<T>(string tableName, T parameters, string top = null)
        {
            var obj = Activator.CreateInstance<T>();
            var prop = obj.GetType().GetProperties(BindingFlags.Public | BindingFlags.Instance);
            var sQueryFormat = "Select {0} From {1} ";

            string sFields = "*";
            string sParameters = string.Empty;

            prop.ToList().ForEach(p =>
            {
                if (sFields == "*")
                    sFields = p.Name;
                else
                    sFields += string.Concat(Environment.NewLine, ",", p.Name);

                var valueWhere = parameters?.GetType().GetProperty(p.Name).GetValue(parameters);
                if (!IsValueMin(valueWhere))
                {
                    if (sParameters == string.Empty)
                        sParameters = string.Concat("Where ", p.Name, " = ", valueWhere);
                    else
                        sParameters = string.Concat("and ", p.Name, " = ", valueWhere);
                }
            });

            if (top == null)
                return string.Format(sQueryFormat, sFields, tableName);
            else
                return string.Format(sQueryFormat, string.Concat(" top ", top, " ", sFields), tableName);
        }
    }
}
