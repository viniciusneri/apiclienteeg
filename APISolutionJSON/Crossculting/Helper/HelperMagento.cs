﻿namespace Saraiva.SE.Crossculting.Helper
{
    public class HelperMagento
    {
        public string l016nt01 { get; set; }
        public string SrvLoja { get; set; }
        public string srvCentral { get; set; }
        public string EGarantida { get; set; }
        public string MongoConnection { get; set; }
        public string MongoDbGc { get; set; }
        public string MySqlConnection { get; set; }
        public string Database { get; set; }
        public string DatabaseCustomer { get; set; }
    }
}
