﻿namespace Saraiva.SE.Crossculting.Helper
{
    public class HelperPO
    {
        public string Url { get; set; }
        public string SoapActon { get; set; }
        public string EndPoint { get; set; }
        public string AutenticacaoMethod { get; set; }
        public string Usuario { get; set; }
        public string Senha { get; set; }
        public string xml { get; set; }
    }
}
