﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;

namespace Saraiva.SE.Crossculting.Enums
{
    public class enumsTypes
    {
        public enum GiftCardType
        {
            GiftCardVirtual = 1676,
        }

        public enum GiftCardState
        {
            GiftCardNovo = 1805
        }

        public enum EncomendaStatus
        {
            Aberto = 1,
            Suspenso = 2,
            Cancelado = 3,
            Finalizado = 4
        }

        public enum MotivoStatusEncomenda
        {
            Ativo = 0,
            Atendido = 20,
            FaltaPagamento = 15
        }

        public enum TipoEstorno
        {
            GC = 3202,
            ESCartao = 3203,
            RBDinheiro = 3310,
            RBDeposito = 3311,
            MarketPlace = 3316,
            CartaoDeCredito = 1157,
            Deposito = 1156,
            GiftCardSaraivaCom = 1162
        }

        public enum TipoStatusEstorno
        {
            Inconsistente = 762,
            Cancelado = 753,
            ParaAprovar = 754,
            EstornoGiftCard = 3284,
            Aprovado = 751,
            Suspenso = 752,
            Encerrado = 850,
            Reprovado = 761
        }

        public enum TipoEncomenda
        {
            SaraivaEntrega = 0,
            MarketPlace = 12,
            AssitenciaTecnica = 3317
        }

        public enum TipoGiftCardLancamento
        {
            InsertNovoSaldo = 290,
            EstornoNovoSaldo = 293
        }
    }

    public enum TipoRecebimentoSE
    {
        MeioRecebtoSE = 9
    }

    public enum CondicaoRecebimentoSE
    {
        CondRecebtoSE = 5
    }

    public enum TipoStatusEG
    {
        StatusAtender = 1610
    }

    public enum TipoItemEG
    {
        Encomenda = 1630,
        Reserva = 1631,
        RequisicaoCompra = 1632,
        Venda = 1633,
        PedidoCompra = 1649,
        ReposicaoLoja = 1727,
        Frete = 1733,
        SaraivaEntrega = 3318,
        AssitenciaTecnica = 3317 
    }

    public enum TipoAcaoEG
    {
        SolicitacaoRecebida = 1,
        Pago = 21,
        ProdutoEmBusca = 3,
        ProdutoReservado = 28,
        ProdutoEnviado = 38,
        EmTransporte = 40,
        ProdutoConferido = 17,
        DirecionadoFaturamento = 19
    }

    public enum PaymentTypeSaraivaEntrega
    {
        AguardandoPagamento = 1,
        Pago = 2
    }

    public enum PaymentSetted
    {
        Pago = 'S',
        NaoPago = 'N'
    }

    public enum TipoVinculo
    {
        VinculoCampanhaDependencia = 1637
    }

    public enum TipoRecebimento
    {
        CartaoDeCredito = 755,
        CartaoDeCreditoGW = 3130,
        TEFonLine = 3130,
        TicketCultura = 3134,
        SodexoCultura = 3180,
        AleloCultura = 3195,
        CartaoPresente = 1160,
        BonusDesconto = 759
    }

    public enum Filial
    {
        MercadoLivre = 696,
        Walmart = 631,
        Televendas = 1,
        SaraivaPontoCom = 16,
        B2W = 698,
        Wallmart = 631
    }

    public enum FormaRecebtoTrocaExtravio
    {
        Troca = 58,
        Extravio = 59
    }

    public enum TipoAcaoMudancaStatus
    {
        Pago = 1, // Apenas realizou pagamento (para retirada e faturamento depois) - (Tipo: 3318 - Saraiva Entrega)
        PagoFaturarMesmaLoja = 2, // Pagou e Faturou na mesma loja mesmo dia - (Tipo: 1633 - Venda)
        FaturarOutraLoja = 3, // Faturamento e retirada outro dia em outra loja (nova encomenda Gerada com tipo 1633 - Venda)
        FaturarMesmaLoja = 4, // Faturamento e retirada outro dia na mesma loja (Apenas mudança de status)
    }

    public enum TipoVinculoServicoEG
    {
        GE = 1,
        SRF = 2,
        SRS = 3
    }

    public enum TipoDocumento
    {
        CPF = 60,
        RG = 61,
        CGC = 64,
        InscEstadual = 65,
        Passaporte = 68
    }

    public enum EstruturadoServico
    {
        [Description("034001")]
        _NIVEL3 = 034001,
        [Description("03400101")]
        _ESTRUTURADO_GARANTIA_ESTENDIDA = 03400101,
        [Description("03400104")] // (Seguro Roubo Furto Simples)
        _ESTRUTURADO_SEGURO_ROUBO_FURTO = 03400104,
        [Description("03400103")]
        _ESTRUTURADO_SEGURO_ROUBO_FURTO_QUEBRA = 03400103
    }

    public enum TipoEncomendaRecebimento
    {
        Loja = 1,
        Cliente = 2
    }

    public enum TypeRefund
    {
        EmAberto = 1

    }

}
