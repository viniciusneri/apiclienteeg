﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Saraiva.SE.Crossculting.Enums
{


    /// <summary>
    /// Para Devolucao Utilizar codigo : ZDDD 
    /// Para Troca Utilizar codigo: ZSTR
    /// </summary>
    public enum enumsTrocaDevolucao : int
    {
        Troca = 0,
        Devolucao = 1
    }
    public enum enumsStatusTrocaDevolucao: int
    {
        rejeitado = 0,
        aceito = 1,
        aguardando_recebimento = 2,
        recebido_correios = 3,
        manuseio_correios = 4,
        recebido_cdo  =5,
        conferido_cdo = 6,
        aceito_troca = 7,
        aceito_devolucao = 8,
        indeferido = 9,
        aguardando_financeiro = 10,
        extravio = 11,
        reanalize = 12,
        finalizado = 13
    }
}
