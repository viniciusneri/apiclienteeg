﻿using System.ComponentModel;

namespace Saraiva.SE.Crossculting.Enums
{
    public enum enumsMessage
    {
        [Description("Encomenda não encontrada!")]
        EncomendaInexistente,
        [Description("Encomenda aguardando pagamento!")]
        EncomendaNaoPaga,
        [Description("Encomenda já cancelada!")]
        EncomendaCancelada,
        [Description("O(s) item(s) estão atendidos/faturados e não podem ser cancelados")]
        ItensAtendidos,
        [Description("Estorno não gerado")]
        EstornoNaoGerado,
        [Description("Dados bancários não informado")]
        DadosBancariosNaoInformado,
        [Description("Seguro não encontrado!")]
        SeguroNaoEncontrado
    }

    public enum typeMessage
    {
        Request,
        Response,
    }
}


