﻿using Saraiva.SE.Crossculting.Infraestructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Saraiva.SE.Crossculting.Service
{
    public class ServiceCross
    {
        public static void Insert<T>(T objectInput, string collectionName)
        {
            var repCross = new RepositoryCross(collectionName);
            repCross.Insert<T>(objectInput);
        }
    }
}
