﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace Saraiva.SE.Crossculting.Service
{
    public class WebServiceCall
    {
        private string _apiUrl;
        private string _SOAPAction;

        public WebServiceCall(string apiUrl, string SOAPAction)
        {
            _apiUrl = apiUrl;
            _SOAPAction = SOAPAction;
        }

        public XDocument Call(XDocument soapRequest)
        {
            try
            {
                using (var client = new HttpClient())
                {
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(_apiUrl),
                        Method = HttpMethod.Post
                    };

                    request.Content = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

                    request.Headers.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));
                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
                    request.Headers.Add("SOAPAction", _SOAPAction);

                    HttpResponseMessage response = client.SendAsync(request).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new Exception();
                    }

                    Task<Stream> streamTask = response.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);

                    return soapResponse;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }


        public XDocument CallPO(string soapRequest, string user, string senha)
        {
            try
            {
                using (var client = new HttpClient(new HttpClientHandler() { AutomaticDecompression = DecompressionMethods.Deflate | DecompressionMethods.GZip }))
                {
                    var request = new HttpRequestMessage()
                    {
                        RequestUri = new Uri(_apiUrl),
                        Method = HttpMethod.Post
                    };

                    request.Content = new StringContent(soapRequest.ToString(), Encoding.UTF8, "text/xml");

                    request.Headers.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("text/xml"));

                    if (!string.IsNullOrEmpty(user))
                    {
                        var byteArray = new UTF8Encoding().GetBytes(string.Format("{0}:{1}", user, senha));
                        client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(byteArray));
                    }

                    request.Content.Headers.ContentType = new MediaTypeHeaderValue("text/xml");
                    request.Headers.Add("SOAPAction", _SOAPAction);

                    HttpResponseMessage response = client.SendAsync(request).Result;

                    if (!response.IsSuccessStatusCode)
                    {
                        throw new ArgumentException(response.Content.ReadAsStringAsync().Result);
                    }

                    Task<Stream> streamTask = response.Content.ReadAsStreamAsync();
                    Stream stream = streamTask.Result;
                    var sr = new StreamReader(stream);
                    var soapResponse = XDocument.Load(sr);

                    return soapResponse;
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

    }
}
