﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace Saraiva.SE.Crossculting.Service
{
    public interface IServiceCall
    {
        string Put(string uri, string postData = "", params KeyValuePair<string, string>[] headers);
        string Post(string uri, string postData = "", params KeyValuePair<string, string>[] headers);
        string Get(string uri);
    }

    public class ServiceCall  : IServiceCall
    {
        public string Put(string uri, string postData = "", params KeyValuePair<string, string>[] headers)
        {
            if (string.IsNullOrWhiteSpace(uri)) throw new Exception("Invalid request");
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Referer = "";
            request.Method = "Put";

            postData = string.IsNullOrEmpty(postData) ? string.Empty : postData;
            var byteArray = Encoding.UTF8.GetBytes(postData);

            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            foreach (var header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            using (var dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);

                var response = request.GetResponse();
                var responseStream = response.GetResponseStream();

                using (var reader = new StreamReader(responseStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }

        public string Post(string uri, string postData = "", params KeyValuePair<string, string>[] headers)
        {
            if (string.IsNullOrWhiteSpace(uri)) throw new Exception("Invalid request");
            var request = (HttpWebRequest)WebRequest.Create(uri);
            request.Referer = "";
            request.Method = "POST";

            postData = string.IsNullOrEmpty(postData) ? string.Empty : postData;
            var byteArray = Encoding.UTF8.GetBytes(postData);

            request.ContentType = "application/json";
            request.ContentLength = byteArray.Length;

            foreach (var header in headers)
            {
                request.Headers.Add(header.Key, header.Value);
            }

            using (var dataStream = request.GetRequestStream())
            {
                dataStream.Write(byteArray, 0, byteArray.Length);

                var response = request.GetResponse();
                var responseStream = response.GetResponseStream();

                using (var reader = new StreamReader(responseStream))
                {
                    return reader.ReadToEnd();
                }
            }
        }
        
        public string Get(string uri)
        {
            HttpClient client;
            client = new HttpClient();
            client.BaseAddress = new Uri(uri);
            client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));

            HttpResponseMessage response = client.GetAsync(uri).Result;

            var list = response.Content.ReadAsStringAsync().Result;

            return list;
        }
    }
}

