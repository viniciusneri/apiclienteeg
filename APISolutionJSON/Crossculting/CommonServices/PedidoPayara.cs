﻿using Saraiva.SE.Crossculting.Helper;
using Saraiva.SE.Crossculting.Models;
using Saraiva.SE.Crossculting.Service;
using System;

namespace Saraiva.SE.Crossculting.CommonServices
{
    public class PedidoPayara
    {
        private readonly IServiceCall _serviceCall;
        private readonly HelperPayara _helperPayara;

        public PedidoPayara(IServiceCall serviceCall)
        {
            _serviceCall = serviceCall;

            var helperPayara = new HelperPayara()
            {
                urlbase = HelperConfig.GetConfigByLabel(string.Empty, "ConfigPayara", "urlbase"),
                user = HelperConfig.GetConfigByLabel(string.Empty, "ConfigPayara", "user"),
                pwt = HelperConfig.GetConfigByLabel(string.Empty, "ConfigPayara", "pwt")
            };

            _helperPayara = helperPayara;
        }

        public Pedido GetPedidoPayaraByIdPedido(int idPedido, int idFilial = 16)
        {
            try
            {
                int _idFilial = idFilial;
                var postGetPedidoPayara = _serviceCall.Get(string.Format("{0}/api-sis-pedido/loja/{1}/pedido/{2}", _helperPayara.urlbase, _idFilial, idPedido));
                var _pedidoResponse = Newtonsoft.Json.JsonConvert.DeserializeObject<Pedido>(postGetPedidoPayara);

                return _pedidoResponse;
            }
            catch (Exception ex)
            {
                throw new ArgumentException("Erro ao recuperar pedido do payara", ex);
            }
        }
    }
}


