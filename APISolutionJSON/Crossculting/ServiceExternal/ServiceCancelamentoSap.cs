﻿using Saraiva.SE.Crossculting.Domain.Model;
using Saraiva.SE.Crossculting.Service;
using System;
using System.Runtime.Serialization;
using System.Xml.Linq;
using System.Xml.Serialization;

namespace Saraiva.SE.Crossculting.ServiceExternal
{

    public class ServiceCancelamentoSap
    {
        private string  _UrlSoapAction = "http://sapazappd02.saraiva.corp:52200/XISOAPAdapter/MessageServlet?senderParty=&s" +
            "enderService=BS_PAYARA_D&receiverParty=&receiverService=&interface=SI_S_CANC_OV_" +
            "SAP_OUT&interfaceNamespace=http%3A%2F%2Fsaraiva.com.br%2Fpo%2Fpayara%2FcancelaOr" +
            "demVendaSAP";

        public CancelamentoSapResponse CancelamentoSap(CancelamentoSapRequest CancelamentoSapRequest)
        {
            WebServiceCall webServiceCall = new WebServiceCall("http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP", _UrlSoapAction);

            XDocument doc = new XDocument();
            using (var writer = doc.CreateWriter())
            {
                // write xml into the writer
                var serializer = new DataContractSerializer(CancelamentoSapRequest.GetType());
                serializer.WriteObject(writer, CancelamentoSapRequest);
            }

            XDocument _response =  webServiceCall.Call(doc);
            CancelamentoSapResponse _result = new CancelamentoSapResponse();

            XmlSerializer xmlSerializer = new XmlSerializer(typeof(CancelamentoSapResponse));
            using (var reader = _response.Root.CreateReader())
            {
                _result = (CancelamentoSapResponse)xmlSerializer.Deserialize(reader);
            }

            return _result;
        }

    }

    #region Classes do WSDL

    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP")]
    public partial class DT_CANC_OV_REQ
    {

        private DT_CANC_OV_REQHeader headerField;

        private DT_CANC_OV_REQItem[] itemField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public DT_CANC_OV_REQHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Item", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public DT_CANC_OV_REQItem[] Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }
    }

    /// <remarks/>
    
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP")]
    public partial class DT_CANC_OV_REQHeader
    {

        private string idPedidoMagentoField;

        private System.DateTime dataPedidoField;

        private bool dataPedidoFieldSpecified;

        private bool cartaoPresenteField;

        private bool cartaoPresenteFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer")]
        public string IdPedidoMagento
        {
            get
            {
                return this.idPedidoMagentoField;
            }
            set
            {
                this.idPedidoMagentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "date")]
        public System.DateTime DataPedido
        {
            get
            {
                return this.dataPedidoField;
            }
            set
            {
                this.dataPedidoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataPedidoSpecified
        {
            get
            {
                return this.dataPedidoFieldSpecified;
            }
            set
            {
                this.dataPedidoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public bool CartaoPresente
        {
            get
            {
                return this.cartaoPresenteField;
            }
            set
            {
                this.cartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CartaoPresenteSpecified
        {
            get
            {
                return this.cartaoPresenteFieldSpecified;
            }
            set
            {
                this.cartaoPresenteFieldSpecified = value;
            }
        }
    }

    /// <remarks/>
    
    [System.SerializableAttribute()]
    [XmlRoot(ElementName = "CancelamentoSapResponse")]
    [System.Xml.Serialization.XmlTypeAttribute(Namespace = "http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP")]
    public partial class CancelamentoSapResponse
    {
        [XmlElement(ElementName = "Header")]
        private CancelamentoSapResponseHeader headerField;

        [XmlElement(ElementName = "Item")]
        private CancelamentoSapResponseItens[] itensField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CancelamentoSapResponseHeader Header
        {
            get
            {
                return this.headerField;
            }
            set
            {
                this.headerField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Itens", Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public CancelamentoSapResponseItens[] Itens
        {
            get
            {
                return this.itensField;
            }
            set
            {
                this.itensField = value;
            }
        }
    }

    /// <remarks/>
    
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP")]
    public partial class CancelamentoSapResponseHeader
    {

        private string idPedidoMagentoField;

        private System.DateTime dataPedidoField;

        private bool dataPedidoFieldSpecified;

        private bool cartaoPresenteField;

        private bool cartaoPresenteFieldSpecified;

        private System.DateTime dataProcessamentoField;

        private bool dataProcessamentoFieldSpecified;

        private System.DateTime horaProcessamentoField;

        private bool horaProcessamentoFieldSpecified;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer")]
        public string IdPedidoMagento
        {
            get
            {
                return this.idPedidoMagentoField;
            }
            set
            {
                this.idPedidoMagentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "date")]
        public System.DateTime DataPedido
        {
            get
            {
                return this.dataPedidoField;
            }
            set
            {
                this.dataPedidoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataPedidoSpecified
        {
            get
            {
                return this.dataPedidoFieldSpecified;
            }
            set
            {
                this.dataPedidoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public bool CartaoPresente
        {
            get
            {
                return this.cartaoPresenteField;
            }
            set
            {
                this.cartaoPresenteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CartaoPresenteSpecified
        {
            get
            {
                return this.cartaoPresenteFieldSpecified;
            }
            set
            {
                this.cartaoPresenteFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "date")]
        public System.DateTime DataProcessamento
        {
            get
            {
                return this.dataProcessamentoField;
            }
            set
            {
                this.dataProcessamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DataProcessamentoSpecified
        {
            get
            {
                return this.dataProcessamentoFieldSpecified;
            }
            set
            {
                this.dataProcessamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "time")]
        public System.DateTime HoraProcessamento
        {
            get
            {
                return this.horaProcessamentoField;
            }
            set
            {
                this.horaProcessamentoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool HoraProcessamentoSpecified
        {
            get
            {
                return this.horaProcessamentoFieldSpecified;
            }
            set
            {
                this.horaProcessamentoFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP")]
    public partial class CancelamentoSapResponseItens
    {

        private string produtoField;

        private string itemField;

        private short quantidadeField;

        private bool quantidadeFieldSpecified;

        private string idMotivoField;

        private string statusField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Produto
        {
            get
            {
                return this.produtoField;
            }
            set
            {
                this.produtoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer")]
        public string Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public short Quantidade
        {
            get
            {
                return this.quantidadeField;
            }
            set
            {
                this.quantidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeSpecified
        {
            get
            {
                return this.quantidadeFieldSpecified;
            }
            set
            {
                this.quantidadeFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string IdMotivo
        {
            get
            {
                return this.idMotivoField;
            }
            set
            {
                this.idMotivoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Status
        {
            get
            {
                return this.statusField;
            }
            set
            {
                this.statusField = value;
            }
        }
    }

    /// <remarks/>
    
    [System.SerializableAttribute()]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://saraiva.com.br/po/payara/cancelaOrdemVendaSAP")]
    public partial class DT_CANC_OV_REQItem
    {

        private string produtoField;

        private string itemField;

        private short quantidadeField;

        private bool quantidadeFieldSpecified;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public string Produto
        {
            get
            {
                return this.produtoField;
            }
            set
            {
                this.produtoField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified, DataType = "integer")]
        public string Item
        {
            get
            {
                return this.itemField;
            }
            set
            {
                this.itemField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Form = System.Xml.Schema.XmlSchemaForm.Unqualified)]
        public short Quantidade
        {
            get
            {
                return this.quantidadeField;
            }
            set
            {
                this.quantidadeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool QuantidadeSpecified
        {
            get
            {
                return this.quantidadeFieldSpecified;
            }
            set
            {
                this.quantidadeFieldSpecified = value;
            }
        }
    }



    #endregion

}
