﻿using Newtonsoft.Json;
using Saraiva.SE.Crossculting.Helper;
using System.IO;
using System.Net.Http;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace Saraiva.SE.Crossculting.ServiceExternal
{
    public class ServiceExternalSoap
    {
        private static HelperCorreios _helperCorreios;
        public ServiceExternalSoap(HelperCorreios helperCorreios) => _helperCorreios = helperCorreios;

        public static T GetAssyncSoapResul<T>(object soap, string metodo)
        {
            var urlWebService =   HelperConfig.GetConfigByLabel("appsettings.json", "ConfigCorreios", "UrlWebServiceCorreios");
            var Usuario =  HelperConfig.GetConfigByLabel("appsettings.json", "ConfigCorreios", "Usuario");
            var Senha =  HelperConfig.GetConfigByLabel("appsettings.json", "ConfigCorreios", "Senha");

            var tag = char.ToUpper(metodo[0]) + metodo.Substring(1);

            var serialize = new XmlSerializer(soap.GetType());
            var xml = new StringWriter();
            serialize.Serialize(xml, soap);
            var mensagem = ConstruiMensagemSoap(metodo, xml.ToString()
                                                            .Replace(@"<?xml version=""1.0"" encoding=""utf-16""?>", "")
                                                            .Replace("<" + tag + @" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"">", "")
                                                            .Replace(@"</"+ tag + ">", ""));

            HttpClientHandler handler = new HttpClientHandler
            {
                Credentials = new System.Net.NetworkCredential(Usuario, Senha)
            };

            using (var httClient = new HttpClient(handler))
            {
                var stringTaskPost = httClient.PostAsync(urlWebService, new StringContent(mensagem, Encoding.UTF8, "text/xml"));
                stringTaskPost.Wait();
                var tObject = stringTaskPost.Result.Content.ReadAsStringAsync().Result;

                XmlDocument doc = new XmlDocument();
                doc.LoadXml(tObject);

                var GetXml = doc.GetElementsByTagName(metodo);

                var bodyXml = GetXml[0].OuterXml;

                XmlSerializer serializer = new XmlSerializer(typeof(T));
                StringReader rdr = new StringReader(bodyXml);
                var retorno = (T)serializer.Deserialize(rdr);

                //doc.LoadXml(bodyXml);

                //string jsonText = JsonConvert.SerializeXmlNode(doc);

                // JsonConvert.DeserializeObject<T>(jsonText);

                return retorno;

            }
        }

        public static string ConstruiMensagemSoap(string metodo, string message)
        {
            var LinkCorreios = HelperConfig.GetConfigByLabel("appsettings.json", "ConfigCorreios", "UrlXmlCorreios");

            StringBuilder corpoMensagem = new StringBuilder();
            corpoMensagem.AppendLine(@"<soapenv:Envelope  xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" 
            xmlns:ser=""{2}"" xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance""
            xsi:noNamespaceSchemaLocation=""MySchema.xsd"">");
            corpoMensagem.AppendLine(@"<soapenv:Header/>");
            corpoMensagem.AppendLine(@"     <soapenv:Body>");
            corpoMensagem.AppendLine(@"         <ser:{0}>");
            corpoMensagem.AppendLine(@"             {1}");
            corpoMensagem.AppendLine(@"         </ser:{0}>");
            corpoMensagem.AppendLine(@"     </soapenv:Body> ");
            corpoMensagem.AppendLine(@"</soapenv:Envelope>");

            return string.Format(corpoMensagem.ToString(), metodo, message.Trim(), LinkCorreios);
        }

    }
}
