﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;

namespace Saraiva.SE.Crossculting.ServiceExternal
{
    public class ServiceHelper
    {
        public class Authorization
        {
            public string Username { get; set; }
            public string Password { get; set; }
        }

        public static T GetAssyncRestResultJson<T>(string url, List<KeyValuePair<string, dynamic>> parameters,
            HttpMethod type, bool isQueryStringNamed = false, bool isObjectParameter = false, Authorization authorization = null)
        {
            try
            {
                var urlFormatted = url;
                var httClient = new HttpClient();
                httClient.GetAsync(url, HttpCompletionOption.ResponseContentRead);

                var client = new HttpClient();
                client.DefaultRequestHeaders.Accept.Clear();

                MediaTypeWithQualityHeaderValue contentType = new MediaTypeWithQualityHeaderValue("application/json");
                client.DefaultRequestHeaders.Accept.Add(contentType);

                HttpContent httpContext = null;

                HttpRequestMessage requestMessage = new HttpRequestMessage();
                                
                if (authorization != null)
                {                    
                    var _autorization = Encoding.ASCII.GetBytes(authorization.Username + ":" + authorization.Password);
                    client.DefaultRequestHeaders.Authorization = new
                        System.Net.Http.Headers.AuthenticationHeaderValue("Basic", Convert.ToBase64String(_autorization));
                }

                if (type == HttpMethod.Get || type == HttpMethod.Post)
                {
                    int contItem = 0;
                    parameters.ToList().ForEach(nm =>
                    {
                        if (isObjectParameter)
                        {
                            var inputObject = Newtonsoft.Json.JsonConvert.SerializeObject(nm.Value);
                            requestMessage = new HttpRequestMessage(type, urlFormatted);
                            requestMessage.Content = new StringContent(inputObject.ToString());
                            httpContext = new StringContent(inputObject.ToString());
                            var response = client.SendAsync(requestMessage);
                            response.Wait();
                        }
                        else if (isQueryStringNamed)
                        {
                            if (contItem == 0)
                                urlFormatted = urlFormatted + string.Concat("?", nm.Key, "=", nm.Value);
                            else
                                urlFormatted = urlFormatted + string.Concat("&", nm.Key, "=", nm.Value);
                        }
                        else
                        {
                            urlFormatted = urlFormatted + string.Concat("/", nm.Value);
                        }
                        contItem++;
                    });
                }

                if (type == HttpMethod.Post)
                {
                    httpContext.Headers.ContentType = new MediaTypeHeaderValue("application/json");
                    var stringTaskPost = client.PostAsync(urlFormatted, httpContext);
                    stringTaskPost.Wait();
                    var tObject = stringTaskPost.Result.Content.ReadAsStringAsync().Result;
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(tObject);
                }
                else
                {
                    var stringTask = client.GetStringAsync(urlFormatted);
                    stringTask.Wait();
                    return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(stringTask.Result);
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
