﻿using System;
using Saraiva.SE.Crossculting.Infraestructure.Repository;

namespace Saraiva.SE.Crossculting.Logs
{

}

public class ExceptionHandlerQueue
{
    public class entityException
    {
        public string trace { get; set; }
        public string message { get; set; }
        public DateTime data { get; set; }
    }


    public static void SaveException(Exception ex)
    {
        var repository = new RepositoryCross("CancelamentoExceptionLog");
        var log = new entityException()
        {
            data = DateTime.Now,
            message = ex.Message,
            trace = ex.StackTrace
        };
        repository.Insert<entityException>(log);
    }
}

