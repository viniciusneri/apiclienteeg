﻿using Saraiva.SE.Crossculting.Logs.EntityLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Saraiva.SE.Crossculting.Service;
using Saraiva.SE.Crossculting.Infraestructure.Repository;
using Saraiva.SE.Crossculting.Enums;

namespace Saraiva.SE.Crossculting.Logs
{
   
}

public class ExceptionHandler
{
    public class entityException
    {
        public int? orderId { get; set; }
        public string trace { get; set; }
        public string message { get; set; }
        public DateTime data { get; set; }
    }


    public static void SaveException(Exception ex)
    {
        var repository = new RepositoryCross("ApiExceptionLog");
        var log = new entityException()
        {
            data = DateTime.Now,
            message = ex.Message,
            trace = ex.StackTrace
        };
        repository.Insert<entityException>(log);
    }

    public static void SaveException(int orderId, string ex)
    {
        var repository = new RepositoryCross("ApiExceptionLog");
        var log = new entityException()
        {
            data = DateTime.Now,
            message = ex,
            orderId = orderId
        };
        repository.Insert<entityException>(log);
    }
}

