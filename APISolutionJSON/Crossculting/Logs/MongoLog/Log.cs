﻿using System;

namespace Saraiva.SE.Crossculting.Logs
{
    public class Log
    {
        public int OrderId { get; set; }
        public string key { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Request { get; set; }
        public string Response { get; set; }
        public bool IsError { get; set; }
        public string Menssage { get; set; }
        public DateTime Date { get; set; }
    }

    public static class InputLog
    {
        public static int OrderId { get; set; }
        public static string Key { get; set; }
    }
}
