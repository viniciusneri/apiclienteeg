﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Saraiva.SE.Crossculting.Logs.EntityLog
{
    public class LogProcessEntity
    {
        public ObjectId _id { get; set; }
        public int encomendaId { get; set; }
        public DateTime dataInvoke { get; set; }
        public string typeProcess { get; set; }
        public string document { get; set; }
    }
}
