﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APISolutionJSON.Models
{
	public class dadosCliente
	{
		private int? entity_id { get; set; }
		public int? entity_type_id { get; set; }
		public int? attribute_set_id { get; set; }
		public int? website_id { get; set; }
		public string email { get; set; }
		public int? group_id { get; set; }
		public int? increment_id { get; set; }
		public int? store_id { get; set; }
		public string created_at { get; set; }
		public string updated_at { get; set; }
		public int? is_active { get; set; }
		public int? disable_auto_group_change { get; set; }
		public string status { get; set; }
		public string integrated_updated_at { get; set; }
		public string exists_on_sis { get; set; }
		public string senha { get; set; }
		public string facebook_id { get; set; }
		public string created_in { get; set; }
		public string firstname { get; set; }
		public string lastname { get; set; }
		public string password_hash { get; set; }
		public string taxvat { get; set; }		
		public int? tipo_pessoa { get; set; }
		public string tipo_documento { get; set; }
		public string dob { get; set; }
		public string default_billing { get; set; }
		public int? default_shipping { get; set; }
		public int? gender { get; set; }
		public int? reward_update_notification { get; set; }
		public int? reward_warning_notification { get; set; }
		public int? origem { get; set; }
		public int? aceita_email { get; set; }

		public dadosCliente()
		{

		}

		public dadosCliente(int entity_id, int entity_type_id, int attribute_set_id, int website_id, string email, int group_id, int increment_id, int store_id, string created_at, string updated_at, int is_active, int disable_auto_group_change, string status, string integrated_updated_at, string exists_on_sis, string senha, string facebook_id, string created_in, string firstname, string lastname, string password_hash, string taxvat, int myProperty, int tipo_pessoa, string tipo_documento, string dob, string default_billing, int default_shipping, int gender, int reward_update_notification, int reward_warning_notification, int origem, int aceita_email)
		{
			this.entity_id = entity_id;
			this.entity_type_id = entity_type_id;
			this.attribute_set_id = attribute_set_id;
			this.website_id = website_id;
			this.email = email;
			this.group_id = group_id;
			this.increment_id = increment_id;
			this.store_id = store_id;
			this.created_at = created_at;
			this.updated_at = updated_at;
			this.is_active = is_active;
			this.disable_auto_group_change = disable_auto_group_change;
			this.status = status;
			this.integrated_updated_at = integrated_updated_at;
			this.exists_on_sis = exists_on_sis;
			this.senha = senha;
			this.facebook_id = facebook_id;
			this.created_in = created_in;
			this.firstname = firstname;
			this.lastname = lastname;
			this.password_hash = password_hash;
			this.taxvat = taxvat;
			
			this.tipo_pessoa = tipo_pessoa;
			this.tipo_documento = tipo_documento;
			this.dob = dob;
			this.default_billing = default_billing;
			this.default_shipping = default_shipping;
			this.gender = gender;
			this.reward_update_notification = reward_update_notification;
			this.reward_warning_notification = reward_warning_notification;
			this.origem = origem;
			this.aceita_email = aceita_email;
		}
	}
}