﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace APISolutionJSON.Models
{
	public class enderecosCliente
	{
		public int? entity_id { get; set; }
		public int? entity_type_id { get; set; }
		public int? attribute_set_id { get; set; }
		public string increment_id { get; set; }
		public int? parent_id { get; set; }
		public string created_at { get; set; }
		public string updated_at { get; set; }
		public int? is_active { get; set; }
		public int? edited_id { get; set; }
		public string status { get; set; }
		public string integrated_updated_at { get; set; }
		public int? exists_on_sis { get; set; }
		public string firstname { get; set; }
		public string lastname { get; set; }
		public string city { get; set; }
		public string country_id { get; set; }
		public string postcode { get; set; }
		public string telephone { get; set; }
		public string celular { get; set; }
		public string outros { get; set; }
		public int? numero_endereco { get; set; }
		public string complemento_endereco { get; set; }
		public string bairro { get; set; }
		public string ponto_referencia { get; set; }
		public string region { get; set; }
		public string region_id { get; set; }
		public int? cod_municipio { get; set; }
		public string tipo { get; set; }
		public string street { get; set; }
		public string customer_id { get; set; }

		public enderecosCliente()
		{

		}

		public enderecosCliente(int entity_id, int entity_type_id, int attribute_set_id, string increment_id, int parent_id, string created_at, string updated_at, int is_active, int edited_id, string status, string integrated_updated_at, int exists_on_sis, string firstname, string lastname, string city, string country_id, string postcode, string telephone, string celular, string outros, int numero_endereco, string complemento_endereco, string bairro, string ponto_referencia, string region, string region_id, int cod_municipio, string tipo, string street, string customer_id)
		{
			this.entity_id = entity_id;
			this.entity_type_id = entity_type_id;
			this.attribute_set_id = attribute_set_id;
			this.increment_id = increment_id;
			this.parent_id = parent_id;
			this.created_at = created_at;
			this.updated_at = updated_at;
			this.is_active = is_active;
			this.edited_id = edited_id;
			this.status = status;
			this.integrated_updated_at = integrated_updated_at;
			this.exists_on_sis = exists_on_sis;
			this.firstname = firstname;
			this.lastname = lastname;
			this.city = city;
			this.country_id = country_id;
			this.postcode = postcode;
			this.telephone = telephone;
			this.celular = celular;
			this.outros = outros;
			this.numero_endereco = numero_endereco;
			this.complemento_endereco = complemento_endereco;
			this.bairro = bairro;
			this.ponto_referencia = ponto_referencia;
			this.region = region;
			this.region_id = region_id;
			this.cod_municipio = cod_municipio;
			this.tipo = tipo;
			this.street = street;
			this.customer_id = customer_id;
		}
	}
}