﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using APISolutionJSON.Models;

namespace APISolutionJSON.Controllers
{
	[RoutePrefix("api/Teste")]
	public class TesteController : ApiController
    {

		private static List<Teste> listaUsuarios = new List<Teste>();

		


		[AcceptVerbs("GET")]
		[Route("ConsultarUsuarioPorCodigo/{codigo}")]
		public Teste ConsultarUsuarioPorCodigo(int codigo)
		{

			Teste usuario = listaUsuarios.Where(n => n.Codigo == codigo)
												.Select(n => n)
												.FirstOrDefault();

			return usuario;
		}


		[AcceptVerbs("GET")]
		[Route("ConsultarUsuarios")]
		public List<Teste> ConsultarUsuarios()
		{
			return listaUsuarios;
		}



	}
}
