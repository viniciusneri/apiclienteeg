﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Configuration;
using Microsoft.Extensions.Configuration;
using System.Text;
using System.Web.Http;
using System.Xml;
using APISolutionJSON.Models;
using Newtonsoft.Json.Linq;

namespace APISolutionJSON.Controllers
{
	[RoutePrefix("api/cliente")]
	public class ClienteController : ApiController
    {

		private static List<dadosCliente> listaCliente = new List<dadosCliente>();
		private static string caminhoServico = "";
		


		[AcceptVerbs("GET")]
		[Route("ConsultarUsuarios")]
		public List<dadosCliente> ConsultarUsuarios()
		{
			return listaCliente;
		}

		[AcceptVerbs("POST")]
		[Route("InfoCliente")]
		public string Cadastrarcliente(JObject jsonData)
		{
			caminhoServico = System.Configuration.ConfigurationManager.AppSettings["caminhoServico"].ToString();


			List<enderecosCliente> listaEndereco = new List<enderecosCliente>();
			dadosCliente cliente = jsonData["dadosCliente"].ToObject<dadosCliente>();
			var endereco = jsonData["enderecosCliente"].ToArray();
			string operacao = jsonData["operacao"].ToObject<string>() == "1" ? "U" : "I";

			

			foreach (var item in endereco)
			{
				enderecosCliente ed = item.ToObject<enderecosCliente>();
				listaEndereco.Add(ed);
			}

			return getAuthenticationToken(operacao, cliente, listaEndereco);

		}

		public static string getAuthenticationToken(string operacao,dadosCliente cliente, List<enderecosCliente> listaEndereco_)
		{
			string s = "";

			try
			{
				StringBuilder xml = new StringBuilder();

				string dthj = DateTime.Now.ToString("yyyy-MM-ddTHH':'mm':'ss");

				xml.Append(@"<soapenv:Envelope xmlns:soapenv=""http://schemas.xmlsoap.org/soap/envelope/"" ");
				xml.Append(@"xmlns:impl=""http://impl.service.ws.sis.cliente.saraiva.br/"" ");
				xml.Append(@"xmlns:sar=""http://www.saraiva.com.br/"">");
				xml.Append("<soapenv:Header/>");
				xml.Append("<soapenv:Body>");
				xml.Append("<impl:mantemCliente>");
				xml.Append("<!--Optional:-->");
				xml.Append("<Atualizacao>");
				xml.Append("<IdAtualizacao>3a2da188-bc8c-4ddd-a544-4f9e66cf6333</IdAtualizacao>");
				xml.Append(String.Format("<DataHora>{0}</DataHora>", dthj));
				xml.Append(String.Format("<Operacao>{0}</Operacao>", operacao));
				xml.Append("<Status>N</Status>");
				xml.Append("<Recurso>Cliente</Recurso>");
				xml.Append("<Componente>Cliente</Componente>");
				xml.Append("<Dados>");
				xml.Append("<Cliente>");
				xml.Append(String.Format("<IdCliente>{0}</IdCliente>", cliente.increment_id));
				xml.Append(String.Format("<Nome><![CDATA[{0}]]></Nome>", cliente.firstname));
				xml.Append(String.Format("<Login>{0}</Login>", cliente.email));
				xml.Append(String.Format("<Email>{0}</Email>", cliente.email));
				xml.Append("<Senha></Senha>");
				xml.Append(String.Format("<PessoaJuridica>{0}</PessoaJuridica>", cliente.tipo_documento == "CPF" ? "false" : "true"));
				xml.Append("<Documento>");
				xml.Append(String.Format("<CPF>{0}</CPF>", cliente.tipo_documento == "CPF" ? cliente.taxvat : ""));
				xml.Append(String.Format("<CNPJ>{0}</CNPJ>", cliente.tipo_documento == "CNPJ" ? cliente.taxvat : ""));
				xml.Append("<InscricaoEstadual></InscricaoEstadual>");
				xml.Append("<Passaporte />");
				xml.Append("</Documento>");
				xml.Append("<Perfil>");
				xml.Append(String.Format("<DataNascimento>{0}</DataNascimento>", cliente.dob.Substring(0,10)));
				xml.Append(String.Format("<Sexo>{0}</Sexo>", cliente.gender == 1 ? "M" : "F"));
				xml.Append(String.Format("<AceitaMalaDireta>{0}</AceitaMalaDireta>", cliente.aceita_email));
				xml.Append("</Perfil>");
				//ENDEREÇO 
				xml.Append("<Enderecos>");
				xml.Append(String.Format("<TotalPaginas>{0}</TotalPaginas>", listaEndereco_.Count));

				foreach (var endereco in listaEndereco_)
				{
					xml.Append("<Endereco>");
					xml.Append(String.Format("<IdEndereco>{0}</IdEndereco>", endereco.increment_id));
					xml.Append(String.Format("<Destinatario>{0}</Destinatario>", endereco.firstname));
					xml.Append(String.Format("<Logradouro>{0}</Logradouro>", endereco.street));
					xml.Append(String.Format("<Numero>{0}</Numero>", endereco.numero_endereco));
					xml.Append(String.Format("<Complemento>{0}</Complemento>", endereco.complemento_endereco));
					xml.Append(String.Format("<Referencia>{0}</Referencia>", endereco.ponto_referencia));
					xml.Append(String.Format("<Bairro>{0}</Bairro>", endereco.bairro));
					xml.Append(String.Format("<Cidade>{0}</Cidade>", endereco.city));
					xml.Append(String.Format("<CodMunicipio>{0}</CodMunicipio>", endereco.region_id));
					xml.Append("<UF>SP</UF>");
					xml.Append(String.Format("<CEP>{0}</CEP>", endereco.postcode));
					xml.Append(String.Format("<Pais>{0}</Pais>", endereco.country_id));
					xml.Append("<Telefones>");
					xml.Append("<Contato>");
					xml.Append("<DDI>55</DDI>");
					xml.Append(String.Format("<Numero>{0}</Numero>", endereco.telephone));
					xml.Append("</Contato>");
					xml.Append("</Telefones>");
					xml.Append("<Tipo>80</Tipo>");
					xml.Append("<Principal>true</Principal>");
					xml.Append("</Endereco>");
				}
				
				xml.Append("</Enderecos>");
				xml.Append("<Ativo>true</Ativo>");				
				xml.Append("<Colaborador>true</Colaborador>");
				xml.Append("<UmClique>0</UmClique>");
				xml.Append("<Contribuinte>false</Contribuinte>");
				xml.Append("<Telefones />");
				xml.Append("<CodCanal>1</CodCanal>");
				xml.Append(String.Format("<Sobrenome><![CDATA[{0}]]></Sobrenome>", cliente.lastname));				
				xml.Append(String.Format("<DtCadastro>{0}</DtCadastro>", dthj));
				xml.Append(String.Format("<DtAlteracao>{0}</DtAlteracao>", dthj));
				xml.Append("<SaraivaPlus>false</SaraivaPlus>");
				xml.Append("</Cliente>");
				xml.Append("</Dados>");
				xml.Append("</Atualizacao>");
				xml.Append("</impl:mantemCliente>");
				xml.Append("</soapenv:Body>");
				xml.Append("</soapenv:Envelope>");


				s = getUKMailData(xml.ToString(), caminhoServico);
			}
			catch (Exception ex)
			{

				throw ex;
			}
			return s;

			//ClienteServic Scliente = new ClienteService();
		}

		public static string getUKMailData(string xml, string address)
		{
			string result = "";
			HttpWebRequest request = CreateWebRequest(address);
			XmlDocument soapEnvelopeXml = new XmlDocument();
			soapEnvelopeXml.LoadXml(xml);
			string soapResult = "";

			using (Stream stream = request.GetRequestStream())
			{
				soapEnvelopeXml.Save(stream);
			}

			using (WebResponse response = request.GetResponse()) 
			{
				using (StreamReader rd = new StreamReader(response.GetResponseStream()))
				{
					soapResult = rd.ReadToEnd();
					Console.WriteLine(soapResult);
				}
			}

			return soapResult;
		}

		public static HttpWebRequest CreateWebRequest(string url)
		{
			HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
			string action = "";
			webRequest.Headers.Add("SOAPAction", action);
			webRequest.ContentType = "text/xml;charset=\"utf-8\"";
			webRequest.Accept = "text/xml";
			webRequest.Method = "POST";
			return webRequest;
		}



	}
}
