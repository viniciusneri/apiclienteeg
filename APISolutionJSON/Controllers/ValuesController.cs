﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Xml;

namespace APISolutionJSON.Controllers
{
	[RoutePrefix("api/values")]
	public class ValuesController : ApiController
	{



		[AcceptVerbs("GET")]
		[Route("xml")]
		public IEnumerable<string> Get()
		{
			XmlTextReader reader = new XmlTextReader("C:\\Dados\\Dados.xml");

			return new string[] { "value1", "value2" };
		}

		// GET api/values/5
		public string Get(int id)
		{
			return "value";
		}

		// POST api/values
		public void Post([FromBody]string value)
		{
		}

		// PUT api/values/5
		public void Put(int id, [FromBody]string value)
		{
		}

		// DELETE api/values/5
		public void Delete(int id)
		{
		}
	}
}
